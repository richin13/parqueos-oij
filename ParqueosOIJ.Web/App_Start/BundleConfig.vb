﻿Imports System.Web.Optimization

Public Module BundleConfig
    ' For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    Public Sub RegisterBundles(ByVal bundles As BundleCollection)

        bundles.Add(New ScriptBundle("~/bundles/site").Include(
                    "~/Content/bower/jquery/dist/jquery.min.js",
                    "~/Content/bower/bootstrap/dist/js/bootstrap.min.js",
                    "~/Content/bower/sticky-kit/jquery.sticky-kit.min.js",
                    "~/Content/bower/bs-typeahead/js/bootstrap-typeahead.min.js",
                    "~/Content/js/site.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Content/js/jquery.validate*",
                    "~/Content/js/jqueryval.bootstrap.js"))

        bundles.Add(New ScriptBundle("~/bundles/pickadate").Include(
                    "~/Content/bower/pickadate/lib/compressed/picker.js",
                    "~/Content/bower/pickadate/lib/compressed/picker.date.js",
                    "~/Content/bower/pickadate/lib/compressed/picker.time.js"))

        bundles.Add(New ScriptBundle("~/bundles/swal").Include(
                    "~/Content/bower/sweetalert2/dist/sweetalert2.min.js"))

        bundles.Add(New ScriptBundle("~/bundles/list.js").Include(
                    "~/Content/bower/list.js/dist/list.min.js"))

        bundles.Add(New ScriptBundle("~/bundles/timeago").Include(
                    "~/Content/bower/jquery-timeago/jquery.timeago.js",
                    "~/Content/bower/jquery-timeago/locales/jquery.timeago.es.js"))

        bundles.Add(New ScriptBundle("~/bundles/selectize").Include(
                    "~/Content/bower/selectize/dist/js/standalone/selectize.min.js"))

        bundles.Add(New ScriptBundle("~/bundles/raphael").Include(
                    "~/Content/bower/raphael/raphael.min.js"))

        bundles.Add(New ScriptBundle("~/bundles/handlebars").Include(
                    "~/Content/bower/handlebars.js/lib/handlebars.js"))

        bundles.Add(New ScriptBundle("~/bundles/parkings").Include(
                    "~/Content/bower/underscore/underscore-min.js",
                    "~/Content/js/parkings.js"))

        bundles.Add(New ScriptBundle("~/bundles/admissions").Include(
                    "~/Content/js/admissions.js"))

        bundles.Add(New ScriptBundle("~/bundles/edit-parkings").Include(
                    "~/Content/js/edit_parkings.js"))

        ' StyleBundes from here (all the css files)
        '"~/Content/bower/flat-ui/dist/css/flat-ui.min.css",
        bundles.Add(New StyleBundle("~/styles/main").Include(
                    "~/Content/bower/bootstrap/dist/css/bootstrap.min.css",
                    "~/Content/css/Site.css",
                    "~/Content/css/vendor/css-loader/css-loader.css",
                    "~/Content/css/vendor/balloon.css/balloon.min.css",
                    "~/Content/bower/font-awesome/css/font-awesome.min.css"))

        bundles.Add(New StyleBundle("~/styles/pickadate").Include(
                    "~/Content/bower/pickadate/lib/compressed/themes/classic.css",
                    "~/Content/bower/pickadate/lib/compressed/themes/classic.date.css",
                    "~/Content/bower/pickadate/lib/compressed/themes/classic.time.css"))

        bundles.Add(New StyleBundle("~/styles/swal").Include(
                    "~/Content/bower/sweetalert2/dist/sweetalert2.min.css"))

        bundles.Add(New StyleBundle("~/styles/selectize").Include(
            "~/Content/bower/selectize/dist/css/selectize.bootstrap3.css"))
    End Sub
End Module

