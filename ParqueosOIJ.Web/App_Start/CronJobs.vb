﻿Imports Owin
Imports ParqueosOIJ.Service

Partial Public Class Startup
    Public Sub ConfigureCronJobs(app As IAppBuilder)
        ParkingLotUpdater.Start()
        ReservationUpdater.Start()
    End Sub
End Class