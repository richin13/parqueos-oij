﻿@Imports ParqueosOIJ.Model
@ModelType ICollection(Of Reservation)
@Code
    ViewData("Title") = "Solicitudes de espacios"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    <div class="col-xs-12 col-centered text-center">
        <div class="panel panel-default page-content" id="reservations-container">
            <div class="panel-body">
                <h2>@ViewBag.Title</h2>
                <hr />
                <div class="col-xs-12 text-left">
                    <div class="row">
                        <div class="col-xs-6">
                            Filtrar: <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Index"})">Todas</a> |
                            <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Index", .filter = "Pendientes"})">Pendientes</a> |
                            <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Index", .filter = "Aprobadas"})">Aprobadas</a> |
                            <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Index", .filter = "Rechazadas"})">Rechazadas</a>
                        </div>
                        <div class="col-xs-6 text-left hidden-xs">
                            <div class="form-group has-feedback pull-right">
                                <input type="text" class="search form-control" placeholder="Filtrar" style="max-width: 12em;" />
                                <span class="form-control-feedback">
                                    <i class="fa fa-filter"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                @If Model.Count > 0 Then
                    @<ul class="list list-unstyled text-center">
                        @For Each r As Reservation In Model
                @<text>
                    <li>
                        <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Ver", .id = r.Id})" class="clickable-panel">
                            <div class="col-xs-4">
                                <div class="panel panel-default">
                                    <div class="panel-body text-left">
                                        <h3>Reservación #@r.Id</h3>
                                        <hr />
                                        <ul class="list-unstyled">
                                            <li class="visitor-name"><b>Visitante: </b>@r.Visitor.Name</li>
                                            <li class="vehicle-license-plate"><b>Vehículo: </b>@r.Vehicle.LicensePlate</li>
                                            <li><b>Fecha de entrada: </b>@r.EntryDate.ToString("dd/MM/yyyy a la\s HH:mm")</li>
                                            <li><b>Estado: </b>@Html.StatusLabel(r.Status)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                </text>
                        Next
                    </ul>

                Else
                    @<text>
                        <div class="col-xs-12 page-content">
                            <div class="alert alert-info" data-dissmiss="alert">
                                <i class="fa fa-info-circle fa-3x"></i> <h3>No hay ninguna solicitud de espacio</h3>
                            </div>
                        </div>
                    </text>
                End If
            </div>
        </div>

    </div>
</div>
@Section Scripts
    <script>
        $('.reservations tr').each(function (i, el) {
            el.onclick = function () {
                window.location = $(el).data('href');
            }
        });
    </script>

    @Scripts.Render("~/bundles/list.js")
    <script>
        $(document).ready(function () {
            //
            var options = {
                valueNames: [
                    'visitor-name',
                    'vehicle-license-plate',
                    { data: ['visitor-dni'] }]
            };

            var userList = new List('reservations-container', options);
        });
    </script>
End Section