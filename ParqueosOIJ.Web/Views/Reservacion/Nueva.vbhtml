﻿@ModelType ReservationForm
@Code
    ViewData("Title") = "Nueva reservación"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@Section Styles
    @Styles.Render("~/styles/pickadate")
    @Styles.Render("~/styles/selectize")
End Section

<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        <h1>Reservación de espacio</h1>
        <p>En caso de que esté esperando una visita y esta venga en vehículo propio, es posible realizar una <b>solicitud de reservación de espacio</b> en el parqueo institucional. La solicitud deberá ser aprobada por un administrador. Para saber el estado de una consulta de reservación vaya a la página de <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Consultar"})">consulta</a> o llamando al 2222-2222</p>
        @Using Html.BeginForm("Nueva", "Reservacion", FormMethod.Post, New With {.role = "form", .class = "text-left"})
            @Html.AntiForgeryToken()
            @Html.ValidationSummary("", New With {.class = "text-danger"})
            @<div class="row">
                <h4 class="underlined-header">Detalles del visitante</h4>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.Visitor, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.Visitor, New With {.class = "form-control", .placeholder = "Cédula del conductor"})
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.Vehicle, New With {.class = ""})
                        <div class="row">
                            <div class="col-xs-6" style="margin-right:0!important;">
                                <select name="" tabindex="1" class="form-control" id="placas">
                                    <option value="CL">CARGA LIVIANA</option>
                                    <option value="C">CARGA PESADA</option>
                                    <option value="CC">CUERPO CONSULAR</option>
                                    <option value="CD">CUERPO DIPLOMATICO</option>
                                    <option value="D">DISCAPACITADO</option>
                                    <option value="MD">MISION DIPLOMATICA</option>
                                    <option value="MI">MISION INTERNACIONAL</option>
                                    <option value="MOT">MOTO</option>
                                    <option selected="selected" value="PART">PARTICULAR</option>
                                    <option value="PEN">PENSIONADOS</option>
                                    <option value="SM">SERVICIO MUNICIPAL</option>
                                    <option value="TA">TAXI ALAJUELA</option>
                                    <option value="TC">TAXI CARTAGO</option>
                                    <option value="TG">TAXI GUANACASTE</option>
                                    <option value="TH">TAXI HEREDIA</option>
                                    <option value="TL">TAXI LIMÓN</option>
                                    <option value="TP">TAXI PUNTARENAS</option>
                                    <option value="TSJ">TAXI SAN JOSE</option>
                                    <option value="VE">VE VEHICULO ESPECIAL</option>
                                    <option value="VH">VH VEHICULO HISTORICO</option>
                                </select>
                            </div>
                            <div class="col-xs-6" style="margin-left:0px !important;">
                                @Html.TextBoxFor(Function(m) m.Vehicle, New With {.class = "form-control", .placeholder = "Placa del vehículo"})
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @<div class="row">
                <h4 class="underlined-header">Detalles de la visita</h4>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="Employee.Dni" data-balloon-length="medium" data-balloon="Cédula del funcionario que está realizando la reservación" data-balloon-pos="up">
                            Funcionario
                            <i class="fa fa-info-circle"></i>
                        </label>
                        @Html.TextBoxFor(Function(m) m.Employee, New With {.class = "form-control", .placeholder = "Cédula del funcionario"})
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.Reason, New With {.class = ""})
                        @Html.TextAreaFor(Function(m) m.Reason, New With {.class = "form-control", .placeholder = "Motivo de la visita"})
                    </div>
                </div>
            </div>
            @<div class="row row-centered">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.EntryDateV, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.EntryDateV, New With {.class = "form-control date-input", .placeholder = "Fecha entrada", .readonly = ""})
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.EntryHour, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.EntryHour, New With {.class = "form-control hour-input", .placeholder = "Hora entrada"})
                    </div>
                </div>
            </div>
            @<div class="row row-centered">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="DepartureDateV" data-balloon-length="medium" data-balloon="Esta fecha es tentativa" data-balloon-pos="up">
                            Fecha de salida
                            <i class="fa fa-info-circle"></i>
                        </label>
                        @Html.TextBoxFor(Function(m) m.DepartureDateV, New With {.class = "form-control date-input", .placeholder = "Fecha de salida", .readonly = ""})
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.DepartureHour, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.DepartureHour, New With {.class = "form-control hour-input", .placeholder = "Hora de salida"})
                    </div>
                </div>
            </div>
            @<div class="row row-centered">
                <div class="col-xs-12">
                    <input type="submit" value="Crear" class="btn btn-primary btn-lg pull-right" />
                </div>
            </div>
        End Using
    </div>
</div>
@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
    @Scripts.Render("~/bundles/pickadate")
    <script>
        $(document).ready(function () {
            $('.date-input').pickadate({
                format: 'd !de mmmm !del yyyy',
                formatSubmit: 'd/m/yyyy',
                hiddenName: true,
                today: "Hoy",
                clear: "Limpiar",
                close: "Cerrar",
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                min: new Date(),
                max: false,
                disable: [
                    1, 7
                ],
                closeOnClear: false
            });

            $('.hour-input').pickatime({
                formatSubmit: 'H:i',
                hiddenName: true,
                disable: [{
                    from: [0, 0],
                    to: [7, 30]
                }, {
                    from: [17, 0],
                    to: [23, 30]
                }]
            });
        });
    </script>
    @Scripts.Render("~/bundles/selectize")
    <script>
        $(document).ready(function () {
            //
            $('#placas').selectize({
                create: false,
                sortField: 'text'
            })
        })
    </script>
End Section