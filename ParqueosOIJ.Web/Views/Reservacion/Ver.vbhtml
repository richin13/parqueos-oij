﻿@Imports ParqueosOIJ.Model
@ModelType Reservation
@Code
    ViewData("Title") = "Reservación"
    Layout = "~/Views/Shared/_Layout.vbhtml"

    Dim status As ReservationStatus
    status = Model.Status

End Code
@Section Styles
    @Styles.Render("~/styles/swal")
End Section
<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        @If ViewBag.FlashMessages IsNot Nothing Then
            For Each fm As FlashMessage In ViewBag.FlashMessages
                Html.RenderPartial("_FlashMessage", fm)
            Next
        End If
        <div id="parking-loader" class="loader loader-default is-active hidden" data-text="Cargando información del parqueo" blink></div>
        @If Roles.IsUserInRole("Admin") Then
            Using Html.BeginForm("Ver", "Reservacion", New With {.id = Model.Id}, FormMethod.Post, New With {.id = "ReservationForm", .role = "form"})
                @<text>
                    <div class="panel panel-default page-content" id="reservation-panel" data-id="@Model.Id">
                        @Html.HiddenFor(Function(r) r.Id)
                        @Html.HiddenFor(Function(r) r.Status)
                        @Html.HiddenFor(Function(r) r.Reason)
                        @Html.HiddenFor(Function(r) r.VisitorID)
                        @Html.HiddenFor(Function(r) r.ParkingLotID)
                        @Html.HiddenFor(Function(r) r.VehicleID)
                        @Html.HiddenFor(Function(r) r.EmployeeID)
                        @Html.HiddenFor(Function(r) r.RDate)
                        @Html.HiddenFor(Function(r) r.EntryDate)
                        @Html.HiddenFor(Function(r) r.DepartureDate)
                        <div class="panel-body">
                            <h1>@ViewBag.Title <small># @Model.Id</small></h1>
                            <p>Se muestran los datos de la solicitud número @Model.Id</p>
                            <hr />
                            <ul class="list-group main-list-group text-left">
                                <li class="list-group-item">
                                    <i class="fa fa-asterisk"></i> @Html.LabelFor(Function(r) r.Status): @Html.StatusLabel(Model.Status)
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-calendar-o"></i> Fechas
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <i class="fa fa-calendar"></i>
                                            <label>Realizada el</label>
                                            @Model.RDate.ToString("dd/MM/yyyy a la\s HH:mm") (<time class="timeago" datetime="@Model.RDate.ToString("yyyy-MM-dd HH:mm:ss")"></time>)
                                        </li>
                                        <li class="list-group-item">
                                            <i class="fa fa-calendar-plus-o"></i>
                                            <label>Entrada </label>
                                            @Model.EntryDate.ToString("dd/MM/yyyy a la\s HH:mm")
                                            @If Model.Status = ReservationStatus.Pendiente Or Model.Status = ReservationStatus.Rechazada Then
                                                @<button type="button" class="btn btn-default" data-target-date="@Model.EntryDate.ToString("dd/MM/yyyy HH:mm")" id="view-parking" data-balloon-length="medium" data-balloon="Ver parqueo en esta fecha" data-balloon-pos="up">
                                                    <i class="fa fa-external-link"></i>
                                                </button>
                                            End If
                                        </li>
                                        <li class="list-group-item">
                                            <i class="fa fa-calendar-minus-o"></i>
                                            <label>Salida </label>
                                            @Model.DepartureDate.ToString("dd/MM/yyyy a la\s HH:mm")
                                        </li>
                                    </ul>
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-building"></i> <label>Espacio asignado: <span class="parking-lot-id">@(If(Model.ParkingLotID = 0, "Ninguno", Model.ParkingLotID.ToString()))</span></label>

                                </li>
                                @If Model.Status <> ReservationStatus.Pendiente Then
                                    @<li class="list-group-item"><i class="fa fa-lemon-o"></i> Último cambio por: @Model.User.FirstName @Model.User.LastName</li>
                                End If

                                <li class="list-group-item">
                                    <i class="fa fa-user"></i> Visitante
                                    <ul class="list-group">
                                        <li class="list-group-item">@Html.LabelFor(Function(r) r.Visitor.Dni): @Model.Visitor.Dni</li>
                                        <li class="list-group-item">@Html.LabelFor(Function(r) r.Visitor.Name): @Model.Visitor.Name</li>
                                    </ul>
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-car"></i> Vehículo
                                    <ul class="list-group">
                                        <li class="list-group-item">@Html.LabelFor(Function(r) r.Vehicle.LicensePlate): @Model.Vehicle.LicensePlate</li>
                                        <li class="list-group-item">@Html.LabelFor(Function(r) r.Vehicle.Brand): @Model.Vehicle.Brand</li>
                                    </ul>
                                </li>
                                <li class="list-group-item"><i class="fa fa-user-secret"></i> <label>Realizada por: </label> @Model.Employee.Name</li>
                                <li class="list-group-item"><i class="fa fa-briefcase"></i> @Html.LabelFor(Function(r) r.Reason): @Model.Reason</li>
                                <li class="list-group-item">
                                    <label for="Comment" data-balloon-length="medium" data-balloon="El comentario es público" data-balloon-pos="up">
                                        <i class="fa fa-comment"></i> Comentario <i class="fa fa-info-circle"></i>
                                    </label>
                                    @Html.TextAreaFor(Function(r) r.Comment, New With {.class = "form-control"})
                                </li>
                            </ul>
                        </div>
                        <div class="panel-footer text-right">
                            <a class="btn btn-default hidden-xs" href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Index"})"><i class="fa fa-arrow-left"></i> Regresar</a>
                            @If Model.CanReject() Then
                                @<button class="btn btn-danger reject" type="button"><i class="fa fa-times"></i> Rechazar</button>
                            End If
                            @If Model.CanApprove() Then
                                @<button class="btn btn-primary accept" type="button"><i class="fa fa-check"></i> Aprobar</button>

                            End If
                        </div>
                    </div>
                </text>
            End Using

            @<div id="parking-modal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Proyección del parqueo para la fecha @Model.EntryDate.ToString("dd/MM/yyyy a la\s HH:mm")</h4>
                            <span>Seleccione el espacio a reservar</span>
                        </div>
                        <div class="modal-body">
                            <div class="col-xs-10 col-md-10 col-centered">

                                <div id="paper" style="width:50%; height:50%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        Else
            @<text>
                <div class="panel panel-default page-content" id="reservation-panel" data-id="@Model.Id">
                    <div class="panel-body">
                        <h2>Detalles de la reservación</h2>
                        <hr />
                        <ul class="list-group main-list-group text-left">
                            <li class="list-group-item">
                                <i class="fa fa-hashtag"></i> @Html.LabelFor(Function(r) r.Id): @Model.Id
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-asterisk"></i> @Html.LabelFor(Function(r) r.Status): @Html.StatusLabel(Model.Status)
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-calendar"></i> <label>Realizada el</label> @Model.RDate.ToString("dd/MM/yyyy a la\s HH:mm") (<time class="timeago" datetime="@Model.RDate.ToString("yyyy-MM-dd HH:mm:ss")"></time>)
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-calendar-plus-o"></i> <label>Fecha de entrada</label> @Model.EntryDate.ToString("dd/MM/yyyy a la\s HH:mm")
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-calendar-minus-o"></i> <label>Vence:</label> <time class="timeago" datetime="@Model.EntryDate.ToString("yyyy-MM-dd HH:mm:ss")"></time>
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-user"></i> <label>Visitante:</label> @Model.Visitor.Dni
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-car"></i> <label>Vehículo:</label> @Model.Vehicle.LicensePlate
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-user-secret"></i> <label>Solicitada por:</label> @Model.Employee.Name
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-briefcase"></i> @Html.LabelFor(Function(r) r.Reason): @Model.Reason
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-comment"></i> @Html.LabelFor(Function(r) r.Comment): @Model.Comment
                            </li>
                        </ul>
                    </div>
                </div>
            </text>
        End If

    </div>
</div>

@Section Scripts
    @Scripts.Render("~/bundles/swal")
    @Scripts.Render("~/bundles/timeago")

    <script>
        $(document).ready(function () {
            jQuery.timeago.settings.allowFuture = true;
            $('time.timeago').timeago();
        })
    </script>

    @If Roles.IsUserInRole("Admin") Then
        @Scripts.Render("~/bundles/raphael")
        @Scripts.Render("~/bundles/parkings")

        @<script>
            $(document).ready(function () {
                $('#reservation-panel').find('.reject').click(function (e) {
                    e.preventDefault();
                    swal({
                        title: 'Rechazar la solicitud',
                        text: '¿Está seguro/a que desea rechazar la solicitud?',
                        type: 'question',
                        showCancelButton: true,
                        cancelButtonText: 'No',
                        confirmButtonText: 'Sí',
                        closeOnConfirm: false,
                        showCloseButton: true
                    }).then(function () {
                        sendWithStatus("Rechazada");
                    });
                });

                $('#reservation-panel').find('.accept').click(function (e) {
                    swal({
                        title: 'Aprobar la solicitud',
                        text: '¿Está seguro/a que desea aprobar la solicitud?',
                        type: 'question',
                        showCancelButton: true,
                        cancelButtonText: 'No',
                        confirmButtonText: 'Sí',
                        showCloseButton: true
                    }).then(function () {
                        sendWithStatus("Aprobada");
                    });
                });
            });

            // updates the status field with the new status value and send the form to the server.
            function sendWithStatus(status) {
                if (status === null) {
                    status = "Pendiente";
                }
                $('#Status').val(status);
                $('#ReservationForm').submit();
            }

        </script>

        @<script>
            $(document).ready(function () {
                $('#view-parking').on('click', function (e) {
                    // se carga la info del parqueo y se muestra el modal
                    $('#parking-modal').modal('show');
                    var loader = new Parking({
                        parking: 1,
                        showNumber: false,
                        date: $(this).data('target-date'),
                        onRectClick: function (parkingLot) {
                            // TODO
                            if (parkingLot.Status == 0) {
                                setParkingLot(parkingLot);
                            } else if (parkingLot.Status == 4) {
                                swal({
                                    title: 'Estado del espacio',
                                    text: 'No es posible determinar con certeza el estado de ese espacio de parqueo para esa fecha. ¿Está seguro que desea asignar este espacio?',
                                    type: 'question',
                                    showCancelButton: true,
                                    cancelButtonText: 'No',
                                    confirmButtonText: 'Sí',
                                    closeOnConfirm: true,
                                    showCloseButton: true
                                }).then(function () {
                                    setParkingLot(parkingLot);
                                });
                            } else {
                                swal({
                                    title: 'Error!',
                                    text: 'El espacio seleccionado no puede ser asignado a la reservación porque no va a estar disponible. Seleccione otro',
                                    type: 'error',
                                    confirmButtonText: 'Sí',
                                });
                            }
                        }
                    }).load();
                });

                function setParkingLot(pl) {
                    $('#ParkingLotID').val(pl.id);
                    $('.parking-lot-id').html(pl.id);
                    $('#parking-modal').modal('hide');
                }
            });
        </script>
    End If


End Section