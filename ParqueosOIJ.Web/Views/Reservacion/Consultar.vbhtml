﻿@Imports ParqueosOIJ.Model
@ModelType CheckReservationStatusForm
@Code
    ViewData("Title") = "Consulta"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        <h1>@ViewBag.Title</h1>
        <p>Si al realizar la reservación ingresó su correo electrónico, cualquier cambio en el estado de la solicitud se notificará por ese medio. Si aún no ha realizado la solicitud puede hacerlo en la página de <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Nueva"})">Solicitud de reservación</a></p>
        @If ViewBag.FlashMessages IsNot Nothing Then
            For Each fm As FlashMessage In ViewBag.FlashMessages
                Html.RenderPartial("_FlashMessage", fm)
            Next
        End If

        @Using Html.BeginForm("Consultar", "Reservacion", FormMethod.Post, New With {.role = "form", .class = "text-left"})
            @Html.AntiForgeryToken()
            @<div class="row">
                <h4 class="underlined-header">Por cédula y placa</h4>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.VisitorDni, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.VisitorDni, New With {.class = "form-control", .placeholder = "Cédula del conductor"})
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.VehicleLicensePlate, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.VehicleLicensePlate, New With {.class = "form-control", .placeholder = "Placa del vehículo"})
                    </div>
                </div>
            </div>
            @<div class="row row-centered">
                <div class="col-xs-12">
                    <input type="submit" value="Consultar" class="btn btn-primary btn-lg pull-right" />
                </div>
            </div>
        End Using
    </div>
</div>
