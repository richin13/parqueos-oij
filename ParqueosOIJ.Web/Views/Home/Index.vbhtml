﻿@Code
    ViewData("Title") = "Parqueos OIJ"
    ViewData("Page") = "homepage"
End Code
@If Request.IsAuthenticated Then
    @<div class="row row-centered">
        @If ViewBag.FlashMessages IsNot Nothing Then
            For Each fm As FlashMessage In ViewBag.FlashMessages
                Html.RenderPartial("_FlashMessage", fm)
            Next
        End If
        <div class="col-xs-10 col-md-10 col-centered">
            <div id="parking-loader" class="loader loader-default is-active" data-text="Cargando información del parqueo" blink></div>
            <div id="paper" style="width:100%"></div>
        </div>
    </div>

    @<text>
        <div id="modal-container"></div>
    </text>

    @Section Scripts
        @Scripts.Render("~/bundles/timeago")
        @Scripts.Render("~/bundles/raphael")
        @Scripts.Render("~/bundles/handlebars")
        @Scripts.Render("~/bundles/parkings")

        <script>
            //pasar a otro archivo distinto
            $(document).ready(function () {
                var source = $("#modal-template").html();
                var template = Handlebars.compile(source);

                // Aquí se obtiene el id del parqueo
                // Por el momento sólo hay uno entonces se pone a la brava
                var parkingId = 1;

                var loader = new Parking({
                    parking: parkingId,
                    onRectClick: function (parkingLot) {
                        var html = template(parkingLot);
                        $('#modal-container').html(html);
                        var $modal = $('#parking-lot-modal');
                        $('time.timeago').timeago();
                        $modal.modal('show');
                    }
                }).load();
            });
        </script>

        <script id="modal-template" type="text/x-handlebars-template">
            <div class="modal fade text-left" tabindex="-1" role="dialog" id="parking-lot-modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Espacio #{{id}}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default">
                                <div class="panel-body text-left">
                                    <ul class="list-group main-list-group">
                                        <li class="list-group-item"><i class="fa fa-bookmark"></i> <b>Tipo de espacio:</b> {{type}}</li>
                                        <li class="list-group-item"><i class="fa fa-star"></i> <b>Estado del espacio:</b> {{status}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" Class="btn btn-danger" data-dismiss="modal"><i Class="fa fa-close"></i> Cerrar</button>
                            @If Roles.IsUserInRole("Admin") Then
                                @<a href="@Url.RouteUrl("Default", New With {.controller = "Admin", .action = "EditarParqueo"})/{{parking}}?espacio={{id}}" class="btn btn-default"><i class="fa fa-edit"></i> Editar</a>
                            End If

                            {{#if isAvailable}}
                            <a href="@Url.RouteUrl("Default", New With {.controller = "Ingresos", .action = "Nuevo"})/{{id}}" class="btn btn-primary"><i class="fa fa-plus-square"></i> Registrar ingreso</a>
                            {{else}}
                                {{#if hasAdmission}}
                                    <a href="@Url.RouteUrl("Default", New With {.controller = "Ingresos", .action = "Salida"})/{{id}}" class="btn btn-warning"><i class="fa fa-paper-plane"></i> Registrar salida</a>
                                {{/if}}
                            {{/if}}
                        </div>
                    </div>
                </div>
            </div>
        </script>

    End Section

Else
    @<div class="row row-centered">
        <div class="col-xs-10 col-centered text-center">
            <h1>Reservación</h1>
            <p>
                Se brinda la posibilidad de que un funcionario realiza una solicitud de reserva de espacio en el parqueo para algún visitante que vaya a necesitar el servicio. Esta solicitud deberá ser revisada por un administrador quién decidirá si la aprueba o no.
            </p>
            <hr />
            <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Nueva"})" class="btn btn-primary pull-right" type="button">Reservar espacio</a>
        </div>
    </div>
    @<div class="row row-centered">
        <div class="col-xs-10 col-centered text-center">
            <h1>Ya reservó?</h1>
            <p>
                ¿Realizó una solicitud previamente y le gustaría saber su estado? Aquí podrá encontrar el estado actual de la solicitud realizada y así poder confirmar con su visita si podrá hacer uso del parqueo institucional. <b>Tome en cuenta que</b> si la fecha y hora de entrada del visitante están prontos a cumplirse y la solicitud aún está pendiente lo mejor es comunicarse con el administrador del piso.
            </p>
            <hr />
            <a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Consultar"})" class="btn btn-primary pull-right" type="button">Consultar reservación</a>
        </div>
    </div>
End If