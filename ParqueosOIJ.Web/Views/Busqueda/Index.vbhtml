﻿@Imports ParqueosOIJ.Model
@ModelType ICollection(Of Admission)
@Code
    ViewData("Title") = "Búsqueda"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    <div class="col-xs-12 col-centered">
        <div class="panel panel-default">
            <div class="panel-body" id="search-results-container">
                <h2>Resultados de la búsqueda</h2>
                @If Model.Count > 0 Then
                    @<p>Se muestran los resultados de la búsqueda para el término "<b>@ViewBag.Q</b>"</p>
                End If
                <hr />
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group has-feedback pull-right">
                                <input type="text" class="search form-control" placeholder="Filtrar" style="max-width: 12em;" />
                                <span class="form-control-feedback">
                                    <i class="fa fa-filter"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                @If Model.Count > 0 Then
                    @<text>
                        <ul class="list list-unstyled text-left">
                            @For Each a As Admission In Model
                                @<li data-visitor-dni="@a.Visitors(0).Dni">
                                    <a href="@Url.RouteUrl("Default", New With {.controller = "Ingresos", .action = "Ver", .id = a.Id})" class="clickable-panel">
                                        <div class="col-xs-4">
                                            <div class="panel panel-default">
                                                <div class="panel-body text-left">
                                                    <h3>Ingreso #@a.Id</h3>
                                                    <hr />
                                                    <ul class="list-unstyled">
                                                        <li class="visitor-name"><b>Conductor: </b>@a.Visitors(0).Name</li>
                                                        <li class="vehicle-license-plate"><b>Placa del vehículo: </b>@a.Vehicle.LicensePlate</li>
                                                        <li><b>Espacio asignado: </b>#@a.ParkingLotID</li>
                                                        <li><b>Fecha de entrada: </b>@a.EntryDate.ToString("dd/MM/yyyy a la\s HH:mm")</li>
                                                        <li data-balloon-length="medium" data-balloon="Si el ingreso aún no se ha completado entonces esta fecha podría ser estimada" data-balloon-pos="up"><b>Fecha de salida: </b><i class="fa fa-info-circle"></i> @(If(IsNothing(a.DepartureDate), "Desconocida", a.DepartureDate.ToString()))</li>
                                                        <li><b>Tipo de ingreso: </b>@a.Vehicle.Type.ToString()</li>
                                                        <li><b>Estado: </b>@(If(a.Status = AdmissionStatus.Completada, "Completada", "Activa"))</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            Next
                        </ul>
                    </text>
                Else
                    @<text>
                        <div class="col-xs-12 page-content">
                            <div class="alert alert-info" data-dissmiss="alert">
                                <i class="fa fa-info-circle fa-3x"></i> <h3>No hay ningún resultado para con el término @ViewBag.Q</h3>
                            </div>
                        </div>
                    </text>
                End If
            </div>
        </div>
    </div>
</div>
@Section Scripts
    @Scripts.Render("~/bundles/list.js")
    <script>
        $(document).ready(function () {
            //
            var options = {
                valueNames: [
                    'visitor-name',
                    'vehicle-license-plate',
                    { data: ['visitor-dni'] }]
            };

            var userList = new List('search-results-container', options);
        });
    </script>
End Section