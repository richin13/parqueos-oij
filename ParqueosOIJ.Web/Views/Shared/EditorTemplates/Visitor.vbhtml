﻿@Imports ParqueosOIJ.Model
@ModelType Visitor

<div class="col-xs-12 col-sm-6">
    <div class="form-group">
        @Html.LabelFor(Function(m) m.Name)
        @Html.TextBoxFor(Function(m) m.Name, New With {.class = "form-control", .placeholder = "Nombre"})
    </div>
</div>
<div class="col-xs-4 col-sm-3 text-left">
    @Html.LabelFor(Function(m) m.Nationality)
    <div class="form-group">
        <label class="radio-inline">
            @Html.RadioButtonFor(Function(m) m.Nationality, NationalityType.Local, New With {.checked = "true"}) Nacional
        </label>
        <label class="radio-inline">
            @Html.RadioButtonFor(Function(m) m.Nationality, NationalityType.Foreigner) Extranjero
        </label>
    </div>
</div>
<div class="col-xs-8 col-sm-3">
    <div class="form-group">
        @Html.LabelFor(Function(m) m.Dni)
        @Html.TextBoxFor(Function(m) m.Dni, New With {.class = "form-control", .placeholder = "Cédula/Identificación"})
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="form-group">
        @Html.LabelFor(Function(m) m.PhoneNumber)
        @Html.TextBoxFor(Function(m) m.PhoneNumber, New With {.class = "form-control", .placeholder = "Teléfono (Opcional)"})
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="form-group">
        @Html.LabelFor(Function(m) m.Email)
        @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control", .placeholder = "Correo electrónico (Opcional)"})
    </div>
</div>
