﻿@Imports Microsoft.AspNet.Identity

<nav id="navbar-main" class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target=".colls" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Parqueos</a>
        </div>

        <div class="collapse navbar-collapse colls">
            @If Request.IsAuthenticated Then
                @<ul class="nav navbar-nav">
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Ingresos", .action = "Index"})">Ingresos</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true" aria-expanded="false">
                            Reservaciones <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Index"})">Ver reservaciones</a></li>
                            <li><a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Nueva"})">Crear</a></li>
                            <li><a href="@Url.RouteUrl("Default", New With {.controller = "Reservacion", .action = "Consultar"})">Consultar</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            Reportes <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="@Url.RouteUrl("Default", New With {.controller = "Reporte", .action = "General"})">General</a></li>
                            <li><a href="@Url.RouteUrl("Default", New With {.controller = "Reporte", .action = "ControlVisitas"})">Control de visitas</a></li>
                        </ul>
                    </li>
                </ul>
            End If
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden-xs hidden-sm"><a href="https://www.facebook.com/OIJpolicia/" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li class="hidden-xs hidden-sm"><a href="https://twitter.com/oij_Organismo" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                @If Request.IsAuthenticated Then
                    @<li>
                        @Using Html.BeginForm("Index", "Busqueda", FormMethod.Get, New With {.class = "navbar-form", .role = "search"})
                            @<div class="input-group" id="search-box-container">
                                <input type="text" class="form-control" placeholder="Buscar" name="q" id="search-box">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn"><span class="fa fa-search"></span></button>
                                </span>
                            </div>
                        End Using
                    </li>
                    @<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user"></i> <span class="hidden-lg">@User.Identity.Name</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Index"})"><i class="fa fa-user"></i> Perfil</a></li>
                            @If Roles.IsUserInRole("Admin") Then
                                @<li> <a href="@Url.RouteUrl("Default", New With {.controller = "Cuentas", .action = "Index"})"><i class="fa fa-users"></i> Cuentas</a></li>
                            End If
                            <li role="separator" class="divider"></li>
                            <li Class="logoff">
                                @Using Html.BeginForm("Salir", "Cuentas", New With {.ReturnUrl = ViewBag.ReturnUrl}, FormMethod.Post)
                            @Html.AntiForgeryToken()
                            @<button type="submit" class="btn btn-danger btn-block">Salir</button>
                                End Using
                            </li>
                        </ul>
                    </li>
                Else
                    @<li><a href="@Url.RouteUrl("Default", New With {.controller = "Cuentas", .action = "Ingresar"})">Ingresar</a></li>
                End If
            </ul>

        </div>
    </div>
</nav>
