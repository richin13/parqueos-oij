﻿@ModelType FlashMessage

<div class="alert alert-@Model.Type alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
    <span aria-hidden="true">&times;</span></button>
    <strong>@Model.Title</strong> @Model.Text
</div>