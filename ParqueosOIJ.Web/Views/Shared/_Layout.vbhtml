﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title</title>
    @Styles.Render("~/styles/main")
    @RenderSection("styles", required:=False)
</head>
<body id="@ViewBag.Page">
    <div class="page">
        @If ViewBag.Page = "homepage" Then
            @<div class="jumbotron">
                <div class="container">
                    <div class="message">
                        <img src="http://www.poder-judicial.go.cr/oij/templates/shaper_apps/images/logo.png"
                             alt="Poder Judicial">
                        <h2>Organismo de Investigación Judicial</h2>
                        <p title="Descripción">Sistema de administración de parqueos del <b>Organismo de Investigación Judicial</b></p>
                    </div>
                </div>
            </div>
        End If
        @Html.Partial("_MainNavbarPartial")
        <div id="content" class="">
            <div class="container">
                @RenderBody()
            </div>
        </div>
        <footer id="footer" class="footer">
            <div class="container-fluid">
                <div class="container navbar-links colls">
                    <ul class="nav">
                        <li> <a href="@Url.RouteUrl("Default", New With {.controller = "Home", .action = "Index"})"> Inicio </a></li>
                        <li> <a href="http://www.poder-judicial.go.cr/oij/" target="_blank"> O.I.J</a></li>
                        <li> <a href="http://www.poder-judicial.go.cr" target="_blank"> Poder Judicial</a></li>
                        <li> <a href="http://gob.go.cr/es/" target="_blank"> Gobierno Digital</a></li>
                    </ul>
                </div>
                <div class="container">
                    <small>Hecho con <i class="fa fa-html5 text-warning"></i>, <i class="fa fa-css3 text-info"></i> &amp; <i class="fa fa-heart text-danger"></i></small>
                </div>
            </div>
        </footer>
    </div>

    @Scripts.Render("~/bundles/site")
    @RenderSection("scripts", required:=False)
    @If ViewBag.Page = "homepage" Then
        @<script>
            $("#navbar-main").stick_in_parent();
        </script>
    End If
</body>
</html>
