﻿@ModelType LoginViewForm
@Code
    ViewBag.Title = "Ingreso al sistema"
    ViewBag.Page = "default"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    <div class="col-xs-6 col-centered text-center">
        <div class="panel panel-default panel-login">
            <div class="panel-body">
                <h1>@ViewBag.Title</h1>
                <p>Ingrese los detalles de su cuenta</p>
                <hr />
                @Using Html.BeginForm("Ingresar", "Cuentas", New With {.ReturnUrl = ViewBag.ReturnUrl}, FormMethod.Post, New With {.role = "form"})
                    @Html.AntiForgeryToken()
                    @<text>
                        @Html.ValidationSummary(True, "", New With {.class = "text-danger"})
                        <div class="row row-centered">
                            <div class="col-xs-12 col-sm-10 col-centered">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.Email, New With {.class = "control-label"})
                                    @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control", .placeholder = "Correo electrónico"})
                                    @Html.ValidationMessageFor(Function(m) m.Email, "", New With {.class = "help-block"})
                                </div>
                            </div>
                        </div>
                        <div class="row row-centered">
                            <div class="col-xs-12 col-sm-10 col-centered">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.Password, New With {.class = "control-label"})
                                    @Html.PasswordFor(Function(m) m.Password, New With {.class = "form-control", .placeholder = "Contraseña"})
                                    @Html.ValidationMessageFor(Function(m) m.Password, "", New With {.class = "help-block"})
                                </div>
                            </div>
                        </div>
                        <div class="row row-centered">
                            <div class="col-xs-10 col-centered">
                                <button type="submit" class="btn btn-primary pull-right">Entrar</button>
                            </div>
                        </div>
                    </text>
                End Using
            </div>
        </div>
    </div>
</div>
@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section
