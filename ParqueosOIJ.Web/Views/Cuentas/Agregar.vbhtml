﻿@ModelType AddAccountForm
@Code
    ViewData("Title") = "Agregar nuevo usuario"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    <div class="col-xs-12 col-sm-9 text-center">
        <div class="panel panel-default">
            @Using Html.BeginForm("Agregar", "Cuentas", FormMethod.Post, New With {.role = "form", .class = "text-left"})
                @<div class="panel-body">
                    <h1>@ViewBag.Title</h1>
                    <p>Los usuarios del sistema son capaces de realizar diversas acciones que influyen en el funcionamiento del sistema.</p>
                    <hr />
                    <div class="col-xs-12">
                        @Html.AntiForgeryToken()
                        @Html.ValidationSummary("", New With {.class = "text-danger"})
                        <div class="row">
                            <h4 class="underlined-header">Detalles del usuario</h4>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-user"></i>  @Html.LabelFor(Function(m) m.FirstName, New With {.class = "control-label"})
                                    @Html.TextBoxFor(Function(m) m.FirstName, New With {.class = "form-control", .placeholder = "Nombre"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.LastName, New With {.class = "control-label"})
                                    @Html.TextBoxFor(Function(m) m.LastName, New With {.class = "form-control", .placeholder = "Apellidos"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-lock"></i> @Html.LabelFor(Function(m) m.Password, New With {.class = "control-label"})
                                    @Html.PasswordFor(Function(m) m.Password, New With {.class = "form-control", .placeholder = "Contraseña"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.ConfirmPassword, New With {.class = ""})
                                    @Html.PasswordFor(Function(m) m.ConfirmPassword, New With {.class = "form-control", .placeholder = "Confirme la contraseña"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-envelope"></i> @Html.LabelFor(Function(m) m.Email, New With {.class = ""})
                                    @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control date-input", .placeholder = "Correo electrónico"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label><i class="fa fa-user-secret"></i> @Html.LabelFor(Function(m) m.Role)</label>
                                <div class="form-group">
                                    <label class="radio-inline">
                                        @Html.RadioButtonFor(Function(m) m.Role, AddAccountForm.RolesEnum.Admin, New With {.checked = True}) Administrador
                                    </label>
                                    <label class="radio-inline">
                                        @Html.RadioButtonFor(Function(m) m.Role, AddAccountForm.RolesEnum.Officer) Oficial de seguridad
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @<div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                </div>
            End Using
        </div>
    </div>

    <div class="col-xs-12 col-sm-3">
        <div class="panel panel-default">
            <div class="panel-body text-left">
                <h3 class="text-center">Acciones</h3>
                <hr />
                <ul class="list-unstyled">
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Cuentas", .action = "Index"})"><i class="fa fa-users"></i> Ver usuarios</a></li>
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Cuentas", .action = "Agregar"})" class="active"><i class="fa fa-plus"></i> Agregar usuario</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section