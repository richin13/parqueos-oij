﻿@Imports ParqueosOIJ.Model
@ModelType ICollection(Of User)
@Code
    ViewData("Title") = "Usuarios"
    ViewData("Page") = "admin"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    @If Not IsNothing(ViewBag.FlashMessages) Then
        For Each fm As FlashMessage In ViewBag.FlashMessages
            Html.RenderPartial("_FlashMessage", fm)
        Next
    End If
    <div class="col-xs-12 col-sm-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>@ViewBag.Title</h3>
                <hr />
                @for Each u As User In Model
                    @<text>
                        <div class="col-xs-12 col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>@u.FirstName @u.LastName</h4>
                                    <hr />
                                    <ul class="list-unstyled">
                                        <li><b>Rol:</b> @u.Role.Name</li>
                                        <li><b>Correo:</b> @u.Email</li>
                                        <li><b>Usuario desde:</b> @u.JoinDate.ToString("dd/MM/yyyy") (<time class="timeago" datetime="@u.JoinDate.ToString("yyyy-MM-dd HH:mm:ss")">@u.JoinDate</time>)</li>
                                    </ul>
                                </div>
                                <div class="panel-footer text-right">
                                    <a class="btn btn-danger btn-xs" href="@Url.RouteUrl("Default", New With {.controller = "Cuentas", .action = "Eliminar", .id = u.Id})"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </text>
                Next

            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3">
        <div class="panel panel-default">
            <div class="panel-body text-left">
                <h3 class="text-center">Acciones</h3>
                <hr />
                <ul class="list-unstyled">
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Cuentas", .action = "Index"})" class="active"><i class="fa fa-users"></i> Ver usuarios</a></li>
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Cuentas", .action = "Agregar"})"><i class="fa fa-plus"></i> Agregar usuario</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@Section Scripts
    @Scripts.Render("~/bundles/timeago")

    <script>
        $(document).ready(function () {
            $('time.timeago').timeago();
        })
    </script>
End Section