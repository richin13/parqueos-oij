﻿@Imports ParqueosOIJ.Model
@ModelType IEnumerable(Of Admission)

@Code
    Layout = "~/Views/Shared/_Layout.vbhtml"
    ViewData("Title") = "Report general"
    ViewData("Page") = "default"
End Code
@Section Styles
    @Styles.Render("~/styles/pickadate")
End Section
<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        <h1>Resultado de la busqueda de los reportes</h1>
    </div>
    <p>Se muestra la informacion de todas las visitas a corde con los datos ingresados.</p>
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Número</th>
                        <th>Compañía</th>
                        <th>Conductor</th>
                        <th>Vehículo</th>
                        <th>Motivo</th>
                        <th>Fecha Ingreso</th>
                        <th>Fecha Salida</th>
                    </tr>
                </thead>
                <tbody>

                    @For Each r As Admission In Model
                        @<text>
                            <tr data-href="@Url.RouteUrl("Default", New With {.controller = "Reporte", .action = "Ver", .id = r.Id})">
                                <td>@r.Id</td>
                                <td>@r.Company</td>
                                <td>@r.Visitors.Where(Function(v) v.IsDriver = True).First.Dni</td>
                                <td>@r.Vehicle.LicensePlate</td>
                                <td>@r.Reason</td>
                                <td>@r.EntryDate</td>
                                <td>@r.DepartureDate</td>
                            </tr>
                        </text>
                    Next
                </tbody>
            </table>
        </div>

    </div>
    <div Class="row row-centered">
        <div Class="col-xs-12 col-sm-10 col-centered">
            <div Class="form-group">
                <form action="@Url.Action("Export", "Reporte")" method="post">
                    <div style="visibility:hidden">
                        <input type="text" name="car" value="@ViewBag.car">
                        <input type="text" name="ced" value="@ViewBag.ced">
                        <input type="text" name="entryDate" value="@ViewBag.entryDate">
                        <input type="text" name="departureDate" value="@ViewBag.departureDate">
                    </div>
                    <input type="submit" value="Generar" Class="btn btn-success btn-lg" />
                </form>
                @*<a href="@Url.Action("Export", "Reporte")">Exportar Reporte</a>*@
                @*<input type="submit" value="Generar" Class="btn btn-success btn-lg" />*@
            </div>
        </div>
    </div>

    @Section Scripts
        @Scripts.Render("~/bundles/pickadate")
        <script>

            $('.date-input').pickadate({
                today: "Hoy",
                clear: "Limpiar",
                close: "Cerrar",
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                max: false,
                disable: [
                    1, 7
                ],
                closeOnClear: false
            });

        </script>
    End Section
</div>
