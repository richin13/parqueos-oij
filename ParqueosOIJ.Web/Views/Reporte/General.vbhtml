﻿@Imports ParqueosOIJ.Model
@ModelType ReporteForm

@Code
    Layout = "~/Views/Shared/_Layout.vbhtml"
    ViewData("Title") = "Report general"
    ViewData("Page") = "default"
End Code
@Section Styles
    @Styles.Render("~/styles/pickadate")
End Section
<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        <h1>Reporte General</h1>
        <p>Al solicitar un reporte general, desplegara toda la informacion de todos los movimientos realizados en el parqueo en un lapso de tiempo</p>

        @If Not String.IsNullOrEmpty(ViewBag.message) Then
            @<div class="alert @ViewBag.alert" role="alert">La solicitud de reservación @ViewBag.message</div>
        End If

        @If ViewBag.FlashMessages IsNot Nothing Then
            For Each fm As FlashMessage In ViewBag.FlashMessages
                Html.RenderPartial("_FlashMessage", fm)
            Next
        End If

        @Using Html.BeginForm("General", "Reporte", FormMethod.Post, New With {.role = "form", .class = "text-left"})
            @Html.AntiForgeryToken()

            @<div class="row">
                <h4 class="underlined-header">Indique el periodo de tiempo</h4>

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.EntryDate, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.EntryDate, New With {.class = "date-input form-control", .placeholder = "Fecha inicial"})
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.DepartureDate, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.DepartureDate, New With {.class = "date-input form-control", .placeholder = "Fecha final"})
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.car, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.car, New With {.class = "form-control", .placeholder = "Placa del vehículo"})
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        @Html.LabelFor(Function(m) m.ced, New With {.class = ""})
                        @Html.TextBoxFor(Function(m) m.ced, New With {.class = "form-control", .placeholder = "Cédula del conductor"})
                    </div>
                </div>
            </div>
            @<div class="row row-centered">
                <div class="col-xs-12">
                    <input type="submit" value="Consultar" class="btn btn-success btn-lg pull-right" />
                </div>
            </div>
        End Using
    </div>


    @Section Scripts
        @Scripts.Render("~/bundles/pickadate")
        <script>

            $('.date-input').pickadate({
                today: "Hoy",
                clear: "Limpiar",
                close: "Cerrar",
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                max: false,
                disable: [
                    1, 7
                ],
                closeOnClear: false
            });

        </script>
    End Section
</div>
