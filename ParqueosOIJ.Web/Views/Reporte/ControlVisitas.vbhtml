﻿@Imports ParqueosOIJ.Model
@ModelType ICollection(Of Admission)

@Code
    Layout = "~/Views/Shared/_Layout.vbhtml"
    ViewData("Title") = "Reporte de visitas."
    ViewData("Page") = "default"
End Code
<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        <h1>Reporte de visitas.</h1>
    </div>
    <p>Se muestra la informacion de todas las visitas realizadas durante el presente día.</p>

    @If ViewBag.FlashMessages IsNot Nothing Then
        For Each fm As FlashMessage In ViewBag.FlashMessages
            Html.RenderPartial("_FlashMessage", fm)
        Next
    End If

    <div class="col-xs-12">
        @If Not IsNothing(Model) Then
            @<text>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Número</th>
                                <th>Compañía</th>
                                <th>Conductor</th>
                                <th>Vehículo</th>
                                <th>Motivo</th>
                                <th>Fecha Ingreso</th>
                                <th>Fecha Salida</th>
                            </tr>
                        </thead>
                        <tbody>
                            @For Each r As Admission In Model
                            @<text>
                                <tr data-href="@Url.RouteUrl("Default", New With {.controller = "Reporte", .action = "Ver", .id = r.Id})">
                                    <td>@r.Id</td>
                                    <td>@r.Company</td>
                                    <td>@r.Visitors.Where(Function(v) v.IsDriver = True).First.Dni</td>
                                    <td>@r.Vehicle.LicensePlate</td>
                                    <td>@r.Reason</td>
                                    <td>@r.EntryDate</td>
                                    <td>@r.DepartureDate</td>
                                </tr>
                            </text>
                            Next
                        </tbody>
                    </table>
                </div>
            </text>
        End If

    </div>
    <div Class="row row-centered">
        <div Class="col-xs-12 col-sm-10 col-centered">
            <div Class="form-group">
                <form action="@Url.Action("Export", "Reporte")" method="post">
                    <div style="visibility:hidden">
                        <input type="text" name="car" value="@ViewBag.car">
                        <input type="text" name="ced" value="@ViewBag.ced">
                        <input type="text" name="entryDate" value="@ViewBag.entryDate">
                        <input type="text" name="departureDate" value="@ViewBag.departureDate">
                    </div>
                    <input type="submit" value="Generar" Class="btn btn-success btn-lg" />
                </form>
            </div>
        </div>
    </div>
</div>