﻿@Imports ParqueosOIJ.Model
@ModelType User
@Code
    ViewData("Title") = "Datos de usuario"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    @If ViewBag.FlashMessages IsNot Nothing Then
        For Each fm As FlashMessage In ViewBag.FlashMessages
            Html.RenderPartial("_FlashMessage", fm)
        Next
    End If
    <div class="col-xs-10 col-sm-9 text-center">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>@ViewBag.Title</h3>
                <hr />
                <div class="panel panel-default">
                    <div class="panel-body text-left">
                        <ul class="list-group main-list-group">
                            <li class="list-group-item"><i class="fa fa-user"></i> <b>Nombre:</b> @Model.FirstName @Model.LastName </li>
                            <li class="list-group-item"><i class="fa fa-envelope"></i> <b>Correo:</b> @Model.Email</li>
                            <li class="list-group-item"><i class="fa fa-calendar"></i> <b>Registrado desde:</b> @Model.JoinDate.ToString("dd/MM/yyyy") (<time class="timeago" datetime="@Model.JoinDate.ToString("yyyy-MM-dd HH:mm:ss")">@Model.JoinDate</time>)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3">
        <div class="panel panel-default">
            <div class="panel-body text-left">
                <h3 class="text-center">Acciones</h3>
                <hr />
                <ul class="list-unstyled">
                    <li class="active"><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Index"})"><i class="fa fa-user"></i>  Perfil</a></li>
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Editar"})"><i class="fa fa-edit"></i>  Editar</a></li>
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Contrasena"})"><i class="fa fa-lock"></i> Cambiar contraseña</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
@Section Scripts
    @Scripts.Render("~/bundles/timeago")
    <script>
        $(document).ready(function () {
            $('time.timeago').timeago();
        })
    </script>
End Section