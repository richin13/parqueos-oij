﻿@ModelType ChangePasswordViewModel
@Code
    ViewData("Title") = "Actualizar contraseña"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    <div class="col-xs-10 col-sm-9 text-center">
        <div class="panel panel-default">
            @Using Html.BeginForm("Contrasena", "Usuario", FormMethod.Post, New With {.class = "text-left", .role = "form"})
                @<div class="panel-body">
                    <h3>@ViewBag.Title</h3>
                    <hr />
                    <div Class="col-xs-12">
                        @Html.AntiForgeryToken()
                        @Html.ValidationSummary("", New With {.class = "text-danger"})

                        <div class="row">
                            <h4 class="underlined-header">Antigua contraseña</h4>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-user"></i>  @Html.LabelFor(Function(m) m.OldPassword, New With {.class = "control-label"})
                                    @Html.PasswordFor(Function(m) m.OldPassword, New With {.class = "form-control", .placeholder = "Nombre"})
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="underlined-header">Nueva contraseña</h4>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-user"></i>  @Html.LabelFor(Function(m) m.NewPassword, New With {.class = "control-label"})
                                    @Html.PasswordFor(Function(m) m.NewPassword, New With {.class = "form-control", .placeholder = "Nombre"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-user"></i>  @Html.LabelFor(Function(m) m.ConfirmPassword, New With {.class = "control-label"})
                                    @Html.PasswordFor(Function(m) m.ConfirmPassword, New With {.class = "form-control", .placeholder = "Nombre"})
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @<div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-lock"></i> Cambiar contraseña</button>
                </div>
            End Using
        </div>
    </div>

    <div class="col-xs-12 col-sm-3">
        <div class="panel panel-default">
            <div class="panel-body text-left">
                <h3 class="text-center">Acciones</h3>
                <hr />
                <ul class="list-unstyled">
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Index"})"><i class="fa fa-user"></i>  Perfil</a></li>
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Editar"})"><i class="fa fa-edit"></i>  Editar</a></li>
                    <li class="active"><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Contrasena"})"><i class="fa fa-lock"></i> Cambiar contraseña</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section