﻿@Imports ParqueosOIJ.Model
@ModelType User
@Code
    ViewData("Title") = "Editar los datos de usuario"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    <div class="col-xs-12 col-sm-9 text-center">
        <div class="panel panel-default">
            @Using Html.BeginForm("Editar", "Usuario", FormMethod.Post, New With {.class = "text-left", .role = "form"})
                @<div class="panel-body">
                    <h3>@ViewBag.Title</h3>
                    <hr />
                    <div Class="col-xs-12">
                        @Html.AntiForgeryToken()
                        @Html.ValidationSummary("", New With {.class = "text-danger"})

                        <div class="row">
                            <h4 class="underlined-header">Datos generales</h4>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-user"></i>  @Html.LabelFor(Function(m) m.FirstName, New With {.class = "control-label"})
                                    @Html.TextBoxFor(Function(m) m.FirstName, New With {.class = "form-control", .placeholder = "Nombre"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.LastName, New With {.class = "control-label"})
                                    @Html.TextBoxFor(Function(m) m.LastName, New With {.class = "form-control", .placeholder = "Apellidos"})
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="underlined-header">Datos de contacto</h4>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <i class="fa fa-envelope"></i>  @Html.LabelFor(Function(m) m.Email, New With {.class = "control-label"})
                                    @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control", .placeholder = "Correo electrónico"})
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @<div class="panel-footer text-right">
                     <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar cambios</button>
                </div>
            End Using
        </div>
    </div>

    <div class="col-xs-12 col-sm-3">
        <div class="panel panel-default">
            <div class="panel-body text-left">
                <h3 class="text-center">Acciones</h3>
                <hr />
                <ul class="list-unstyled">
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Index"})"><i class="fa fa-user"></i>  Perfil</a></li>
                    <li class="active"><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Editar"})"><i class="fa fa-edit"></i>  Editar</a></li>
                    <li><a href="@Url.RouteUrl("Default", New With {.controller = "Usuario", .action = "Contrasena"})"><i class="fa fa-lock"></i> Cambiar contraseña</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
