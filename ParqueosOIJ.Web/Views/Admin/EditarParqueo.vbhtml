﻿@Imports ParqueosOIJ.Model
@ModelType ParkingLot
@Code
    ViewData("Title") = "Editar el parqueo"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div class="row row-centered">
    @If Not IsNothing(ViewBag.FlashMessages) Then
        For Each fm As FlashMessage In ViewBag.FlashMessages
            Html.RenderPartial("_FlashMessage", fm)
        Next
    End If
    <div id="parking-loader" class="loader loader-default is-active" data-text="Cargando información del parqueo" blink></div>
    <div class="col-xs-12 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>@ViewBag.Title</h3>
                <hr />
                @if Model.Status = StatusEnum.Ocupado Then
                    @<div class="alert alert-warning">
                        <h4><i class="fa fa-warning"></i> Editar el estado de un campo ocupado podría arrojar resultados inesperados.</h4>
                    </div>
                End If
                <div id="paper" style="width:100%"></div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-4">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body text-left">
                    <h3 class="text-center">
                        Editando el espacio #<span id="parking-lot-id" data-parking-lot-id="@Model.Id">@Model.Id</span>
                    </h3>
                    <hr />
                    @Using Html.BeginForm("EditarParqueo", "Admin", New With {.id = ViewBag.Parking.Id}, FormMethod.Post, New With {.role = "form", .id = "edit-parking-form"})
                        @<text>
                            @Html.AntiForgeryToken()
                            <input type="hidden" id="Id" name="Id" value="@Model.Id" />
                            @Html.HiddenFor(Function(pl) pl.X)
                            @Html.HiddenFor(Function(pl) pl.Y)
                            @Html.HiddenFor(Function(pl) pl.Height)
                            @Html.HiddenFor(Function(pl) pl.Width)
                            <div class="form-group">
                                @Html.LabelFor(Function(pl) pl.Type, New With {.class = "control-label"})
                                @Html.EnumDropDownListFor(Function(pl) pl.Type, Model.Type, New With {.class = "form-control"})
                            </div>

                            <div class="form-group">
                                @Html.LabelFor(Function(pl) pl.Status, New With {.class = "control-label"})
                                @Html.EnumDropDownListFor(Function(pl) pl.Status, Model.Status, New With {.class = "form-control"})
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-info disabled" id="reset-button"><i class="fa fa-undo"></i> Restablecer posición</button>
                            </div>
                        </text>
                    End Using
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary @(If(Model.Id = 0, "disabled", ""))" id="edit-parking-save-button"><i class="fa fa-save"></i> Guardar</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body text-left">
                    <h3 class="text-center">Información</h3>
                    <hr />
                    <ul class="list-unstyled">
                        <li><b>Nombre del parqueo: </b>@ViewBag.Parking.Name</li>
                        <li><b>Capacidad del parqueo: </b>@ViewBag.Parking.Capacity</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@Section Scripts
    @Scripts.Render("~/bundles/raphael")
    @Scripts.Render("~/bundles/parkings")
    @Scripts.Render("~/bundles/edit-parkings")

End Section
