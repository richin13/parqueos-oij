﻿@Imports ParqueosOIJ.Model
@ModelType ICollection(Of Admission)
@Code
    Layout = "~/Views/Shared/_Layout.vbhtml"
    ViewData("Title") = "Lista de Ingresos"
End Code

<div class="row row-centered">
    <div class="col-xs-12 col-centered text-center">
        <div class="panel panel-default page-content">
            <div class="panel-body" id="admissions-table-container">
                <h2>@ViewBag.Title</h2>
                <p>Se muestra la lista de todos los ingresos activos, si desea ver un histórico de los ingresos al parqueo diríjase a la sección de <a href="@Url.RouteUrl("Default", New With {.controller = "Reportes", .action = "Ingresos"})">Reportes</a></p>
                <hr />
                @If Model.Count > 0 Then
                    @<text>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group has-feedback pull-right" data-balloon-length="medium" data-balloon="Puede usar: Nombre del conductor, Cédula de conductor, Placa del vehículo, Número de espacio de parqueo" data-balloon-pos="up">
                                        <input type="text" class="search form-control" placeholder="Filtrado rápido" style="max-width: 12em;" />
                                        <span class="form-control-feedback">
                                            <i class="fa fa-filter"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="list list-unstyled">
                            @For Each a As Admission In Model
                                @<li>
                                    <a href="@Url.RouteUrl("Default", New With {.controller = "Ingresos", .action = "Ver", .id = a.Id})" class="clickable-panel">
                                        <div class="col-xs-4">
                                            <div class="panel panel-default">
                                                <div class="panel-body text-left">
                                                    <h3>Ingreso #@a.Id</h3>
                                                    <hr />
                                                    <ul class="list-unstyled">
                                                        <li class="visitor-name"><b>Conductor: </b>@a.Visitors(0).Name</li>
                                                        <li class="vehicle-license-plate"><b>Placa del vehículo: </b>@a.Vehicle.LicensePlate</li>
                                                        <li class="parking-lot-id"><b>Espacio asignado: </b>#@a.ParkingLotID</li>
                                                        <li><b>Fecha de entrada: </b>@a.EntryDate.ToString("dd/MM/yyyy a la\s HH:mm")</li>
                                                        <li data-balloon-length="medium" data-balloon="En ocasiones podría ser estimada" data-balloon-pos="up"><b>Fecha de salida: </b><i class="fa fa-info-circle"></i> @(If(IsNothing(a.DepartureDate), "Desconocida", a.DepartureDate.ToString()))</li>
                                                        <li><b>Tipo de ingreso: </b>@a.Vehicle.Type.ToString()</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            Next
                        </ul>
                    </text>
                Else
                    @<text>
                        <div class="col-xs-12 page-content">
                            <div class="alert alert-info" data-dissmiss="alert">
                                <i class="fa fa-info-circle fa-3x"></i> <h3>No hay ningún ingreso activo</h3>
                            </div>
                        </div>
                    </text>
                End If
            </div>
        </div>
    </div>
</div>
@Section Scripts
    @Scripts.Render("~/bundles/list.js")
    <script>
        $(document).ready(function () {
            //
            var options = {
                valueNames: [
                    'visitor-name',
                    'vehicle-license-plate',
                    'parking-lot-id',
                    { data: ['visitor-dni'] }]
            };

            var userList = new List('admissions-table-container', options);
        });
    </script>
End Section