﻿@Imports ParqueosOIJ.Model
@ModelType Admission
@Code
    ViewData("Title") = "Detalles del Ingreso #" + Model.Id.ToString()
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@Section Styles
    @Styles.Render("~/styles/swal")
End Section
<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        @If ViewBag.FlashMessages IsNot Nothing Then
            For Each fm As FlashMessage In ViewBag.FlashMessages
                Html.RenderPartial("_FlashMessage", fm)
            Next
        End If
        <div class="panel panel-default" id="admission-panel" data-id="@Model.Id">
            <div class="panel-body">
                <h1>@ViewBag.Title</h1>
                <p>Se muestran los datos de la solicitud número @Model.Id</p>
                <hr />
                <ul class="list-group main-list-group text-left">
                    <li class="list-group-item"><i class="fa fa-hashtag"></i> <b>Número:</b> @Model.Id</li>
                    <li class="list-group-item"><i class="fa fa-tag"></i> <b>Estado:</b> @Model.Status</li>
                    @For Each v In Model.Visitors
                        @<text>
                            <li class="list-group-item">
                                <i class="fa fa-user"></i> @(If(v.IsDriver, "Conductor", "Ocupante"))
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <ul class="list-group">
                                            <li class="list-group-item"><b>Cédula:</b> @Model.Visitors(0).Dni</li>
                                            <li class="list-group-item"><b>Nombre:</b> @Model.Visitors(0).Name</li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <ul class="list-group">
                                            <li class="list-group-item"><b>Número de teléfono:</b> @Model.Visitors(0).PhoneNumber</li>
                                            <li class="list-group-item"><b>Correo electrónico:</b> @Model.Visitors(0).Email</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </text>
                    Next
                    <li class="list-group-item">
                        <i class="fa fa-car"></i> Vehículo
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Placa o número</b> @Model.Vehicle.LicensePlate</li>
                                </ul>
                            </div>
                            @If Model.Vehicle.Type = VehicleType.Visita Then
                            @<text>
                                <div class="col-xs-12 col-sm-6">
                                    <ul class="list-group">
                                        <li class="list-group-item"><b>Marca</b> @Model.Vehicle.Brand</li>
                                        <li class="list-group-item"><b>Marca</b> @Model.Vehicle.Color</li>
                                    </ul>
                                </div>
                            </text>
                            End If
                        </div>
                    </li>
                    <li Class="list-group-item"><i class="fa fa-th"></i> Espacio: #@Model.ParkingLotID</li>
                    <li class="list-group-item"><i class="fa fa-calendar"></i> <b>Fecha de ingreso</b> @Model.EntryDate.ToString("dd/MM/yyyy a la\s HH:mm") (<time class="timeago" datetime="@Model.EntryDate.ToString("yyyy-MM-dd HH:mm:ss")">@Model.EntryDate</time>)</li>
                    <li class="list-group-item">
                        <i class="fa fa-calendar"></i> <b>Fecha de salida @(If(Model.Status = AdmissionStatus.Incompleta, "(estimada)", "")):</b>
                        @If IsNothing(Model.DepartureDate) Then
                        @<text>Desconocida.</text>
                        Else
                        @<text>
                            @Model.DepartureDate.ToString()
                        </text>
                        End If
                    </li>
                    <li class="list-group-item"><i class="fa fa-id-badge"></i> <b>Registrado por el usuario: </b> @Model.User.FirstName @Model.User.LastName <a href="mailto:@Model.User.Email"><i class="fa fa-external-link-square"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-footer text-right">
            <a class="btn btn-default hidden-sm" href="@Url.RouteUrl("Default", New With {.controller = "Ingresos", .action = "Index"})"><i class="fa fa-arrow-left"></i> Regresar</a>
            @If Model.Status = AdmissionStatus.Incompleta Then
            @<a class="btn btn-primary" href="@Url.RouteUrl("Default", New With {.controller = "Ingresos", .action = "Salida", .id = Model.ParkingLotID})"><i class="fa fa-check"></i> Registrar salida</a>
            End If

        </div>
    </div>

</div>
</div>

@Section Scripts
    @Scripts.Render("~/bundles/swal")
    @Scripts.Render("~/bundles/timeago")
    <script>
        $(document).ready(function () {
            $('time.timeago').timeago();

            $('#admission-panel').find('.delete').click(function (e) {
                swal({
                    title: 'Eliminar el ingreso',
                    text: '¿Está seguro que desea eliminar el ingreso? La operación es irreversible',
                    type: 'question',
                    showCancelButton: true,
                    cancelButtonText: 'No',
                    confirmButtonText: 'Sí',
                    showCloseButton: true
                });
            });

            $('#admission-panel').find('.accept').click(function (e) {
                //
                swal('i mean it', 'i lied!');
            });
        });

    </script>
End Section