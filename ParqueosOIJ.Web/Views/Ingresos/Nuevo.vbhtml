﻿@Imports ParqueosOIJ.Model
@ModelType AdmissionForm
@Code
    ViewData("Title") = "Registrar nuevo ingreso"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@Section Styles
    @Styles.Render("~/styles/pickadate")
End Section

<div class="row row-centered">
    <div class="col-xs-10 col-centered text-center">
        <h2>Registrar ingreso al parqueo</h2>
        @If ViewBag.FlashMessages IsNot Nothing Then
            For Each fm As FlashMessage In ViewBag.FlashMessages
                Html.RenderPartial("_FlashMessage", fm)
            Next
        End If
        @Using Html.BeginForm("Nuevo", "Ingresos", FormMethod.Post, New With {.role = "form", .class = "text-left"})
            @Html.AntiForgeryToken()
            @Html.ValidationSummary("", New With {.class = "text-danger"})
            @<text>
                <div class="row">
                    <h4 class="underlined-header">Datos del vehículo</h4>
                    <div class="col-xs-12 col-sm-6">
                        <label>Tipo de vehículo</label>
                        <p class="form-control-static text-center">
                            @Html.HiddenFor(Function(a) a.VehicleType)
                            <label class="label label-info">
                                @If Model.VehicleType = VehicleType.Jefatura Then
                                    @<text>Jefatura</text>
                                ElseIf Model.VehicleType = VehicleType.Oficial Then
                                    @<text>Oficial</text>
                                Else
                                    @<text>Visitante</text>
                                End If
                            </label>
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group" id="license-plate-group">
                            @Html.LabelFor(Function(m) m.VehicleLp)
                            @Html.TextBoxFor(Function(m) m.VehicleLp, New With {.class = "form-control", .placeholder = "Número de vehículo"})
                        </div>
                    </div>
                </div>
                @If Model.VehicleType = VehicleType.Visita Then
                    @<text>
                        <div class="row">
                            <h4 class="underlined-header">Detalles del vehículo</h4>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.VehicleBrand)
                                    @Html.TextBoxFor(Function(m) m.VehicleBrand, New With {.class = "form-control", .placeholder = "Modelo o Marca"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.VehicleColor)
                                    @Html.TextBoxFor(Function(m) m.VehicleColor, New With {.class = "form-control", .placeholder = "Color"})
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="underlined-header">Detalles de la visita</h4>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="Company" data-balloon-length="medium" data-balloon="Compañía o institución que representa" data-balloon-pos="up">
                                        Compañía
                                        <i class="fa fa-info-circle"></i>
                                    </label>
                                    @Html.TextBoxFor(Function(m) m.Company, New With {.class = "form-control", .placeholder = "Compañía/Institución"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label for="DepartureDate" data-balloon-length="medium" data-balloon="Representa una fecha tentativa de salida" data-balloon-pos="up">
                                        Fecha de salida
                                        <i class="fa fa-info-circle"></i>
                                    </label>
                                    @Html.TextBoxFor(Function(m) m.DepartureDate, New With {.class = "form-control date-input", .placeholder = "Fecha de salida", .readonly = ""})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.DepartureHour, New With {.class = ""})
                                    @Html.TextBoxFor(Function(m) m.DepartureHour, New With {.class = "form-control hour-input", .placeholder = "Hora tentativa de salida"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    @Html.LabelFor(Function(m) m.Reason)
                                    @Html.TextAreaFor(Function(m) m.Reason, New With {.class = "form-control", .placeholder = "Razón de la visita"})
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <Label> Funcionario a quién visita</Label>
                                    <input type="text" id="EmployeeName" class="form-control" placeholder="Nombre del funcionario" autocomplete="off" />
                                    @Html.HiddenFor(Function(m) m.Employee)
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-centered">
                                <h2 class="text-center"><small>Piso: </small><span id="EmployeeFloor">?</span></h2>
                            </div>
                        </div>
                    </text>
                End If
                <div class="row">
                    <h4 class="underlined-header">Datos del conductor</h4>
                    <div class="col-xs-8 col-sm-3">
                        <div class="form-group">
                            @Html.LabelFor(Function(m) m.Visitors(0).Dni)
                            @Html.TextBoxFor(Function(m) m.Visitors(0).Dni, New With {.class = "form-control", .placeholder = "Cédula/Identificación del conductor"})
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-3 text-left">
                        <Label for="nationality-0">Nacionalidad</Label>
                        <div class="form-group">
                            <Label class="radio-inline">
                                @Html.RadioButtonFor(Function(m) m.Visitors(0).Nationality, NationalityType.Local, New With {.checked = True}) Nacional
                            </Label>
                            <Label class="radio-inline">
                                @Html.RadioButtonFor(Function(m) m.Visitors(0).Nationality, NationalityType.Foreigner) Extranjero
                            </Label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            @Html.LabelFor(Function(m) m.Visitors(0).Name)
                            @Html.TextBoxFor(Function(m) m.Visitors(0).Name, New With {.class = "form-control", .placeholder = "Nombre completo del conductor"})
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            @Html.LabelFor(Function(m) m.Visitors(0).PhoneNumber)
                            @Html.TextBoxFor(Function(m) m.Visitors(0).PhoneNumber, New With {.class = "form-control", .placeholder = "Teléfono (Opcional)"})
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            @Html.LabelFor(Function(m) m.Visitors(0).Email)
                            @Html.TextBoxFor(Function(m) m.Visitors(0).Email, New With {.class = "form-control", .placeholder = "Correo electrónico (Opcional)"})
                        </div>
                    </div>
                </div>
                <div class="row row-centered" id="extra-visitors-row" data-count="@(Model.Visitors.Count-1)">
                    <h4 class="underlined-header text-left">Ocupantes extra</h4>
                    <div class="col-xs-12 col-sm-12 col-centered" id="extra-visitors-body">
                        @If Model.Visitors.Count > 1 Then
                            For i = 1 To Model.Visitors.Count - 1
                                Dim index As Integer
                                index = i
                        @<text>
                            <div class="panel panel-default" id="panel-visitor-@index">
                                <div class="panel-body">
                                    <div class="col-xs-8 col-sm-3">
                                        <div class="form-group">
                                            @Html.LabelFor(Function(m) m.Visitors(index).Dni)
                                            @Html.TextBoxFor(Function(m) m.Visitors(index).Dni, New With {.class = "form-control", .placeholder = "Cédula/Identificación del visitante"})
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-3 text-left">
                                        <label for="nationality-0">Nacionalidad</label>
                                        <div class="form-group">
                                            <label class="radio-inline">
                                                @Html.RadioButtonFor(Function(m) m.Visitors(index).Nationality, NationalityType.Local, New With {.checked = True}) Nacional
                                            </label>
                                            <label class="radio-inline">
                                                @Html.RadioButtonFor(Function(m) m.Visitors(index).Nationality, NationalityType.Foreigner) Extranjero
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            @Html.LabelFor(Function(m) m.Visitors(index).Name)
                                            @Html.TextBoxFor(Function(m) m.Visitors(index).Name, New With {.class = "form-control", .placeholder = "Nombre completo del visitante"})
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            @Html.LabelFor(Function(m) m.Visitors(index).PhoneNumber)
                                            @Html.TextBoxFor(Function(m) m.Visitors(index).PhoneNumber, New With {.class = "form-control", .placeholder = "Teléfono (Opcional)"})
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            @Html.LabelFor(Function(m) m.Visitors(index).Email)
                                            @Html.TextBoxFor(Function(m) m.Visitors(index).Email, New With {.class = "form-control", .placeholder = "Correo electrónico (Opcional)"})
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right">
                                    <a href="#" class="text-danger visitor-delete hidden" id="visitor-delete-@index" data-target-delete="#panel-visitor-@index"><i class="fa fa-trash"></i> Quitar</a>
                                </div>
                            </div>
                        </text>
                            Next
                        End If
                        <div id="extra-visitors-alert" class="alert alert-info hidden" data-dissmiss="alert">
                            <i class="fa fa-info-circle fa-3x" title="Puede agregarlos haciendo click en el link inferior '+ Agregar ocupantes'"></i> <h3>No hay ocupantes extra</h3>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-left">
                        <a href="#" id="extra-visitor-link"><i class="fa fa-plus"></i> Agregar ocupante</a>
                    </div>
                </div>
                <div class="row row-centered">
                    <h4 class="underlined-header text-left"></h4>
                    <div class="col-xs-12 col-sm-12">
                        <div class="form-group">
                            <input type="submit" value="Ingresar" class="btn btn-success btn-lg pull-right" />
                        </div>
                    </div>
                </div>
            </text>
        End Using
    </div>
</div>

@Section Scripts
    @Scripts.Render("~/bundles/handlebars")
    @Scripts.Render("~/bundles/jqueryval")
    @Scripts.Render("~/bundles/admissions")
    @Scripts.Render("~/bundles/pickadate")
    <script>
        $(document).ready(function () {
            $('.date-input').pickadate({
                today: "Hoy",
                clear: "Limpiar",
                close: "Cerrar",
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                min: new Date(),
                max: false,
                disable: [ // non-working days
                    1, 7 // sats and sundays
                ],
                closeOnClear: false
            });

            $('.hour-input').pickatime({
                disable: [{ // non-working hours
                    from: [0, 0],
                    to: [7, 30]
                }, {
                    from: [17, 0],
                    to: [23, 30]
                }]
            });
        });
    </script>

    <script id="visitor-template" type="text/x-handlebars-template">
        <div class="panel panel-default" id="panel-visitor-{{index}}">
            <div class="panel-body">
                <div class="col-xs-8 col-sm-3">
                    <div class="form-group">
                        <label for="Visitors_{{index}}__Dni">Cédula</label>
                        <input type="text" name="Visitors[{{index}}].Dni" id="Visitors_{{index}}__Dni" class="form-control" placeholder="Cédula/Identificación del visitante">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-3 text-left">
                    <label for="Visitors_{{index}}__Nationality">Nacionalidad</label>
                    <div class="form-group">
                        <label class="radio-inline">
                            <input type="radio" name="Visitors[{{index}}].Nationality" value="Local" checked> Nacional
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="Visitors[{{index}}].Nationality" value="Foreigner"> Extranjero
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="Visitors_{{index}}__Name">Nombre</label>
                        <input type="text" id="Visitors_{{index}}__Name" name="Visitors[{{index}}].Name" class="form-control" placeholder="Nombre completo del visitante" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="Visitors_{{index}}__PhoneNumber">Teléfono</label>
                        <input type="text" name="Visitors[{{index}}].PhoneNumber" id="Visitors_{{index}}__PhoneNumber" class="form-control" placeholder="Teléfono (Opcional)">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="Visitors_{{index}}__Email">Correo electrónico</label>
                        <input type="text" name="Visitors[{{index}}].Email" id="Visitors_{{index}}__Email" class="form-control" placeholder="Correo">
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <a href="#" class="text-danger visitor-delete hidden" id="visitor-delete-{{index}}" data-target-delete="#panel-visitor-{{index}}"><i class="fa fa-trash"></i> Quitar</a>
            </div>
        </div>
    </script>


End Section
