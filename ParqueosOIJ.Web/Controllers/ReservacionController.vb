﻿Imports Microsoft.AspNet.Identity
Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

Namespace Controllers

    <Authorize(Roles:="Admin")>
    Public Class ReservacionController
        Inherits Controller

        Private Property _serv As ReservationService

        Public Sub New()
            MyBase.New()
            _serv = New ReservationService()
        End Sub

        '
        ' GET: /Reservacion/Index
        <AllowAnonymous>
        Function Index(filter As String) As ActionResult
            Dim reservations As ICollection(Of Reservation)

            If filter = "Pendientes" Then
                reservations = _serv.FindByStatus(ReservationStatus.Pendiente)
            ElseIf filter = "Aprobadas" Then
                reservations = _serv.FindByStatus(ReservationStatus.Aprobada)
            ElseIf filter = "Rechazadas" Then
                reservations = _serv.FindByStatus(ReservationStatus.Rechazada)
            Else
                reservations = _serv.FindAll()
            End If

            Return View(reservations)
        End Function

        '
        ' GET: /Reservacion/Nueva
        <AllowAnonymous>
        Function Nueva() As ActionResult
            Return View()
        End Function

        '
        ' POST: /Reservacion/Nueva
        <AllowAnonymous>
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function Nueva(model As ReservationForm) As ActionResult
            If Not ModelState.IsValid Then
                Return View(model)
            End If

            Dim reservation As Reservation
            reservation = New Reservation With {
                .Reason = model.Reason,
                .Status = ReservationStatus.Pendiente,
                .EntryDate = DateUtils.MergeDate(model.EntryDateV, model.EntryHour),
                .DepartureDate = DateUtils.MergeDate(model.DepartureDateV, model.DepartureHour),
                .Employee = New Employee With {.Dni = model.Employee},
                .Visitor = New Visitor With {.Dni = model.Visitor, .Name = "Default", .Nationality = NationalityType.Local},
                .Vehicle = New Vehicle With {.LicensePlate = model.Vehicle, .Type = VehicleType.Visita, .Brand = "Ws", .Color = "Negro"}}

            reservation = _serv.Save(reservation)

            If reservation IsNot Nothing Then
                Return RedirectToAction("Ver", New With {.id = reservation.Id})
            End If

            Return View()
        End Function

        '
        ' GET: /Reservacion/Consultar
        <AllowAnonymous>
        Function Consultar() As ActionResult
            Return View()
        End Function

        '
        ' POST: /Reservacion/Consultar
        <AllowAnonymous>
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function Consultar(model As CheckReservationStatusForm) As ActionResult
            Try
                Dim reservation = _serv.FindLastByLicensePlateAndDni(model.VehicleLicensePlate, model.VisitorDni)
                Return RedirectToAction("Ver", New With {.id = reservation.Id})
            Catch ex As NotFoundException
                ViewData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                                     .Title = "Error!",
                                                                     .Text = ex.Message,
                                                                     .Type = "danger"})
            End Try

            Return View(model)
        End Function


        '
        ' GET: /Reservacion/Ver/id
        <AllowAnonymous>
        Function Ver(id As Integer) As ActionResult
            Dim reservation As Reservation
            reservation = _serv.Find(id)

            If reservation Is Nothing Then
                Return HttpNotFound("No existe la reservación")
            End If

            Return View(reservation)
        End Function

        ' POST /Reservacion/Ver/{id}
        <HttpPost>
        Function Ver(id As Integer, model As Reservation) As ActionResult
            If Not ModelState.IsValid Then
                Return View(model)
            End If

            model.UserID = User.Identity.GetUserId()
            Try
                _serv.Update(model)
                model = _serv.Find(id)

                ViewBag.FlashMessages = FmGenerator.GenerateList(New FlashMessage With {
                                                                 .Type = "success",
                                                                 .Title = "Éxito!",
                                                                 .Text = "La reservación se actualizó correctamente"})
            Catch ex As Exception
                ViewBag.FlashMessages = FmGenerator.GenerateList(New FlashMessage With {
                                 .Type = "danger",
                                 .Title = "Error!",
                                 .Text = ex.Message})
            End Try

            Return View(model)
        End Function
    End Class
End Namespace