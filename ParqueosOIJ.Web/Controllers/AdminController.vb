﻿Imports System.Web.Mvc
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

Namespace Controllers
    <Authorize>
    Public Class AdminController
        Inherits Controller

        Private _serv As ParkingService
        Private _plServ As ParkingLotService

        Public Sub New()
            _serv = New ParkingService()
            _plServ = New ParkingLotService()
        End Sub

        ' GET: /Admin/EditarParqueo/{parkingId}?espacio={parkingLotId}
        Function EditarParqueo(id As Integer, espacio As Integer) As ActionResult
            If Not IsNothing(TempData("FlashMessages")) Then
                ViewData("FlashMessages") = TempData("FlashMessages")
            End If

            Try
                ViewData("Parking") = _serv.Find(id)

                Dim parkingLot As ParkingLot = _plServ.Find(espacio)

                Return View(parkingLot)
            Catch ex As Exception
                ViewData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                    .Title = "Error!",
                    .Text = ex.Message,
                    .Type = "danger"})
                Return RedirectToAction("Index", "Home")
            End Try
        End Function

        ' POST: /Admin/EditarParqueo/{parkingId}
        <HttpPost>
        Function EditarParqueo(id As Integer, model As ParkingLot) As ActionResult
            If Not ModelState.IsValid Then
                Return View(model)
            End If
            Dim parkingLot As ParkingLot = New ParkingLot()
            Try
                parkingLot = _plServ.Save(model)

                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                    .Title = "Éxito!",
                    .Text = "Los cambios se guardaron satisfactoriamente",
                    .Type = "success"})
            Catch ex As Exception
                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                    .Title = "Error!",
                    .Text = ex.Message,
                    .Type = "danger"})
            End Try

            Return RedirectToAction("EditarParqueo", New With {.id = id, .espacio = model.Id})
        End Function
    End Class
End Namespace