﻿Public Class HomeController
    Inherits Controller

    Function Index() As ActionResult
        If Not IsNothing(TempData("FlashMessages")) Then
            ViewData("FlashMessages") = TempData("FlashMessages")
        End If

        Return View()
    End Function

End Class
