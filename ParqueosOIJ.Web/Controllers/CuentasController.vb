﻿Imports System.Threading.Tasks
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security
Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

Namespace Controllers
    Public Class CuentasController
        Inherits Controller

        Private _signInManager As ApplicationSignInManager
        Private _userManager As ApplicationUserManager
        Private _roleManager As ApplicationRoleManager

        Private _serv As UserService

        Public Sub New()
            _serv = New UserService()
        End Sub

        Public Sub New(appUserMan As ApplicationUserManager, signInMan As ApplicationSignInManager)
            UserManager = appUserMan
            SignInManager = signInMan
        End Sub

        Public Property SignInManager() As ApplicationSignInManager
            Get
                Return If(_signInManager, HttpContext.GetOwinContext().[Get](Of ApplicationSignInManager)())
            End Get
            Private Set
                _signInManager = Value
            End Set
        End Property

        Public Property UserManager() As ApplicationUserManager
            Get
                Return If(_userManager, HttpContext.GetOwinContext().GetUserManager(Of ApplicationUserManager)())
            End Get
            Private Set
                _userManager = Value
            End Set
        End Property

        Public Property RoleManager() As ApplicationRoleManager
            Get
                Return If(_roleManager, HttpContext.GetOwinContext().GetUserManager(Of ApplicationRoleManager)())
            End Get
            Private Set
                _roleManager = Value
            End Set
        End Property

        ' GET: Cuenta
        <Authorize(Roles:="Admin")>
        Function Index() As ActionResult
            If Not IsNothing(TempData("FlashMessages")) Then
                ViewData("FlashMessages") = TempData("FlashMessages")
            End If

            Dim users As ICollection(Of User)
            users = _serv.FindAll()

            For Each u In users
                u.Role = RoleManager.FindById(u.Roles.First().RoleId)
            Next


            Return View(users)
        End Function

        ' GET: /Cuentas/Ingresar
        <AllowAnonymous>
        Public Function Ingresar(returnUrl As String) As ActionResult
            ViewData!ReturnUrl = returnUrl
            Return View()
        End Function

        '
        ' POST: /Cuentas/Ingresar
        <HttpPost>
        <AllowAnonymous>
        <ValidateAntiForgeryToken>
        Public Async Function Ingresar(model As LoginViewForm, returnUrl As String) As Task(Of ActionResult)
            If Not ModelState.IsValid Then
                Return View(model)
            End If

            ' This doesn't count login failures towards account lockout
            ' To enable password failures to trigger account lockout, change to shouldLockout := True
            Dim result = Await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout:=False)
            Select Case result
                Case SignInStatus.Success
                    Return RedirectToLocal(returnUrl)
                Case SignInStatus.LockedOut
                    Return View("Lockout")
                Case SignInStatus.RequiresVerification
                    Return RedirectToAction("SendCode", New With {
                        .ReturnUrl = returnUrl,
                        .RememberMe = model.RememberMe
                    })
                Case Else
                    ModelState.AddModelError("", "Datos inválidos.")
                    Return View(model)
            End Select
        End Function

        ' POST: /Cuentas/Salir
        <HttpPost>
        <ValidateAntiForgeryToken>
        Public Function Salir() As ActionResult
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie)
            Return RedirectToAction("Index", "Home")
        End Function

        ' GET: /Cuenta/Register
        <Authorize(Roles:="Admin")>
        Public Function Agregar() As ActionResult
            Return View()
        End Function

        '
        ' POST: /Account/Register
        <HttpPost>
        <Authorize(Roles:="Admin")>
        <ValidateAntiForgeryToken>
        Public Async Function Agregar(model As AddAccountForm) As Task(Of ActionResult)
            If ModelState.IsValid Then
                Dim user = New User() With {
                .UserName = model.Email,
                .Email = model.Email,
                .FirstName = model.FirstName,
                .LastName = model.LastName
            }
                Dim result = Await UserManager.CreateAsync(user, model.Password)

                If result.Succeeded Then
                    UserManager.AddToRole(user.Id, model.Role.ToString())

                    TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                         .Title = "Exito!",
                                         .Text = String.Format("El nuevo usuario se guardó correctamente!"),
                                         .Type = "success"})
                    Return RedirectToAction("Index", "Cuentas")

                End If
                AddErrors(result)
            End If

            ' If we got this far, something failed, redisplay form
            Return View(model)
        End Function

        <Authorize(Roles:="Admin")>
        Public Async Function Eliminar(id As String) As Task(Of ActionResult)
            Dim _user As User

            Try
                _user = UserManager.FindById(id)
                Dim result = Await UserManager.DeleteAsync(_user)

                If result.Succeeded Then
                    TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                     .Title = "Exito!",
                     .Text = String.Format("El usuario se eliminó correctamente!"),
                     .Type = "success"})
                    Return RedirectToAction("Index", "Cuentas")
                End If
                AddErrors(result)

            Catch ex As Exception
                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                     .Type = "danger",
                                                     .Title = "Error!",
                                                     .Text = ex.Message})
                Return RedirectToAction("Index", "Cuentas")
            End Try

            TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                     .Type = "danger",
                                     .Title = "Error!",
                                     .Text = "Ocurrió un error. Contacte al administrador"})
            Return RedirectToAction("Index", "Cuentas")
        End Function

        Private ReadOnly Property AuthenticationManager() As IAuthenticationManager
            Get
                Return HttpContext.GetOwinContext().Authentication
            End Get
        End Property

        Private Sub AddErrors(result As IdentityResult)
            For Each [error] In result.Errors
                ModelState.AddModelError("", [error])
            Next
        End Sub


        Private Function RedirectToLocal(returnUrl As String) As ActionResult
            If Url.IsLocalUrl(returnUrl) Then
                Return Redirect(returnUrl)
            End If
            Return RedirectToAction("Index", "Home")
        End Function
    End Class
End Namespace




