﻿Imports System.Net
Imports System.Web.Http
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

Namespace Controllers

    <RoutePrefix("api/search")>
    Public Class SearchAjaxController
        Inherits ApiController

        Private _emplServ As EmployeeService
        Private _visitorServ As VisitorService

        Public Sub New()
            _emplServ = New EmployeeService()
            _visitorServ = New VisitorService()
        End Sub

        <HttpGet>
        <Route("employee")>
        Public Function SearchEmployee(key As String) As IEnumerable(Of Employee)
            If IsNothing(key) Then
                Return BadRequest()
            End If

            Return _emplServ.Search(key)
        End Function

        <HttpGet>
        <Route("visitor")>
        Public Function SearchVisitor(key As String) As IEnumerable(Of Visitor)
            If IsNothing(key) Then
                Return BadRequest()
            End If

            Return _visitorServ.Search(key)
        End Function

    End Class
End Namespace