﻿Imports System.Threading.Tasks
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security
Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

Namespace Controllers

    <Authorize>
    Public Class UsuarioController
        Inherits Controller

        Private _serv As UserService

        Public Sub New()
            _serv = New UserService()
        End Sub

        Private _signInManager As ApplicationSignInManager
        Private _userManager As ApplicationUserManager

        Public Sub New(appUserManager As ApplicationUserManager, appSignInManager As ApplicationSignInManager)
            UserManager = appUserManager
            SignInManager = appSignInManager
        End Sub

        Public Property SignInManager() As ApplicationSignInManager
            Get
                Return If(_signInManager, HttpContext.GetOwinContext().Get(Of ApplicationSignInManager)())
            End Get
            Private Set(value As ApplicationSignInManager)
                _signInManager = value
            End Set
        End Property

        Public Property UserManager() As ApplicationUserManager
            Get
                Return If(_userManager, HttpContext.GetOwinContext().GetUserManager(Of ApplicationUserManager)())
            End Get
            Private Set(value As ApplicationUserManager)
                _userManager = value
            End Set
        End Property

        ' GET: User
        Function Index() As ActionResult
            If Not IsNothing(TempData("FlashMessages")) Then
                ViewData("FlashMessages") = TempData("FlashMessages")
            End If

            Try
                Return View(_serv.Find(User.Identity.GetUserId()))
            Catch ex As Exception
                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                     .Type = "danger",
                                                     .Title = "Error!",
                                                     .Text = "Ocurrió un error. Contacte al administrador"})
                Return RedirectToAction("Index", "Home")
            End Try
        End Function

        ' GET: /Usuario/Editar
        Function Editar() As ActionResult
            Try
                Return View(_serv.Find(User.Identity.GetUserId()))
            Catch ex As Exception
                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                     .Type = "danger",
                                                     .Title = "Error!",
                                                     .Text = "Ocurrió un error. Contacte al administrador"})
                Return RedirectToAction("Index", "Home")
            End Try
        End Function

        ' POST: /Usuario/Editar
        <HttpPost>
        Public Function Editar(model As User) As ActionResult
            If Not ModelState.IsValid Then
                Return View(model)
            End If

            Try
                Dim _user As User
                _user = _serv.Find(User.Identity.GetUserId())
                _user.FirstName = model.FirstName
                _user.LastName = model.LastName
                _user.Email = model.Email

                _serv.Save(_user)

                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                     .Type = "success",
                                     .Title = "Éxito!",
                                     .Text = "Se guardaron los cambios correctamente."})
                Return RedirectToAction("Index", "Usuario")
            Catch ex As Exception
                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                     .Type = "danger",
                                     .Title = "Error!",
                                     .Text = "Ocurrió un error. Contacte al administrador"})
                Return RedirectToAction("Index", "Home")
            End Try
        End Function


        ' GET: /Usuario/Contrasena
        Function Contrasena() As ActionResult

            Return View()
        End Function

        ' POST /Usuario/Editar
        <HttpPost>
        Public Async Function Contrasena(model As ChangePasswordViewModel) As Task(Of ActionResult)
            If Not ModelState.IsValid Then
                Return View(model)
            End If
            Dim result = Await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword)

            If result.Succeeded Then
                TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                     .Type = "success",
                                                     .Title = "Éxito!",
                                                     .Text = "La contraseña se actualizó correctamente"})
                Return RedirectToAction("Index", "Usuario")
            End If
            AddErrors(result)
            Return View(model)
        End Function

        Private Sub AddErrors(result As IdentityResult)
            For Each [error] In result.Errors
                ModelState.AddModelError("", [error])
            Next
        End Sub
    End Class
End Namespace