﻿Imports Microsoft.AspNet.Identity
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

<Authorize>
Public Class IngresosController
    Inherits Controller

    Private _serv As AdmissionService
    Private _plServ As ParkingLotService

    Public Sub New()
        _serv = New AdmissionService()
        _plServ = New ParkingLotService()
    End Sub

    ' GET Ingresos/
    Function Index() As ActionResult
        Dim admissionList As ICollection(Of Admission)
        admissionList = _serv.FindAllActive()

        Return View(admissionList)
    End Function

    ' GET Ingresos/Nuevo/<id>
    Function Nuevo(id As Integer) As ActionResult
        Dim parkingLot As ParkingLot
        parkingLot = _plServ.Find(id)

        If parkingLot.Status <> StatusEnum.Disponible Then
            ' Si el espacio de parqueo ya está ocupado,
            ' muestre un mensaje de error al usuario
            TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                         .Title = "Error!",
                                         .Text = String.Format("El campo seleccionado no está disponible"),
                                         .Type = "danger"})
            Return RedirectToAction("Index", "Home")
        End If

        ViewData("ParkingLot") = parkingLot
        Dim model As AdmissionForm
        model = New AdmissionForm(parkingLot)

        Return View(model)
    End Function

    ' POST Ingresos/Nuevo/<id>
    <HttpPost>
    <ValidateAntiForgeryToken>
    Function Nuevo(id As Integer, model As AdmissionForm) As ActionResult
        If Not ModelState.IsValid Then
            Return View(model)
        End If

        Dim emp As Employee = Nothing
        If model.VehicleType = VehicleType.Visita Then
            emp = New Employee With {.Id = model.Employee}
        End If

        Dim newAdmission As Admission


        newAdmission = New Admission With {
            .Reason = model.Reason,
            .Company = model.Company,
            .EntryDate = Date.Now,
            .DepartureDate = DateUtils.MergeDate(model.DepartureDate, model.DepartureHour),
            .Employee = emp,
            .Vehicle = New Vehicle With {.LicensePlate = model.VehicleLp, .Brand = model.VehicleBrand, .Color = model.VehicleColor, .Type = model.VehicleType},
            .ParkingLotID = id,
            .UserID = User.Identity.GetUserId(),
            .Visitors = FormToVisitors(model.Visitors)
        }
        newAdmission.Visitors.ElementAt(0).IsDriver = True

        Dim a As Admission
        Try
            a = _serv.Save(newAdmission)

            If IsNothing(a) Then
                Return HttpNotFound()
            End If

            TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                                 .Type = "success",
                                                                 .Title = "Éxito!",
                                                                 .Text = "Se registró un nuevo ingreso al parqueo"})
            Return RedirectToAction("Ver", New With {.id = a.Id})
        Catch ex As Exception
            ViewData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                .Title = "Error!",
                .Text = ex.Message,
                .Type = "danger"})
            Return View(model)
        End Try
    End Function

    ' GET Ingresos/Ver/<id>
    Function Ver(id As Integer) As ActionResult
        Dim admission As Admission
        admission = _serv.Find(id)

        If admission Is Nothing Then
            Return HttpNotFound()
        End If

        If Not IsNothing(TempData("FlashMessages")) Then
            ViewData("FlashMessages") = TempData("FlashMessages")
        End If

        Return View(admission)
    End Function

    ' GET /Ingresos/Salida/id
    Function Salida(id As Integer) As ActionResult
        Try
            Dim admission As Admission
            admission = _serv.CheckOut(id)

            TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                                 .Type = "success",
                                                                 .Title = "Éxito!",
                                                                 .Text = "Se registró la salida correctamente."})
            If IsNothing(admission) Then
                Return RedirectToAction("Index", "Home")
            Else
                Return RedirectToAction("Ver", New With {.id = admission.Id})
            End If
        Catch ex As Exception
            TempData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                                 .Type = "danger",
                                                                 .Title = "Error!",
                                                                 .Text = ex.Message})
            Return RedirectToAction("Index", "Home")
        End Try
    End Function

    Private Function FormToVisitors(form As ICollection(Of VisitorForm)) As ICollection(Of Visitor)
        Dim visitors As ICollection(Of Visitor) = New List(Of Visitor)()

        For Each vf As VisitorForm In form
            visitors.Add(New Visitor With {.Dni = vf.Dni,
                         .Name = vf.Name,
                         .Nationality = vf.Nationality,
                         .Email = vf.Email,
                         .PhoneNumber = vf.PhoneNumber}
            )
        Next

        Return visitors
    End Function

    Private Function RedirectToLocal(returnUrl As String) As ActionResult
        If Url.IsLocalUrl(returnUrl) Then
            Return Redirect(returnUrl)
        End If
        Return RedirectToAction("Index", "Home")
    End Function
End Class
