﻿Imports System.Web.Mvc
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

Namespace Controllers
    Public Class BusquedaController
        Inherits Controller

        Private _serv As AdmissionService

        Public Sub New()
            _serv = New AdmissionService()
        End Sub

        ' GET: Busqueda
        Function Index(q As String) As ActionResult
            Dim results As ICollection(Of Admission)
            ViewData("Q") = q
            results = _serv.Search(q)

            Return View(results)
        End Function
    End Class
End Namespace