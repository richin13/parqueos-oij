﻿Imports System.Net
Imports System.Web.Http
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service

Namespace Controllers
    <RoutePrefix("api/parkings")>
    Public Class ParkingsController
        Inherits ApiController

        Private _serv As ParkingService

        Public Sub New()
            _serv = New ParkingService()
        End Sub

        <HttpGet>
        <Route("{id:int}")>
        Public Function GetParking(id As Integer, Optional [date] As String = Nothing) As Parking
            Dim parking As Parking
            If Not IsNothing([date]) Then
                Dim realDate As Date
                Date.TryParse([date], realDate)
                parking = _serv.FindWithDetails(id)

                For Each pl In parking.ParkingLots
                    pl.PredictStatus([date])
                Next
            Else
                parking = _serv.Find(id)
            End If

            Return parking
        End Function

    End Class
End Namespace