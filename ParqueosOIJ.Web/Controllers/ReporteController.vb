﻿Imports System.Globalization
Imports System.IO
Imports System.Web.Mvc
Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.AspNet.Identity
Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model
Imports ParqueosOIJ.Service
Imports System
Imports System.Data

Imports ParqueosOIJ.Web.Report1
Namespace Controllers
    <Authorize>
    Public Class ReporteController
        Inherits Controller

        Private Property _serv As AdmissionService
        Private Property _rd As ReportDocument

        'Solo para insercion de datos de prueba en admission
        Private Property _servEmployee As EmployeeService
        Private Property _servParkingLot As ParkingLotService
        Private Property _servUser As UserService
        Private Property _servVehicle As VehicleService
        Private Property _servVisitor As VisitorService
        'Fin de importaciones necesarias para datos de prueba

        Public Sub New()
            MyBase.New()
            _serv = New AdmissionService()
            _rd = New ReportDocument()

            'Solo para insercion de datos de prueba en admission
            _servEmployee = New EmployeeService()
            _servParkingLot = New ParkingLotService()
            _servUser = New UserService()
            _servVehicle = New VehicleService()
            _servVisitor = New VisitorService()
            'Fin de importaciones necesarias para datos de prueba
        End Sub

        ' GET: Report
        Function Index() As ActionResult
            Return View()

        End Function

        <HttpPost>
        Function Export(car As String, ced As String, entryDate As String, departureDate As String) As ActionResult

            _rd.Load(Path.Combine(Server.MapPath("~/Reports/Report1.rpt")))

            'Eleccion de metodo para obtener datos de la Base de datos
            Dim reservations
            'Ningun filtro = control de reservaciones
            If String.Compare(car, "") = 0 And String.Compare(ced, "") = 0 And String.Compare(entryDate, "") = 0 And String.Compare(departureDate, "") = 0 Then
                Dim thisDate As Date
                thisDate = Today
                reservations = _serv.GetByDate(thisDate)

                'Vienen todos los filtros
            ElseIf Not String.Compare(car, "") = 0 And Not String.Compare(ced, "") = 0 And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRangeCedLicensePlate(car, ced, entryDate, departureDate) 'Hace la consulta a la base de datos y valida

                'Solo filtra rango de recha
            ElseIf String.Compare(car, "") = 0 And String.Compare(ced, "") = 0 And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRange(entryDate, departureDate) 'Hace la consulta a la base de datos y valida

                'Solo filtra rango de recha con ced
            ElseIf String.Compare(car, "") = 0 And Not Not String.Compare(ced, "") = 0 And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRangeCed(ced, entryDate, departureDate) 'Hace la consulta a la base de datos y valida

                'Solo filtra rango de recha con placa
            ElseIf Not String.Compare(car, "") = 0 And String.Compare(ced, "") = 0 And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRangeLicensePlate(car, entryDate, departureDate) 'Hace la consulta a la base de datos y valida
            End If

            'Creacion de dataset
            Dim myDS As New Data.DataSet("CusOrd")

            Dim myCustomers As Data.DataTable = myDS.Tables.Add("DataTable1")
            Dim myDr As Data.DataRow

            'Creacion de la tabla
            With myCustomers
                .Columns.Add("company", Type.GetType("System.String"))
                .Columns.Add("dni", Type.GetType("System.String"))
                .Columns.Add("licensePlate", Type.GetType("System.String"))
                .Columns.Add("reason", Type.GetType("System.String"))
                .Columns.Add("entryDate", Type.GetType("System.String"))
                .Columns.Add("departureDate", Type.GetType("System.String"))
            End With

            'Ingreso de datos
            For Each p As Admission In reservations
                myDr = myCustomers.NewRow()

                myDr("company") = p.Company
                myDr("dni") = p.Visitors.Where(Function(r) r.IsDriver = True).First.Dni
                myDr("licensePlate") = p.Vehicle.LicensePlate
                myDr("reason") = p.Reason
                myDr("entryDate") = p.EntryDate
                myDr("departureDate") = p.DepartureDate

                myCustomers.Rows.Add(myDr)
            Next
            'Fin de creacion de dataset

            _rd.SetDataSource(myDS)

            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            Dim _str As Stream = _rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
            _str.Seek(0, SeekOrigin.Begin)
            Return File(_str, "application/pdf", "ReportParqueoOIJ.pdf")
            Return View()
        End Function


        ' GET: /Reporte/Ingresos
        Function Ingresos() As ActionResult
            Return View()
        End Function

        ' GET: /Reporte/Personas
        Function Personas() As ActionResult
            Return View()
        End Function

        ' GET: /Reporte/Vehiculos
        Function Vehiculos() As ActionResult
            Return View()
        End Function

        Function ControlVisitas() As ActionResult
            Try

                ViewData("car") = ""
                ViewData("ced") = ""
                ViewData("entryDate") = ""
                ViewData("departureDate") = ""

                Dim thisDate As Date
                thisDate = Today
                Return View(_serv.GetByDate(thisDate))
            Catch ex As Exception
                ViewData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                                         .Title = "Error!",
                                                                         .Text = ex.Message,
                                                                         .Type = "danger"})
            End Try
            ViewData("car") = ""
            ViewData("ced") = ""
            ViewData("entryDate") = ""
            ViewData("departureDate") = ""
            Return View()
        End Function

        Function General() As ActionResult
            Return View()
        End Function

        <AllowAnonymous>
        <HttpPost>
        <ValidateAntiForgeryToken>
        Function General(model As ReporteForm) As ActionResult

            Try

                Dim reservations
                'Todos los filtros
                If Not model.car Is Nothing And Not model.ced Is Nothing Then
                    reservations = _serv.FindByDateRangeCedLicensePlate(model.car, model.ced, model.EntryDate, model.DepartureDate) 'Hace la consulta a la base de datos y valida
                    'Solo filtra rango de recha
                ElseIf model.car Is Nothing And model.ced Is Nothing Then
                    reservations = _serv.FindByDateRange(model.EntryDate, model.DepartureDate) 'Hace la consulta a la base de datos y valida

                    'Solo filtra rango de recha con ced
                ElseIf model.car Is Nothing And Not model.ced Is Nothing Then
                    reservations = _serv.FindByDateRangeCed(model.ced, model.EntryDate, model.DepartureDate) 'Hace la consulta a la base de datos y valida

                    'Solo filtra rango de recha con placa
                ElseIf Not model.car Is Nothing And model.ced Is Nothing Then
                    reservations = _serv.FindByDateRangeLicensePlate(model.car, model.EntryDate, model.DepartureDate) 'Hace la consulta a la base de datos y valida
                End If

                Return RedirectToAction("VerReporte", New With {.car = model.car, .ced = model.ced, .entryDate = model.EntryDate, .departureDate = model.DepartureDate})
            Catch ex As Exception
                ViewData("FlashMessages") = FmGenerator.GenerateList(New FlashMessage With {
                                                                     .Title = "Error!",
                                                                     .Text = ex.Message,
                                                                     .Type = "danger"})
            End Try

            Return View(model)
        End Function

        Function VerReporte(car As String, ced As String, entryDate As String, departureDate As String) As ActionResult
            Dim reservations
            If Not car Is Nothing And Not ced Is Nothing And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRangeCedLicensePlate(car, ced, entryDate, departureDate) 'Hace la consulta a la base de datos y valida

                'Solo filtra rango de recha
            ElseIf car Is Nothing And ced Is Nothing And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRange(entryDate, departureDate) 'Hace la consulta a la base de datos y valida

                'Solo filtra rango de recha con ced
            ElseIf car Is Nothing And Not ced Is Nothing And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRangeCed(ced, entryDate, departureDate) 'Hace la consulta a la base de datos y valida

                'Solo filtra rango de recha con placa
            ElseIf Not car Is Nothing And ced Is Nothing And Not String.Compare(entryDate, "") = 0 And Not String.Compare(departureDate, "") = 0 Then
                reservations = _serv.FindByDateRangeLicensePlate(car, entryDate, departureDate) 'Hace la consulta a la base de datos y valida
            End If

            ViewData("car") = car
            ViewData("ced") = ced
            ViewData("entryDate") = entryDate
            ViewData("departureDate") = departureDate

            Return View(reservations)
        End Function

    End Class
End Namespace