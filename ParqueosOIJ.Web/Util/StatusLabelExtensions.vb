﻿Imports System.Runtime.CompilerServices
Imports ParqueosOIJ.Model
Imports System.Linq
Imports System.Linq.Expressions

Public Module Extensions

    <Extension()>
    Public Function StatusLabel(helper As HtmlHelper, status As ReservationStatus) As HtmlString
        Dim labelClass, labelText As String
        labelClass = If(status = ReservationStatus.Aprobada,
            "label-success",
            If(status = ReservationStatus.Rechazada,
                "label-danger",
                If(status = ReservationStatus.Vencida,
                    "label-warning",
                    "label-default")))

        labelText = If(status = ReservationStatus.Aprobada,
            "Aprobada",
            If(status = ReservationStatus.Rechazada,
                "Rechazada",
                If(status = ReservationStatus.Vencida,
                    "Vencida",
                    "Pendiente de aprobación")))

        Dim tag As TagBuilder
        tag = New TagBuilder("label")
        tag.AddCssClass("label")
        tag.AddCssClass(labelClass)
        tag.SetInnerText(labelText)

        Dim mvchtml As MvcHtmlString
        mvchtml = New MvcHtmlString(tag.ToString())

        Return mvchtml
    End Function

    <Extension()>
    Public Function EnumDropDownListFor(Of TModel, TProperty, TEnum)(htmlHelper As HtmlHelper(Of TModel), expression As Expression(Of Func(Of TModel, TProperty)), selectedValue As TEnum, htmlAttrs As Object) As MvcHtmlString
        Dim values As IEnumerable(Of TEnum) = [Enum].GetValues(GetType(TEnum)).Cast(Of TEnum)()
        Dim items As IEnumerable(Of SelectListItem) = From value In values Select New SelectListItem() With {
                .Text = value.ToString(),
                .Value = value.ToString(),
                .Selected = (value.Equals(selectedValue))
    }
        Return SelectExtensions.DropDownListFor(htmlHelper, expression, items, htmlAttrs)
    End Function

End Module
