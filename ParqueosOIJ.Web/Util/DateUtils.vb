﻿Public Class DateUtils
    ''' <summary>
    ''' Fusiona una fecha y hora dadas como string y retorna la fecha
    ''' fusionada.
    ''' 
    ''' Si los argumentos están vacíos retorna Nothing.
    ''' </summary>
    ''' <param name="StrDate">El string que representa la fecha</param>
    ''' <param name="StrHour">El string que representa la hora</param>
    ''' <returns>La Fecha (objecto) fusionada.</returns>
    Public Shared Function MergeDate(StrDate As String, StrHour As String) As Date?
        If String.IsNullOrEmpty(StrDate) Or String.IsNullOrEmpty(StrHour) Then
            Return Nothing
        End If

        Dim ReturningDate As Date
        Dim StrDateTime As String
        StrDateTime = StrDate + " " + StrHour
        If Not DateTime.TryParse(StrDateTime, ReturningDate) Then
            Throw New FormatException()
        End If

        Return ReturningDate
    End Function
End Class
