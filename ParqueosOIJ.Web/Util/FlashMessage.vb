﻿''' <summary>
''' Representa un mensaje.
''' Posee un título, un texto y un tipo
''' Este último es cualquiera de los siguientes valores:
'''  * success
'''  * default
'''  * warning
'''  * error
''' </summary>
Public Class FlashMessage

    Public Property Title As String
    Public Property Text As String
    Public Property Type As String

End Class

''' <summary>
''' Genera un lista de mensajes flash
''' </summary>
Public Class FmGenerator

    Public Shared Function GenerateList(ParamArray args() As FlashMessage) As ICollection(Of FlashMessage)
        Dim list As ICollection(Of FlashMessage)
        list = New List(Of FlashMessage)()
        For Each fm As FlashMessage In args
            list.Add(fm)
        Next
        Return list
    End Function
End Class