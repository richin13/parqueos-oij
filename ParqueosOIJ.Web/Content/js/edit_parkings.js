﻿// debe incluir el archivo parkings.js antes de este

$(document).ready(function () {
    //idealmente aquí debería haber una manera de obtener el parqueo a editar,
    // como por el momento sólamente hay uno, entonces: hard-code it 
    var selectedParking = 1;
    var flag = false;
    //    var hiddenText;

    var loader = new Parking({
        //
        parking: selectedParking,
        onRectClick: function (pl) {
            if ($('#parking-lot-id').attr('data-parking-lot-id') == pl.id) {
                if (!flag) {
                    //can edit
                    makeDraggable(pl, function (el) {//el callback se ejecuta cada vez que se termina de arrastrar el elemento
                        //asignar evento onclick al botón de restablecer
                        $('#reset-button').on('click', function (e) {
                            //
                            var att = { x: el.X, y: el.Y };
                            el.rect.attr(att);
                            el.text.attr(textNiceCoords(el));
                            el.text.show();

                            e.preventDefault();
                            $(this).addClass('disabled');
                        });
                    });
                    pl.text.hide();
                    flag = true;
                }
            } else {
                window.location = '/Admin/EditarParqueo/' + selectedParking + '?espacio=' + pl.id;
            }
        }
    }).load();

    $('#edit-parking-save-button').on('click', function (e) {
        e.preventDefault();
        var hx = $('#X'), hy = $('#Y');

        if (hx.attr('data-x') !== undefined || hy.attr('data-y') !== undefined) {
            hx.val(hx.attr('data-x'));
            hy.val(hy.attr('data-y'));
        }

        $('#edit-parking-form').submit();
    });

});

function makeDraggable(el, callback) {// el callback es un evento onfinish (cuando termina el proceso de arrastrar)
    console.log("[LOG] Coords are: " + el.X + ", " + el.X);
    var dragger = function () {//onstart
        this.ox = this.attr("x");
        this.oy = this.attr("y");
        this.animate({ "fill-opacity": .2 }, 500);
    },
    move = function (dx, dy) {
        var att = { x: this.ox + dx, y: this.oy + dy };
        this.attr(att);
    },
    up = function () {//onend
        var x = this.attr("x"), y = this.attr("y");

        this.animate({ "fill-opacity": 1 }, 500);

        if (this.ox != x || this.oy != y) {
            //position changed
            $('#reset-button').removeClass('disabled');

            var hx = $('#X'), hy = $('#Y');

            hx.attr('data-x', x);
            hy.attr('data-y', y);
        }

        callback(el);
    };

    el.rect.attr({ cursor: "move" });
    el.rect.drag(move, dragger, up);
}