/// <autosync enabled="true" />
/// <reference path="../bower/bootstrap/dist/js/bootstrap.min.js" />
/// <reference path="../bower/bs-typeahead/js/bootstrap-typeahead.min.js" />
/// <reference path="../bower/handlebars.js/lib/handlebars.js" />
/// <reference path="../bower/jquery/dist/jquery.min.js" />
/// <reference path="../bower/jquery-timeago/jquery.timeago.js" />
/// <reference path="../bower/jquery-timeago/locales/jquery.timeago.es.js" />
/// <reference path="../bower/list.js/dist/list.min.js" />
/// <reference path="../bower/pickadate/lib/compressed/picker.date.js" />
/// <reference path="../bower/pickadate/lib/compressed/picker.js" />
/// <reference path="../bower/pickadate/lib/compressed/picker.time.js" />
/// <reference path="../bower/raphael/raphael.min.js" />
/// <reference path="../bower/selectize/dist/js/standalone/selectize.min.js" />
/// <reference path="../bower/sticky-kit/jquery.sticky-kit.min.js" />
/// <reference path="../bower/sweetalert2/dist/sweetalert2.min.js" />
/// <reference path="../bower/underscore/underscore-min.js" />
/// <reference path="admissions.js" />
/// <reference path="edit_parkings.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.settings.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jqueryval.bootstrap.js" />
/// <reference path="parkings.js" />
/// <reference path="site.js" />
