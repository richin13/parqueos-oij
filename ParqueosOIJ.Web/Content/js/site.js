﻿$(document).ready(function () {
    var $box = $('#search-box-container');
    $('#search-box').focusin(function () {
        $box.addClass('focus');
    }).focusout(function () {
        $box.removeClass('focus');
    });

    $('.clickable-rows tr').each(function (idx, el) {
        el.onclick = function () {
            window.location = $(el).data('href');
        }
    })
});