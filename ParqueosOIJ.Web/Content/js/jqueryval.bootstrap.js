﻿$(document).ready(function () {
    $("span.field-validation-valid, span.field-validation-error").addClass('help-inline');
    $("div.control-group").has("span.field-validation-error").addClass('error');
    $(".validation-summary-errors").addClass("alert alert-danger");
});