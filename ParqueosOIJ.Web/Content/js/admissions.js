﻿//Agrega dinámicamente nuevos ocupantes
$(document).ready(function () {
    //
    var source = $("#visitor-template").html();
    var template = Handlebars.compile(source);
    var $visitors = $('#extra-visitors-row');

    $('#extra-visitor-link').on('click', function (e) { // Enlace de Agregar ocupante
        e.preventDefault();
        e.stopPropagation();
        var $index = $visitors.data('count') + 1;

        var context = { index: $index };
        var html = template(context);

        $('#extra-visitors-body').append(html);
        deleteOnlyLastCreated($index, $index);

        $('#visitor-delete-' + $index).on('click', function (e) {
            deleteEvent(e, $(this));
        });

        $visitors.data('count', $index);
    });

    deleteOnlyLastCreated($visitors.data('count'), $visitors.data('count'));// muestra u oculta el botón de borrar en los visitantes ya creados

    for (var i = 1; i <= $visitors.data('count') ; i++) {
        $('#visitor-delete-' + i).on('click', function (e) {// setea el event listener en los visitantes ya creados
            deleteEvent(e, $(this));
        })
    }

    function deleteEvent(e, btn) {
        e.preventDefault();
        e.stopPropagation();

        $(btn.data('target-delete')).remove();// borra todo el panel del visitante

        // actualiza los índices
        var $index = $visitors.data('count') - 1; // menos 1 (el que acabamos de borrar)
        $visitors.data('count', $index);
        deleteOnlyLastCreated($index, $index);
    }
});

// Obtiene los empleados de la base de datos
$(document).ready(function () {
    //
    var employees = new Array();
    $('#EmployeeName').typeahead({
        onSelect: function (item) {
            $('#Employee').val(item.value);
            $('#EmployeeFloor').html(employees[item.value].Floor);
        },
        ajax: {
            url: "/api/search/employee",
            timeout: 500,
            displayField: "Name",
            valueField: "Id",
            triggerLength: 3,
            method: "get",
            loadingClass: "loading-circle",
            preDispatch: function (query) {
                return {
                    key: query
                }
            },
            preProcess: function (data) {
                $.each(data, function (index, value) {
                    employees[value.Id] = value;
                });
                return data;

            }
        }
    });
});

// recursiva porque soy bien macho XD
function deleteOnlyLastCreated(total, index) {
    if (index <= 0) {
        if (total == 0) {
            //$('#extra-visitors-alert').removeClass('hidden');
            $('#extra-visitors-alert').fadeIn('fast', function () {
                $(this).removeClass('hidden');
            });
        } else {
            $('#extra-visitors-alert').fadeOut('fast', function () {
                $(this).addClass('hidden');
            });
            //$('#extra-visitors-alert').addClass('hidden');
        }
        return;
    } else if (index == total) { // al último se le activa el botón de borrar
        $('#visitor-delete-' + index).removeClass('hidden');
        deleteOnlyLastCreated(total, index - 1);
    } else {
        $('#visitor-delete-' + index).addClass('hidden');
        deleteOnlyLastCreated(total, index - 1);
    }
}