﻿function setDefaults(options, defaults) {
    return _.defaults({}, _.clone(options), defaults);
}

var defaults = {
    url: '/api/parkings/',
    parking: 1,
    date: undefined,
    paper: 'paper',
    paperWidth: '745',
    paperHeight: '1050',
    showNumber: true,
    loader: '#parking-loader',
    onRectClick: undefined,
    statusText: {
        0: 'Disponible',
        1: 'Deshabilitado',
        2: 'Ocupado',
        3: 'Reservado',
        4: 'Desconocido',
        fallback: 'No definido'
    },
    statusColors: {
        0: '#47d862',
        1: '#515151',
        2: '#c94236',
        3: '#377ece',
        4: '#adadad',
        fallback: '#ffffff'
    },
    typeText: {
        0: 'Visitantes',
        1: 'Oficial',
        2: 'Jefaturas',
        3: 'Discapacitados',
        fallback: 'No definido'
    },
    typeColors: {
        0: '#ED721A',
        1: '#E0DA33',
        2: '#3A59A8',
        3: '#5ADCFC',
        fallback: 'No definido'
    }
}

function Parking(opt) {
    opt = setDefaults(opt, defaults);

    this.parkingId = opt['parking'];
    this.fullUrl = opt['url'] + opt['parking']

    if (opt['date'] !== undefined) {
        this.fullUrl += '?date=' + opt['date'];
    }

    this.load = function () {
        // Se muestra el loader
        $('#' + opt['paper']).html('');
        $('#parking-loader').removeClass('hidden');

        $.getJSON(this.fullUrl, function (data) {
            var rsr = Raphael(opt['paper'], opt['paperWidth'], opt['paperHeight']);
            var layer1 = rsr.set();
            layer1.attr({ 'id': 'layer1', 'name': 'layer1' });

            // Se podría dar opción para customizar esto pero
            // ahorita mismo no viene al caso xD
            rsr.path(data.Shape).attr({
                id: 'rect4136',
                parent: 'layer1',
                fill: '#e2e2e2',
                "fill-opacity": '1',
                stroke: '#000000',
                "stroke-width": '5.62',
                "stroke-miterlimit": '4',
                "stroke-dasharray": 'none',
                "stroke-opacity": '1'
            });

            // Pintamos cada espacio
            $.each(data.ParkingLots, function (i, el) {
                var rect = rsr.rect().attr(buildDefaults(el, opt));
                layer1.push(rect);
                el.rect = rect;

                rect.mouseover(function (e) {
                    rect.animate({ 'fill': opt.typeColors[el.Type] }, 250);
                });

                rect.mouseout(function (e) {
                    rect.animate({ 'fill': opt.statusColors[el.Status] }, 250);
                });

                // Se le agrega el método onclick deseado
                var niceElement = new ParkingLot(el, opt);
                rect.node.onclick = function () {
                    opt.onRectClick(niceElement);
                }

                if (opt['showNumber']) {
                    coords = textNiceCoords(el);

                    var text = rsr.text(coords.x, coords.y, el.Id).attr({
                        'font-size': '24',
                        cursor: 'pointer'
                    });
                    layer1.push(text);
                    el.text = text;

                    text.mouseover(function (e) {
                        rect.animate({ 'fill': opt.typeColors[el.Type] }, 250);
                    });

                    text.mouseout(function (e) {
                        rect.animate({ 'fill': opt.statusColors[el.Status] }, 250);
                    });

                    text.node.onclick = function () {
                        opt.onRectClick(niceElement);
                    }

                    niceElement.text = text;
                }
            });

            // Se quita el loader
            $('#parking-loader').addClass('hidden');
        });
    }
}

function ParkingLot(raw, opt) {
    opt = setDefaults(opt, defaults);

    this.id = raw.Id;
    this.type = opt.typeText[raw.Type];
    this.status = opt.statusText[raw.Status];
    this.parking = opt['parking'];
    this.isAvailable = raw.Status == 0 || raw.Status == 3;
    this.hasAdmission = raw.Status == 2;

    // Elementos de Raphael (rect y text)
    this.rect = raw.rect;
    this.text = raw.text;

    // Detalles técnicos
    this.X = raw.X;
    this.Y = raw.Y;
    this.Height = raw.Height;
    this.Width = raw.Width;
    this.Status = raw.Status;
    this.Type = raw.Type;
}

function buildDefaults(el, opt) {
    return {
        id: el.Id,
        x: el.X,
        y: el.Y,
        height: el.Height,
        width: el.Width,
        parent: 'layer1',
        fill: opt.statusColors[el.Status],
        "fill-opacity": '1',
        stroke: '#000000',
        "stroke-width": '5.89',
        "stroke-miterlimit": '4',
        "stroke-dasharray": 'none',
        "stroke-opacity": '1',
        title: el.Id,
        cursor: 'pointer'
    };
}

function textNiceCoords(el) {
    var result = {};

    if (el.Width > el.Height) {
        result.x = el.X + 42.5;
        result.y = el.Y + 20;
    } else {
        result.x = el.X + 20;
        result.y = el.Y + 42.5;
    }

    return result;
}