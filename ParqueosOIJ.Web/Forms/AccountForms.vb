﻿Imports System.ComponentModel.DataAnnotations

Public Class LoginViewForm
    <Required(ErrorMessage:="El correo electrónico es requerido")>
    <Display(Name:="Correo electrónico")>
    <DataType(DataType.EmailAddress)>
    <EmailAddress(ErrorMessage:="El correo electrónico no es válido")>
    Public Property Email As String

    <Required(ErrorMessage:="La contraseña es requerida")>
    <DataType(DataType.Password)>
    <Display(Name:="Contraseña")>
    Public Property Password As String

    <Display(Name:="Recordar?")>
    Public Property RememberMe As Boolean
End Class

Public Class AddAccountForm

    <Required(ErrorMessage:="Debe especificar el nombre del usuario.")>
    <Display(Name:="Nombre")>
    Public Property FirstName As String

    <Required(ErrorMessage:="Debe especificar al menos un apellido del usuario.")>
    <Display(Name:="Apellidos")>
    Public Property LastName As String

    <Required(ErrorMessage:="Debe especificar el correo electrónico del usuario.")>
    <EmailAddress>
    <Display(Name:="Email")>
    Public Property Email As String

    <Required(ErrorMessage:="Debe especificar una contraseña")>
    <StringLength(100, ErrorMessage:="La {0} debe contener al menos {2} caracteres de longitud", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="Contraseña")>
    Public Property Password As String

    <DataType(DataType.Password)>
    <Display(Name:="Confirme la contraseña")>
    <Compare("Password", ErrorMessage:="Las contraseñas no coinciden")>
    Public Property ConfirmPassword As String

    <Required(ErrorMessage:="Debe seleccionar un rol de usuario.")>
    <Display(Name:="Rol")>
    Public Property Role As RolesEnum

    Public Enum RolesEnum
        Admin
        Officer
    End Enum

End Class

Public Class ResetPasswordViewForm
    <Required>
    <EmailAddress>
    <Display(Name:="Email")>
    Public Property Email() As String

    <Required>
    <StringLength(100, ErrorMessage:="The {0} must be at least {2} characters long.", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="Password")>
    Public Property Password() As String

    <DataType(DataType.Password)>
    <Display(Name:="Confirm password")>
    <Compare("Password", ErrorMessage:="The password and confirmation password do not match.")>
    Public Property ConfirmPassword() As String

    Public Property Code() As String
End Class

Public Class ForgotPasswordViewForm
    <Required>
    <EmailAddress>
    <Display(Name:="Email")>
    Public Property Email() As String
End Class

Public Class ChangePasswordViewModel
    <Required>
    <DataType(DataType.Password)>
    <Display(Name:="Contraseña actual")>
    Public Property OldPassword As String

    <Required>
    <StringLength(100, ErrorMessage:="La {0} debe tener como mínimo {2} caracteres.", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="Nueva contraseña")>
    Public Property NewPassword As String

    <DataType(DataType.Password)>
    <Display(Name:="Confirmar nueva contraseña")>
    <Compare("NewPassword", ErrorMessage:="Las contraseñas no coinciden")>
    Public Property ConfirmPassword As String
End Class