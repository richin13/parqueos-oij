﻿Imports System.ComponentModel.DataAnnotations

Public Class ReservationForm


    <Required(ErrorMessage:="Debe especificar el motivo de la visita.")>
    <Display(Name:="Motivo de la visita")>
    <DataType(DataType.Text)>
    <MaxLength(255, ErrorMessage:="El motivo debe tener menos de 255 caracteres")>
    Public Property Reason As String

    ' The visitor
    <Display(Name:="Cédula del visitante")>
    <Required(ErrorMessage:="Debe especificar un número de cédula de visitante válido.")>
    Public Overridable Property Visitor As String

    ' The car
    <Display(Name:="Placa del vehículo")>
    <Required(ErrorMessage:="Debe especificar un número de placa de vehículo válido.")>
    Public Overridable Property Vehicle As String

    <Display(Name:="Hecha por")>
    <Required(ErrorMessage:="Debe especificar un número de cédula de empleado válido.")>
    Public Overridable Property Employee As String

    <Display(Name:="Fecha de entrada")>
    <DataType(DataType.Date)>
    <Required(ErrorMessage:="Debe especificar la fecha de entrada.")>
    Public Property EntryDateV As String ' Visualization purposes

    <Display(Name:="Hora de entrada")>
    <DataType(DataType.Time)>
    <Required(ErrorMessage:="Debe especificar la hora de entrada.")>
    Public Property EntryHour As String

    <Display(Name:="Fecha de salida")>
    <DataType(DataType.Date)>
    <Required(ErrorMessage:="Debe especificar la fecha de aproximada de salida.")>
    Public Property DepartureDateV As String ' Visualization purposes

    <Display(Name:="Hora de salida")>
    <DataType(DataType.Time)>
    <Required(ErrorMessage:="Debe especificar la hora de salida.")>
    Public Property DepartureHour As String

End Class

Public Class CheckReservationStatusForm

    <Required(ErrorMessage:="Debe especificar el número de placa del vehículo visitante.")>
    <Display(Name:="Placa del vehículo")>
    Public Property VehicleLicensePlate As String ' placa xD

    <Required(ErrorMessage:="Debe especificar el número de cédula del visitante")>
    <Display(Name:="Cédula del visitante")>
    Public Property VisitorDni As String

End Class
