﻿Imports System.ComponentModel.DataAnnotations
Public Class ReporteForm
    <Required(ErrorMessage:="Debe especificar el número de placa del vehículo visitante.")>
    <Display(Name:="Fecha inicial")>
    Public Property EntryDate As String 'Fecha de ingreso

    <Required(ErrorMessage:="Debe especificar el número de cédula del visitante")>
    <Display(Name:="Fecha final")>
    Public Property DepartureDate As String 'Fecha de salida

    <Required(ErrorMessage:="Debe especificar la placa del vehículo")>
    <Display(Name:="Placa del vehículo")>
    Public Property car As String 'placa del vehiculo

    <Required(ErrorMessage:="Debe especificar la cédula del conductor")>
    <Display(Name:="Cédula del conductor")>
    Public Property ced As String 'cédula del conductor

End Class
