﻿Imports ParqueosOIJ.Model
Imports System.ComponentModel.DataAnnotations

Public Class AdmissionForm

    Public Sub New()
        Visitors = New List(Of VisitorForm)
        Visitors.Add(New VisitorForm()) ' the driver
    End Sub

    Public Sub New(parkingLot As ParkingLot)
        MyClass.New()

        If parkingLot.Type = ParkingLotType.Oficial Then
            VehicleType = VehicleType.Oficial
        ElseIf parkingLot.Type = ParkingLotType.Jefatura Then
            VehicleType = VehicleType.Jefatura
        Else
            VehicleType = VehicleType.Visita
        End If
    End Sub

#Region "VehicleDetails"

    <Display(Name:="Tipo de vehículo")>
    <Required(ErrorMessage:="Debe especificar el tipo de vehículo a ingresar.")>
    Public Property VehicleType As VehicleType

    <Display(Name:="Placa o número de vehículo")>
    <Required(ErrorMessage:="Debe especificar la placa del vehículo a ingresar.")>
    Public Property VehicleLp As String

    <Display(Name:="Marca del vehículo")>
    Public Property VehicleBrand As String

    <Display(Name:="Modelo del vehiculo")>
    Public Property VehicleModel As String

    <Display(Name:="Color del vehículo")>
    Public Property VehicleColor As String

#End Region

#Region "AmissionDetails"

    <Display(Name:="Compañia/Institución")>
    Public Property Company As String

    <Display(Name:="Motivo de la visita")>
    Public Property Reason As String

    <Display(Name:="Fecha tentativa de salida")>
    <DataType(DataType.Date)>
    Public Property DepartureDate As String

    <Display(Name:="Hora tentativa de salida")>
    <DataType(DataType.Time)>
    Public Property DepartureHour As String

    <Display(Name:="Empleado a quien visita")>
    Public Property Employee As Integer?

#End Region

#Region "VisitorsDetails"

    ''' <summary>
    ''' La lista de "Visitantes" que se registran en un Ingreso.
    ''' <para />
    ''' Unas notas acerca de esto:
    ''' <list type="bullet">
    '''     <item>
    '''         <description>El primer elemento de esta colección es siempre el conductor.</description>
    '''     </item>
    '''     <item>
    '''         <description>La lista que se genera con Javascript depende del nombre de esta variable, o sea Visitors. Si por algún motivo
    '''         se realiza un Refactor en este archivo y se cambia el nombre debe modificar también el archivo 
    '''         Nuevo.vbhtml localizado en "Views/Ingresos/", especificamente la sección del formulario correspondiente 
    '''         al conductor así como el Template de Handlebars (también en ese archivo)</description>
    '''     </item>
    ''' </list>
    ''' 
    ''' </summary>
    <Display(Name:="Visitantes")>
    <Required(ErrorMessage:="Debe especificar los datos del conductor")>
    Public Property Visitors As IList(Of VisitorForm)

#End Region

End Class

Public Class VisitorForm

    <Display(Name:="Cédula/Identificación")>
    <Required(ErrorMessage:="Debe especificar el número de cédula")>
    <MaxLength(20)>
    Public Property Dni As String

    <Display(Name:="Nombre")>
    <Required(ErrorMessage:="Debe especificar el nombre")>
    <DataType(DataType.Text)>
    Public Property Name As String

    <Display(Name:="Correo electrónico")>
    <DataType(DataType.EmailAddress)>
    <MaxLength(64, ErrorMessage:="El correo debe tener menos de 64 caracteres")>
    Public Property Email As String

    <Display(Name:="Teléfono")>
    <DataType(DataType.PhoneNumber)>
    Public Property PhoneNumber As String

    <Display(Name:="Nacionalidad")>
    <Required(ErrorMessage:="Debe especificar la nacionalidad del visitante.")>
    Public Property Nationality As NationalityType
End Class