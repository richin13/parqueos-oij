﻿Imports System.Data.Entity
Imports System.Data.Entity.ModelConfiguration.Conventions
Imports Microsoft.AspNet.Identity.EntityFramework
Imports ParqueosOIJ.Model

Public Class ApplicationContext
    Inherits IdentityDbContext(Of User)

    Public Sub New()
        MyBase.New("DefaultConnection")

        Configuration.ProxyCreationEnabled = False
        Configuration.LazyLoadingEnabled = True
    End Sub

    Public Property Admissions() As DbSet(Of Admission)
    Public Property Employees() As DbSet(Of Employee)
    Public Property Parkings() As DbSet(Of Parking)
    Public Property ParkingLots() As DbSet(Of ParkingLot)

    Public Property Persons() As DbSet(Of Person)
    Public Property Reservations() As DbSet(Of Reservation)
    Public Property Vehicles() As DbSet(Of Vehicle)
    Public Property Visitors() As DbSet(Of Visitor)

    Public Shared Function Create() As ApplicationContext
        Return New ApplicationContext()
    End Function

    Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        MyBase.OnModelCreating(modelBuilder)
        ' Singular-named tables
        modelBuilder.Conventions.Remove(Of PluralizingTableNameConvention)()

        modelBuilder.Entity(Of Person)().ToTable("Person")
        modelBuilder.Entity(Of Visitor)().ToTable("Visitor")
        modelBuilder.Entity(Of Employee)().ToTable("Employee")


    End Sub

End Class
