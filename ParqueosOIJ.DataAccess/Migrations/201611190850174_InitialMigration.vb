Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class InitialMigration
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Admission",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Reason = c.String(),
                        .Company = c.String(),
                        .EntryDate = c.DateTime(nullable := False),
                        .DepartureDate = c.DateTime(),
                        .Status = c.Int(nullable := False),
                        .EmployeeID = c.Int(),
                        .VehicleID = c.Int(nullable := False),
                        .ParkingLotID = c.Int(nullable := False),
                        .UserID = c.String(nullable := False, maxLength := 128)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.ParkingLot", Function(t) t.ParkingLotID, cascadeDelete := True) _
                .ForeignKey("dbo.AspNetUsers", Function(t) t.UserID, cascadeDelete := True) _
                .ForeignKey("dbo.Vehicle", Function(t) t.VehicleID, cascadeDelete := True) _
                .ForeignKey("dbo.Employee", Function(t) t.EmployeeID) _
                .Index(Function(t) t.EmployeeID) _
                .Index(Function(t) t.VehicleID) _
                .Index(Function(t) t.ParkingLotID) _
                .Index(Function(t) t.UserID)
            
            CreateTable(
                "dbo.Person",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Dni = c.String(maxLength := 20),
                        .Name = c.String(),
                        .Email = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .Index(Function(t) t.Dni, unique := True)
            
            CreateTable(
                "dbo.Reservation",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .RDate = c.DateTime(nullable := False),
                        .Status = c.Int(nullable := False),
                        .Reason = c.String(nullable := False, maxLength := 255),
                        .Comment = c.String(maxLength := 255),
                        .EntryDate = c.DateTime(nullable := False),
                        .DepartureDate = c.DateTime(nullable := False),
                        .VisitorID = c.Int(nullable := False),
                        .ParkingLotID = c.Int(),
                        .VehicleID = c.Int(nullable := False),
                        .EmployeeID = c.Int(nullable := False),
                        .UserID = c.String(maxLength := 128)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Employee", Function(t) t.EmployeeID) _
                .ForeignKey("dbo.ParkingLot", Function(t) t.ParkingLotID) _
                .ForeignKey("dbo.AspNetUsers", Function(t) t.UserID) _
                .ForeignKey("dbo.Vehicle", Function(t) t.VehicleID, cascadeDelete := True) _
                .ForeignKey("dbo.Visitor", Function(t) t.VisitorID) _
                .Index(Function(t) t.VisitorID) _
                .Index(Function(t) t.ParkingLotID) _
                .Index(Function(t) t.VehicleID) _
                .Index(Function(t) t.EmployeeID) _
                .Index(Function(t) t.UserID)
            
            CreateTable(
                "dbo.ParkingLot",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False),
                        .Type = c.Int(nullable := False),
                        .Status = c.Int(nullable := False),
                        .Height = c.Double(nullable := False),
                        .Width = c.Double(nullable := False),
                        .X = c.Double(nullable := False),
                        .Y = c.Double(nullable := False),
                        .Parking_Id = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Parking", Function(t) t.Parking_Id) _
                .Index(Function(t) t.Parking_Id)
            
            CreateTable(
                "dbo.Parking",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Name = c.String(nullable := False, maxLength := 64),
                        .Capacity = c.Int(nullable := False),
                        .Shape = c.String(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .Index(Function(t) t.Name, unique := True)
            
            CreateTable(
                "dbo.AspNetUsers",
                Function(c) New With
                    {
                        .Id = c.String(nullable := False, maxLength := 128),
                        .FirstName = c.String(nullable := False),
                        .LastName = c.String(nullable := False),
                        .JoinDate = c.DateTime(nullable := False),
                        .Email = c.String(maxLength := 256),
                        .EmailConfirmed = c.Boolean(nullable := False),
                        .PasswordHash = c.String(),
                        .SecurityStamp = c.String(),
                        .PhoneNumber = c.String(),
                        .PhoneNumberConfirmed = c.Boolean(nullable := False),
                        .TwoFactorEnabled = c.Boolean(nullable := False),
                        .LockoutEndDateUtc = c.DateTime(),
                        .LockoutEnabled = c.Boolean(nullable := False),
                        .AccessFailedCount = c.Int(nullable := False),
                        .UserName = c.String(nullable := False, maxLength := 256)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .Index(Function(t) t.UserName, unique := True, name := "UserNameIndex")
            
            CreateTable(
                "dbo.AspNetUserClaims",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .UserId = c.String(nullable := False, maxLength := 128),
                        .ClaimType = c.String(),
                        .ClaimValue = c.String()
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.AspNetUsers", Function(t) t.UserId, cascadeDelete := True) _
                .Index(Function(t) t.UserId)
            
            CreateTable(
                "dbo.AspNetUserLogins",
                Function(c) New With
                    {
                        .LoginProvider = c.String(nullable := False, maxLength := 128),
                        .ProviderKey = c.String(nullable := False, maxLength := 128),
                        .UserId = c.String(nullable := False, maxLength := 128)
                    }) _
                .PrimaryKey(Function(t) New With { t.LoginProvider, t.ProviderKey, t.UserId }) _
                .ForeignKey("dbo.AspNetUsers", Function(t) t.UserId, cascadeDelete := True) _
                .Index(Function(t) t.UserId)
            
            CreateTable(
                "dbo.AspNetUserRoles",
                Function(c) New With
                    {
                        .UserId = c.String(nullable := False, maxLength := 128),
                        .RoleId = c.String(nullable := False, maxLength := 128)
                    }) _
                .PrimaryKey(Function(t) New With { t.UserId, t.RoleId }) _
                .ForeignKey("dbo.AspNetUsers", Function(t) t.UserId, cascadeDelete := True) _
                .ForeignKey("dbo.AspNetRoles", Function(t) t.RoleId, cascadeDelete := True) _
                .Index(Function(t) t.UserId) _
                .Index(Function(t) t.RoleId)
            
            CreateTable(
                "dbo.Vehicle",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .LicensePlate = c.String(nullable := False, maxLength := 20),
                        .Brand = c.String(maxLength := 32),
                        .Color = c.String(maxLength := 32),
                        .Type = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .Index(Function(t) t.LicensePlate, unique := True)
            
            CreateTable(
                "dbo.AspNetRoles",
                Function(c) New With
                    {
                        .Id = c.String(nullable := False, maxLength := 128),
                        .Name = c.String(nullable := False, maxLength := 256)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .Index(Function(t) t.Name, unique := True, name := "RoleNameIndex")
            
            CreateTable(
                "dbo.VisitorAdmission",
                Function(c) New With
                    {
                        .Visitor_Id = c.Int(nullable := False),
                        .Admission_Id = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) New With { t.Visitor_Id, t.Admission_Id }) _
                .ForeignKey("dbo.Visitor", Function(t) t.Visitor_Id, cascadeDelete := True) _
                .ForeignKey("dbo.Admission", Function(t) t.Admission_Id, cascadeDelete := True) _
                .Index(Function(t) t.Visitor_Id) _
                .Index(Function(t) t.Admission_Id)
            
            CreateTable(
                "dbo.Employee",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False),
                        .Floor = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Person", Function(t) t.Id) _
                .Index(Function(t) t.Id)
            
            CreateTable(
                "dbo.Visitor",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False),
                        .PhoneNumber = c.String(),
                        .Nationality = c.Int(nullable := False),
                        .IsDriver = c.Boolean(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id) _
                .ForeignKey("dbo.Person", Function(t) t.Id) _
                .Index(Function(t) t.Id)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Visitor", "Id", "dbo.Person")
            DropForeignKey("dbo.Employee", "Id", "dbo.Person")
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles")
            DropForeignKey("dbo.Admission", "EmployeeID", "dbo.Employee")
            DropForeignKey("dbo.Reservation", "VisitorID", "dbo.Visitor")
            DropForeignKey("dbo.VisitorAdmission", "Admission_Id", "dbo.Admission")
            DropForeignKey("dbo.VisitorAdmission", "Visitor_Id", "dbo.Visitor")
            DropForeignKey("dbo.Reservation", "VehicleID", "dbo.Vehicle")
            DropForeignKey("dbo.Admission", "VehicleID", "dbo.Vehicle")
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers")
            DropForeignKey("dbo.Admission", "UserID", "dbo.AspNetUsers")
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers")
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers")
            DropForeignKey("dbo.Reservation", "UserID", "dbo.AspNetUsers")
            DropForeignKey("dbo.Reservation", "ParkingLotID", "dbo.ParkingLot")
            DropForeignKey("dbo.ParkingLot", "Parking_Id", "dbo.Parking")
            DropForeignKey("dbo.Admission", "ParkingLotID", "dbo.ParkingLot")
            DropForeignKey("dbo.Reservation", "EmployeeID", "dbo.Employee")
            DropIndex("dbo.Visitor", New String() { "Id" })
            DropIndex("dbo.Employee", New String() { "Id" })
            DropIndex("dbo.VisitorAdmission", New String() { "Admission_Id" })
            DropIndex("dbo.VisitorAdmission", New String() { "Visitor_Id" })
            DropIndex("dbo.AspNetRoles", "RoleNameIndex")
            DropIndex("dbo.Vehicle", New String() { "LicensePlate" })
            DropIndex("dbo.AspNetUserRoles", New String() { "RoleId" })
            DropIndex("dbo.AspNetUserRoles", New String() { "UserId" })
            DropIndex("dbo.AspNetUserLogins", New String() { "UserId" })
            DropIndex("dbo.AspNetUserClaims", New String() { "UserId" })
            DropIndex("dbo.AspNetUsers", "UserNameIndex")
            DropIndex("dbo.Parking", New String() { "Name" })
            DropIndex("dbo.ParkingLot", New String() { "Parking_Id" })
            DropIndex("dbo.Reservation", New String() { "UserID" })
            DropIndex("dbo.Reservation", New String() { "EmployeeID" })
            DropIndex("dbo.Reservation", New String() { "VehicleID" })
            DropIndex("dbo.Reservation", New String() { "ParkingLotID" })
            DropIndex("dbo.Reservation", New String() { "VisitorID" })
            DropIndex("dbo.Person", New String() { "Dni" })
            DropIndex("dbo.Admission", New String() { "UserID" })
            DropIndex("dbo.Admission", New String() { "ParkingLotID" })
            DropIndex("dbo.Admission", New String() { "VehicleID" })
            DropIndex("dbo.Admission", New String() { "EmployeeID" })
            DropTable("dbo.Visitor")
            DropTable("dbo.Employee")
            DropTable("dbo.VisitorAdmission")
            DropTable("dbo.AspNetRoles")
            DropTable("dbo.Vehicle")
            DropTable("dbo.AspNetUserRoles")
            DropTable("dbo.AspNetUserLogins")
            DropTable("dbo.AspNetUserClaims")
            DropTable("dbo.AspNetUsers")
            DropTable("dbo.Parking")
            DropTable("dbo.ParkingLot")
            DropTable("dbo.Reservation")
            DropTable("dbo.Person")
            DropTable("dbo.Admission")
        End Sub
    End Class
End Namespace
