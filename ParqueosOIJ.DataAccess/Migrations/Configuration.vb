Imports System.Data.Entity.Migrations
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports ParqueosOIJ.Model
Imports System.Data.Entity.Validation

Namespace Migrations

    Friend NotInheritable Class Configuration
        Inherits DbMigrationsConfiguration(Of ApplicationContext)

        Public Sub New()
            AutomaticMigrationsEnabled = False
        End Sub

        Protected Overrides Sub Seed(context As ApplicationContext)
            Dim roleStore = New RoleStore(Of IdentityRole)(context)
            Dim roleManager = New RoleManager(Of IdentityRole)(roleStore)
            Dim roleAdmin = New IdentityRole With {.Name = "Admin"}
            Dim roleOfficer = New IdentityRole With {.Name = "Officer"}

            roleManager.Create(roleAdmin)
            roleManager.Create(roleOfficer)

            ' Creates the superAdmin user with login credentials dummy@dev.net:Admin@123
            Dim userStore = New UserStore(Of User)(context)
            Dim userManager = New ApplicationUserManager(userStore)

            If Not context.Users.Any(Function(u) u.Email = "dummy@dev.net") Then
                Dim superAdmin = New User() With {
                        .Email = "dummy@dev.net",
                        .EmailConfirmed = True,
                        .UserName = "dummy@dev.net",
                        .FirstName = "Super",
                        .LastName = "Admin",
                        .JoinDate = Date.Now.AddMonths(-1)}
                Try

                    Dim result = userManager.Create(superAdmin, "Admin@123")
                Catch ex As DbEntityValidationException
                    Console.WriteLine(ex.EntityValidationErrors)
                End Try


                userManager.AddToRole(superAdmin.Id, "Admin")
            End If

            Try
                context.Parkings.AddOrUpdate(
                Function(p) p.Name,
                New Parking With {.Name = "Externo",
                                        .Capacity = 63,
                                        .Shape = "m 63.166767,25.956554 0,833.776216 375.581843,0 0,75.17693 274.52122,0 0,-399.80202 -72.40209,0 0,-509.151126 -577.700973,0 z"})
                context.SaveChanges()
                Dim parking1 As Parking
                parking1 = context.Parkings.Select(Function(p) p).Where(Function(p) p.Name = "Externo").FirstOrDefault()

#Region "parking-lots"
                context.ParkingLots.AddOrUpdate(
                Function(p) p.Id,
                New ParkingLot With {.Id = 1, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 128.66339, .Parking = parking1},
                New ParkingLot With {.Id = 2, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 177.38437, .Parking = parking1},
                New ParkingLot With {.Id = 3, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 226.05637, .Parking = parking1},
                New ParkingLot With {.Id = 4, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 274.84222, .Parking = parking1},
                New ParkingLot With {.Id = 5, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 323.79922, .Parking = parking1},
                New ParkingLot With {.Id = 6, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 372.61319, .Parking = parking1},
                New ParkingLot With {.Id = 7, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 421.28424, .Parking = parking1},
                New ParkingLot With {.Id = 8, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 470.1275, .Parking = parking1},
                New ParkingLot With {.Id = 9, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Jefatura, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 518.97119, .Parking = parking1},
                New ParkingLot With {.Id = 10, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 567.9422, .Parking = parking1},
                New ParkingLot With {.Id = 11, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 616.64221, .Parking = parking1},
                New ParkingLot With {.Id = 12, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 664.41412, .Parking = parking1},
                New ParkingLot With {.Id = 13, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 712.2713, .Parking = parking1},
                New ParkingLot With {.Id = 14, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 760.4856, .Parking = parking1},
                New ParkingLot With {.Id = 15, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 73.137665, .Y = 809.24261, .Parking = parking1},
                New ParkingLot With {.Id = 16, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 184.5376, .Parking = parking1},
                New ParkingLot With {.Id = 17, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 184.5376, .Parking = parking1},
                New ParkingLot With {.Id = 18, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 234.1044, .Parking = parking1},
                New ParkingLot With {.Id = 19, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 234.10458, .Parking = parking1},
                New ParkingLot With {.Id = 20, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 283.67609, .Parking = parking1},
                New ParkingLot With {.Id = 21, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 283.6756, .Parking = parking1},
                New ParkingLot With {.Id = 22, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 332.76004, .Parking = parking1},
                New ParkingLot With {.Id = 23, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 332.75958, .Parking = parking1},
                New ParkingLot With {.Id = 24, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 381.29187, .Parking = parking1},
                New ParkingLot With {.Id = 25, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 381.2916, .Parking = parking1},
                New ParkingLot With {.Id = 26, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 430.19748, .Parking = parking1},
                New ParkingLot With {.Id = 27, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 430.1976, .Parking = parking1},
                New ParkingLot With {.Id = 28, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 479.69241, .Parking = parking1},
                New ParkingLot With {.Id = 29, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 479.6926, .Parking = parking1},
                New ParkingLot With {.Id = 30, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 529.80383, .Parking = parking1},
                New ParkingLot With {.Id = 31, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 529.80359, .Parking = parking1},
                New ParkingLot With {.Id = 32, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 251.48599, .Y = 579.07092, .Parking = parking1},
                New ParkingLot With {.Id = 33, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 579.07062, .Parking = parking1},
                New ParkingLot With {.Id = 34, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 250.48599, .Y = 628.96942, .Parking = parking1},
                New ParkingLot With {.Id = 35, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 348.629, .Y = 628.9696, .Parking = parking1},
                New ParkingLot With {.Id = 36, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 84.120117, .Width = 39.120121, .X = 251.53995, .Y = 765.33691, .Parking = parking1},
                New ParkingLot With {.Id = 37, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 84.120117, .Width = 39.120121, .X = 299.36493, .Y = 765.33673, .Parking = parking1},
                New ParkingLot With {.Id = 38, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 84.120117, .Width = 39.120121, .X = 347.17172, .Y = 765.43213, .Parking = parking1},
                New ParkingLot With {.Id = 39, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 84.120117, .Width = 39.120121, .X = 394.9397, .Y = 765.43213, .Parking = parking1},
                New ParkingLot With {.Id = 40, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 95.874153, .Width = 38.752327, .X = 449.56485, .Y = 828.0282, .Parking = parking1},
                New ParkingLot With {.Id = 41, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 95.874153, .Width = 38.752327, .X = 496.90283, .Y = 828.0282, .Parking = parking1},
                New ParkingLot With {.Id = 42, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 618.36798, .Y = 883.94659, .Parking = parking1},
                New ParkingLot With {.Id = 43, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 618.18799, .Y = 834.79443, .Parking = parking1},
                New ParkingLot With {.Id = 44, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 618.18799, .Y = 785.91461, .Parking = parking1},
                New ParkingLot With {.Id = 45, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 618.18799, .Y = 736.97162, .Parking = parking1},
                New ParkingLot With {.Id = 46, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 84.120117, .Width = 39.120121, .X = 651.86853, .Y = 610.30212, .Parking = parking1},
                New ParkingLot With {.Id = 47, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 597.44257, .Parking = parking1},
                New ParkingLot With {.Id = 48, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 548.58557, .Parking = parking1},
                New ParkingLot With {.Id = 49, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 499.60959, .Parking = parking1},
                New ParkingLot With {.Id = 50, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 451.01459, .Parking = parking1},
                New ParkingLot With {.Id = 51, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 402.20059, .Parking = parking1},
                New ParkingLot With {.Id = 52, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 353.38559, .Parking = parking1},
                New ParkingLot With {.Id = 53, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 305.01453, .Parking = parking1},
                New ParkingLot With {.Id = 54, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 256.27158, .Parking = parking1},
                New ParkingLot With {.Id = 55, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 207.44356, .Parking = parking1},
                New ParkingLot With {.Id = 56, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 159.20059, .Parking = parking1},
                New ParkingLot With {.Id = 57, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 546.28601, .Y = 110.55759, .Parking = parking1},
                New ParkingLot With {.Id = 58, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 540.95203, .Y = 51.78059, .Parking = parking1},
                New ParkingLot With {.Id = 59, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 40.209614, .Width = 85.209618, .X = 446.15701, .Y = 51.78059, .Parking = parking1},
                New ParkingLot With {.Id = 60, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Oficial, .Height = 84.120117, .Width = 39.120121, .X = 394.55994, .Y = 36.273144, .Parking = parking1},
                New ParkingLot With {.Id = 61, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Visitas, .Height = 84.120117, .Width = 39.120121, .X = 346.93097, .Y = 36.273144, .Parking = parking1},
                New ParkingLot With {.Id = 62, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Visitas, .Height = 84.120117, .Width = 39.120121, .X = 299.23993, .Y = 36.273144, .Parking = parking1},
                New ParkingLot With {.Id = 63, .Status = StatusEnum.Disponible, .Type = ParkingLotType.Visitas, .Height = 84.120117, .Width = 39.120121, .X = 251.481, .Y = 36.273144, .Parking = parking1}
            )
#End Region
                context.SaveChanges()
            Catch ex As DbEntityValidationException
                If Debugger.IsAttached = False Then
                    Debugger.Launch()
                End If
            End Try

            Try
                context.Employees.AddOrUpdate(
                    Function(e) e.Dni,
                    New Employee With {.Dni = 1, .Email = "v.putin@oij.cr", .Name = "Vladimir Putin", .Floor = 1},
                    New Employee With {.Dni = 2, .Email = "agonza@oij.cr", .Name = "Andra Gonz�lez", .Floor = 1},
                    New Employee With {.Dni = 3, .Email = "s.esudo@oij.cr", .Name = "Sa�l Esudo Zen", .Floor = 2},
                    New Employee With {.Dni = 4, .Email = "rarce@oij.cr", .Name = "Rodolfo Arce", .Floor = 2},
                    New Employee With {.Dni = 5, .Email = "jsp@oij.cr", .Name = "Josseline Sojo P�rez", .Floor = 3},
                    New Employee With {.Dni = 6, .Email = "csesto@oij.cr", .Name = "Camilo Sesto", .Floor = 4},
                    New Employee With {.Dni = 7, .Email = "jaraya@oij.cr", .Name = "Juana Araya", .Floor = 3},
                    New Employee With {.Dni = 8, .Email = "mm@oij.cr", .Name = "Melannie Mart�nez", .Floor = 5},
                    New Employee With {.Dni = 9, .Email = "ss@oij.cr", .Name = "Susan S�nchez", .Floor = 4},
                    New Employee With {.Dni = 10, .Email = "ajuanjo@oij.cr", .Name = "Juan Jos� Quesada", .Floor = 5}
                )
                context.SaveChanges()
            Catch ex As DbEntityValidationException
                If Debugger.IsAttached = False Then
                    Debugger.Launch()
                End If
            End Try
        End Sub
    End Class
End Namespace
