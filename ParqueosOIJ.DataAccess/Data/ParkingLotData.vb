﻿Imports ParqueosOIJ.Model
Imports System.Data.Entity
Imports System.Data.Entity.Validation

Public Class ParkingLotData

    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    Public Function Find(number As Integer) As ParkingLot
        Return _db.ParkingLots _
            .Include(Function(p) p.Admissions) _
            .Select(Function(p) p) _
            .Where(Function(p) p.Id = number) _
            .FirstOrDefault()
    End Function

    Public Function FindAll() As ICollection(Of ParkingLot)
        Return _db.ParkingLots _
            .Include(Function(p) p.Reservations) _
            .Select(Function(p) p) _
            .ToList()
    End Function

    ''' <summary>
    ''' Busca todos los espacios de parqueo que pertenecen a un
    ''' parqueo especifico
    ''' </summary>
    ''' <param name="parkingId">El id de parqueo</param>
    ''' <returns>Lista de espacios de parqueo pertenecientes al parqueo dado.</returns>
    Public Function FindByParking(parkingId As Integer) As ICollection(Of ParkingLot)
        Return _db.ParkingLots _
            .Include(Function(p) p.Parking) _
            .Include(Function(p) p.Admissions) _
            .Include(Function(p) p.Reservations) _
            .Select(Function(p) p) _
            .Where(Function(p) p.Parking.Id = parkingId) _
            .ToList()
    End Function

    Public Function Save(parkingLot As ParkingLot) As ParkingLot
        If (parkingLot.Id <> 0) Then
            _db.Entry(parkingLot).State = EntityState.Modified
        Else
            _db.ParkingLots.Add(parkingLot)
        End If

        Try
            _db.SaveChanges()
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return parkingLot
    End Function

    Public Function Delete(id As Integer) As ParkingLot
        Dim pl As ParkingLot
        pl = Find(id)
        _db.ParkingLots.Remove(pl)

        Try
            _db.SaveChanges()
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return pl
    End Function

    Public Function Exist(id As Integer) As Boolean
        Return _db.ParkingLots.Count(Function(p) p.Id = id) > 0
    End Function
End Class
