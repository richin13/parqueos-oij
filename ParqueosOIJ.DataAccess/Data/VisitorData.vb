﻿Imports System.Data.Entity.Validation
Imports ParqueosOIJ.Model

Public Class VisitorData

    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    Public Function Find(id As Integer) As Visitor
        Return _db.Visitors.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Visitor)
        Return _db.Visitors.ToList()
    End Function

    Public Function FindByDni(dni As String) As Visitor
        Return _db.Visitors _
            .Select(Function(v) v) _
            .Where(Function(v) v.Dni = dni) _
            .FirstOrDefault()
    End Function

    Public Function Save(visitor As Visitor) As Visitor
        _db.Visitors.Add(visitor)

        Try
            _db.SaveChanges()
        Catch ex As DbEntityValidationException
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return visitor
    End Function

    Public Function Delete(dni As String) As Visitor
        Dim visitor As Visitor
        visitor = FindByDni(dni)

        _db.Visitors.Remove(visitor)

        Try
            _db.SaveChanges()
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return visitor
    End Function

    Public Function Update(visitor As Visitor) As Visitor
        _db.Entry(visitor).State = Entity.EntityState.Modified

        Try
            _db.SaveChanges()
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return visitor
    End Function

    Public Function Search(key As String) As ICollection(Of Visitor)
        Return _db.Visitors _
            .Select(Function(v) v) _
            .Where(Function(v) v.Dni.Contains(key) Or v.Name.Contains(key) Or v.Email.Contains(key)) _
            .ToList()
    End Function

End Class
