﻿Imports System.Data.Entity.Validation
Imports ParqueosOIJ.Model

Public Class VehicleData
    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    ''' <summary>
    ''' Busca un vehículo dada su llave o ID
    ''' </summary>
    ''' <param name="id">El id a buscar</param>
    ''' <returns>El vehículo encontrado</returns>
    Public Function Find(id As Integer) As Vehicle
        Return _db.Vehicles.Find(id)
    End Function

    ''' <summary>
    ''' Devuelve todos los vehículos guardados en la base de datos.
    ''' </summary>
    ''' <returns>Lista de todos lo vehículos</returns>
    Public Function FindAll() As ICollection(Of Vehicle)
        Return _db.Vehicles.ToList()
    End Function

    ''' <summary>
    ''' Busca un vehículo en la base de datos dado su número de placa
    ''' </summary>
    ''' <param name="licensePlate">Número de placa del vehículo a buscar</param>
    ''' <returns>El vehículo con la placa dada, nulo si no existe</returns>
    Public Function FindByLicensePlate(licensePlate As String) As Vehicle
        Return _db.Vehicles _
            .Select(Function(v) v) _
            .Where(Function(v) v.LicensePlate = licensePlate) _
            .FirstOrDefault()
    End Function

    ''' <summary>
    ''' Busca un vehículo en la base de datos dado su número de placa,
    ''' si no existe previamente lo guarda.
    ''' </summary>
    ''' <param name="vehicle">El vehículo a buscar (o guardar)</param>
    ''' <returns>El vehículo encontrado o recién guardado, según corresponda</returns>
    ''' <exception cref="DbEntityValidationException">En caso de que falle la validación</exception>
    Private Function FindOrCreateByLicensePlate(vehicle As Vehicle) As Vehicle ' for further use see VehicleBusiness.FindOrCreateByLicensePlate
        Dim _vehicle As Vehicle
        _vehicle = FindByLicensePlate(vehicle.LicensePlate)

        If IsNothing(_vehicle) Then
            Return Save(vehicle)
        End If

        Return _vehicle
    End Function

    ''' <summary>
    ''' Guarda un nuevo vehículo en la base de datos
    ''' </summary>
    ''' <param name="vehicle"></param>
    ''' <returns>El vehículo recién guardado</returns>
    ''' <exception cref="DbEntityValidationException">En caso de que falle la validación</exception>
    Public Function Save(vehicle As Vehicle) As Vehicle
        _db.Vehicles.Add(vehicle)

        Try
            _db.SaveChanges()
        Catch ex As DbEntityValidationException
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return vehicle
    End Function

    ''' <summary>
    ''' Verifica si existe un vehículo en la base de datos.
    ''' 
    ''' </summary>
    ''' <param name="licensePlate">El número de placa del vehículo</param>
    ''' <returns>True si el vehículo existe, false en caso contrario</returns>
    Function Exists(licensePlate As String) As Boolean
        Return _db.Vehicles.Count(Function(v) v.LicensePlate = licensePlate) > 0
    End Function

End Class
