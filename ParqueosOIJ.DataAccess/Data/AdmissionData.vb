﻿Imports ParqueosOIJ.Model
Imports System.Data.Entity
Imports System.Data.Entity.Validation

Public Class AdmissionData

    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    Public Function Find(id As Integer) As Admission
        Return _db.Admissions _
            .Include(Function(a) a.Visitors) _
            .Include(Function(a) a.Vehicle) _
            .Include(Function(a) a.User) _
            .Select(Function(a) a) _
            .Where(Function(a) a.Id = id) _
            .FirstOrDefault()
    End Function

    Public Function FindAll() As ICollection(Of Admission)
        Return _db.Admissions _
            .Include(Function(a) a.Visitors) _
            .Include(Function(a) a.Vehicle) _
            .Include(Function(a) a.Employee) _
            .Select(Function(a) a).ToList()
    End Function

    Public Function FindAllByStatus(status As AdmissionStatus) As ICollection(Of Admission)
        Return _db.Admissions _
            .Include(Function(a) a.Visitors) _
            .Include(Function(a) a.Vehicle) _
            .Include(Function(a) a.ParkingLot) _
            .Select(Function(a) a) _
            .Where(Function(a) a.Status = status) _
            .ToList()
    End Function

    'Todos los filtros
    Public Function FindByDateRangeCedLicensePlate(car As String, ced As String, lowerBound As DateTime, upperBound As DateTime) As IEnumerable(Of Admission)
        Dim results As IQueryable(Of Admission) = (From admission In _db.Admissions, vis In admission.Visitors
                                                   Where ((admission.EntryDate >= lowerBound And admission.EntryDate <= upperBound) Or (admission.DepartureDate >= lowerBound And admission.DepartureDate <= upperBound)) And
                                                        admission.Vehicle.LicensePlate = car And
                                                       vis.IsDriver = True And vis.Dni = ced
                                                   Select admission).Distinct() _
                                                    .Include(Function(a) a.Visitors) _
                                                    .Include(Function(a) a.Vehicle)
        Return results.ToList()
    End Function

    'Solo rango de recha
    Public Function FindByDateRange(lowerBound As DateTime, upperBound As DateTime) As IEnumerable(Of Admission)
        Return _db.Admissions _
            .Include(Function(a) a.Visitors) _
            .Include(Function(a) a.Vehicle) _
            .Include(Function(a) a.Visitors) _
            .Select(Function(a) a) _
            .Where(Function(a) ((a.EntryDate >= lowerBound And a.EntryDate <= upperBound) Or (a.DepartureDate >= lowerBound And a.DepartureDate <= upperBound))).ToList()
    End Function

    'Rango de fecha y licencia
    Public Function FindByDateRangeLicensePlate(car As String, lowerBound As DateTime, upperBound As DateTime) As IEnumerable(Of Admission)
        Return _db.Admissions _
            .Include(Function(a) a.Visitors) _
            .Include(Function(a) a.Vehicle) _
            .Include(Function(a) a.Visitors) _
            .Select(Function(a) a) _
            .Where(Function(a) ((a.EntryDate >= lowerBound And a.EntryDate <= upperBound) Or (a.DepartureDate >= lowerBound And a.DepartureDate <= upperBound)) And a.Vehicle.LicensePlate = car).ToList()
    End Function

    'Rango de fecha y cedula
    Public Function FindByDateRangeCed(ced As String, lowerBound As DateTime, upperBound As DateTime) As IEnumerable(Of Admission)

        Dim results As IQueryable(Of Admission) = (From admission In _db.Admissions, vis In admission.Visitors
                                                   Where ((admission.EntryDate >= lowerBound And admission.EntryDate <= upperBound) Or (admission.DepartureDate >= lowerBound And admission.DepartureDate <= upperBound)) And
                                                       vis.IsDriver = True And vis.Dni = ced
                                                   Select admission).Distinct() _
                                                    .Include(Function(a) a.Visitors) _
                                                    .Include(Function(a) a.Vehicle)
        Return results.ToList()
    End Function

    Public Function FindByDate(theDate As Date) As ICollection(Of Admission)
        Return _db.Admissions _
            .Include(Function(a) a.Visitors) _
            .Include(Function(a) a.Vehicle) _
            .Include(Function(a) a.Visitors) _
            .Select(Function(a) a) _
            .Where(Function(a) a.EntryDate.Day = theDate.Day And a.EntryDate.Month = theDate.Month).ToList()
    End Function

    Public Function Search(key As String) As ICollection(Of Admission)

        Dim results As IQueryable(Of Admission) = (From admission In _db.Admissions, vis In _db.Visitors
                                                   Where admission.Visitors.Contains(vis) And
                                                      (admission.Vehicle.LicensePlate.Contains(key) Or
                                                      admission.Vehicle.Brand.Contains(key) Or
                                                      vis.Dni.Contains(key) Or
                                                      vis.Name.Contains(key))
                                                   Select admission).Distinct() _
                                                    .Include(Function(a) a.Visitors) _
                                                    .Include(Function(a) a.Vehicle)
        Return results.ToList()
    End Function

    Public Function Save(admission As Admission) As Admission
        Using _db
            Using transaction As DbContextTransaction = _db.Database.BeginTransaction
                Try
                    For Each vis As Visitor In admission.Visitors
                        If vis.Id <> 0 Then
                            _db.Visitors.Attach(vis)
                        Else
                            _db.Visitors.Add(vis)
                        End If
                    Next
                    If admission.Vehicle.Id <> 0 Then
                        _db.Vehicles.Attach(admission.Vehicle)
                    End If

                    If Not IsNothing(admission.Employee) Then
                        _db.Employees.Attach(admission.Employee)
                    End If

                    _db.Entry(admission.ParkingLot).State = EntityState.Modified
                    admission = _db.Admissions.Add(admission)
                    _db.SaveChanges()
                    transaction.Commit()
                Catch ex As Exception
                    transaction.Rollback()
                    Debug.WriteLine(ex.Message)
                    Throw ex
                End Try
            End Using
        End Using

        Return admission
    End Function

    Public Function Delete(admission As Admission) As Admission
        admission = _db.Admissions.Remove(admission)
        Try
            _db.SaveChanges()
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try
        Return admission
    End Function

    Public Function Update(admission As Admission)
        Using _db
            Using transaction As DbContextTransaction = _db.Database.BeginTransaction
                Try
                    _db.Entry(admission).State = EntityState.Modified
                    _db.Entry(admission.ParkingLot).State = EntityState.Modified
                    _db.SaveChanges()
                    transaction.Commit()
                Catch ex As Exception
                    transaction.Rollback()
                    Debug.WriteLine(ex.Message)
                    Throw ex
                End Try
            End Using
        End Using

        Return admission
    End Function

End Class
