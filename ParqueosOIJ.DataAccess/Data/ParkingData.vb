﻿Imports ParqueosOIJ.Model
Imports System.Data.Entity

Public Class ParkingData

    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    Public Function Find(id As Integer) As Parking
        Return _db.Parkings _
            .Include(Function(p) p.ParkingLots) _
            .Select(Function(p) p) _
            .Where(Function(p) p.Id = id).First()
    End Function

    Public Function FindAll() As ICollection(Of Parking)
        Return _db.Parkings.ToList()
    End Function

    Public Function Exists(id As Integer) As Boolean
        Return _db.Parkings.Count(Function(p) p.Id = id) > 0
    End Function

End Class
