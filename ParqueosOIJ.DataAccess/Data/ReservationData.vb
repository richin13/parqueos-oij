﻿Imports ParqueosOIJ.Model
Imports System.Data.Entity
Imports System.Data.Entity.Validation

Public Class ReservationData
    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    ''' <summary>
    ''' Busca una reservación dado su ID.
    ''' Si no existe retorna null
    ''' </summary>
    ''' <param name="id">El id de la reservación a buscar</param>
    ''' <returns>La reservación cuyo ID corresponde al número dado,
    ''' nulo en caso de que no exista una reservación con ese ID</returns>
    Public Function Find(id As Integer) As Reservation
        Return _db.Reservations _
            .Include(Function(r) r.Visitor) _
            .Include(Function(r) r.Vehicle) _
            .Include(Function(r) r.Employee) _
            .Include(Function(r) r.User) _
            .Select(Function(r) r) _
            .Where(Function(r) r.Id = id) _
            .FirstOrDefault()
    End Function

    ''' <summary>
    ''' Obtiene todas las reservaciones existentes en la base de datos. Ordena los resultados
    ''' de más reciente a más antigüo.
    ''' </summary>
    ''' <returns>La lista ordenada de reservacioens.</returns>
    Public Function FindAll() As ICollection(Of Reservation)
        Return _db.Reservations _
        .Include(Function(r) r.Vehicle) _
        .Include(Function(r) r.Visitor) _
        .Include(Function(r) r.Employee) _
        .OrderBy(Function(r) r.RDate) _
        .ToList()
    End Function

    ''' <summary>
    ''' Selecciona todas las reservaciones cuyo estado es igual al estado
    ''' dado por parámetro. Ordena la lista resultante de manera ascendente
    ''' basándose en la fecha en que se realizó cada reservación.
    ''' 
    ''' Vea ParqueosOIJ.Model.ReservationStatus para conocer cuáles son los estados válidos.
    ''' </summary>
    ''' <param name="status">El estado</param>
    ''' <returns>La lista de reservaciones cuyo estado es igual a "status" </returns>
    Public Function FindByStatus(status As ReservationStatus) As ICollection(Of Reservation)
        Return _db.Reservations _
            .Include(Function(r) r.Visitor) _
            .Include(Function(r) r.Vehicle) _
            .Select(Function(a) a) _
            .Where(Function(a) a.Status = status) _
            .OrderBy(Function(r) r.RDate) _
            .ToList()
    End Function

    ''' <summary>
    ''' Obtiene una lista de reservaciones cuyo vehículo y visitante corresponden
    ''' a los valores dados por parámetro.
    ''' </summary>
    ''' <param name="vehicleLicensePlate"></param>
    ''' <param name="visitorDni"></param>
    ''' <returns></returns>
    Public Function FindByLicensePlateAndDni(vehicleLicensePlate As String, visitorDni As String) As ICollection(Of Reservation)
        Return _db.Reservations _
            .Include(Function(r) r.Visitor) _
            .Include(Function(r) r.Vehicle) _
            .Select(Function(r) r) _
            .Where(Function(r) r.Vehicle.LicensePlate = vehicleLicensePlate And r.Visitor.Dni = visitorDni) _
            .OrderBy(Function(r) r.RDate) _
            .ToList()
    End Function

    ''' <summary>
    ''' Guarda una reservación en la base de datos.
    ''' </summary>
    ''' <param name="reservation">La reservación a guardar.</param>
    ''' <returns>La reservación guardado.</returns>
    Public Function Save(reservation As Reservation) As Reservation
        Using _db

            Using transaction As DbContextTransaction = _db.Database.BeginTransaction
                Try
                    If reservation.VisitorID = 0 Then
                        _db.Visitors.Add(reservation.Visitor)
                    Else
                        _db.Visitors.Attach(reservation.Visitor)
                    End If
                    reservation = _db.Reservations.Add(reservation)
                    _db.SaveChanges()
                    transaction.Commit()
                Catch ex As DbEntityValidationException
                    transaction.Rollback()
                    Debug.WriteLine(ex.Message)
                    Throw ex
                End Try
            End Using
        End Using

        Return reservation
    End Function

    ''' <summary>
    ''' Actualiza una reservación
    ''' </summary>
    ''' <param name="reservation">La reservación que se va a actualizar.</param>
    Public Function Update(reservation As Reservation) As Reservation
        _db.Entry(reservation).State = EntityState.Modified
        Try
            _db.SaveChanges()
        Catch ex As DbEntityValidationException
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try
        Return reservation
    End Function

End Class
