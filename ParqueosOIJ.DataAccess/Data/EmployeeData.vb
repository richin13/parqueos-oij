﻿Imports System.Data.Entity.Validation
Imports ParqueosOIJ.Model

Public Class EmployeeData

    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    Public Function Find(id As Integer) As Employee
        Return _db.Employees.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Employee)
        Return _db.Employees.ToList()
    End Function

    Public Function FindByDni(dni As String) As Employee
        Return _db.Employees _
            .Select(Function(v) v) _
            .Where(Function(v) v.Dni = dni) _
            .FirstOrDefault()
    End Function

    Public Function Save(employee As Employee) As Employee
        _db.Employees.Add(employee)

        Try
            _db.SaveChanges()
        Catch ex As DbEntityValidationException
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return employee
    End Function

    Public Function Delete(dni As String) As Employee
        Dim employee As Employee
        employee = FindByDni(dni)

        _db.Employees.Remove(employee)

        Try
            _db.SaveChanges()
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return employee
    End Function

    Public Function Update(employee As Employee) As Employee
        _db.Entry(employee).State = Entity.EntityState.Modified

        Try
            _db.SaveChanges()
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
            Throw ex
        End Try

        Return employee
    End Function

    Public Function Search(key As String) As ICollection(Of Employee)
        Return _db.Employees _
            .Select(Function(e) e) _
            .Where(Function(e) e.Dni.Contains(key) Or e.Name.Contains(key) Or e.Email.Contains(key)) _
            .ToList()
    End Function

    Function Exists(guid As String) As Boolean
        Return _db.Persons.Count(Function(p) p.Id = guid) > 0
    End Function

End Class
