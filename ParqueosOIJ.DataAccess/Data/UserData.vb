﻿Imports ParqueosOIJ.Model
Imports System.Data.Entity
Imports System.Data.Entity.Validation
Imports Microsoft.AspNet.Identity.EntityFramework

Public Class UserData

    Private _db As ApplicationContext

    Public Sub New()
        _db = New ApplicationContext()
    End Sub

    ''' <summary>
    ''' Busca un usuario dado su user-id.
    ''' </summary>
    ''' <param name="userId">El user-id del usuario</param>
    ''' <returns>El usuario encontrado</returns>
    Function Find(userId As String) As User
        Return _db.Users.Find(userId)
    End Function

    ''' <summary>
    ''' Retorna una lista de usuarios existentes en el sistema.
    ''' </summary>
    ''' <returns>Lista con todos los usuarios del sistema</returns>
    Function FindAll() As ICollection(Of User)
        Return _db.Users _
            .Include(Function(u) u.Roles) _
            .Select(Function(u) u) _
            .ToList()
    End Function

    ''' <summary>
    ''' Guarda los cambios realizados a un usuario.
    ''' Si el usuario ya existe, entonces lo actualiza con los nuevos valores,
    ''' si no existe, entonces lo crea. En cualquiera de los casos, retorna el
    ''' usuario
    ''' </summary>
    ''' <param name="user">El usuario a guardar o actualizar</param>
    ''' <returns>El usuario guardado o actualizado</returns>
    Function Save(user As User) As User
        Using _db
            Using transaction As DbContextTransaction = _db.Database.BeginTransaction
                Try
                    If String.IsNullOrEmpty(user.Id) Then
                        ' is saving a new one
                        _db.Users.Add(user)
                    Else
                        ' is updating
                        _db.Entry(user).State = EntityState.Modified
                    End If
                    _db.SaveChanges()
                    transaction.Commit()
                Catch ex As Exception
                    transaction.Rollback()
                    Debug.WriteLine(ex.Message)
                    Throw ex
                End Try
            End Using
        End Using

        Return user
    End Function

    ''' <summary>
    ''' Verifica si existe un usuario en la base de datos que
    ''' posea el user-id dado.
    ''' </summary>
    ''' <param name="id">user-id del usuario a buscar</param>
    ''' <returns>Si existe o no</returns>
    Function Exists(id As String) As Boolean
        Return _db.Users.Count(Function(u) u.Id = id) > 0
    End Function

    Function FindRole(id As String) As IdentityRole
        Return _db.Users.Include(Function(u) u.Roles).Select(Function(u) u.Roles)
    End Function

End Class
