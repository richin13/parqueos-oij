﻿Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

Public Class UserService

    Private _biz As UserBusiness

    Public Sub New()
        _biz = New UserBusiness()
    End Sub

    Function Find(id As String)
        Return _biz.Find(id)
    End Function

    Function FindAll() As ICollection(Of User)
        Return _biz.FindAll()
    End Function

    Function Save(user As User) As User
        Return _biz.Save(user)
    End Function

End Class
