﻿Imports System.EnterpriseServices
Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

<Transaction>
Public Class AdmissionService

    Private _biz As AdmissionBusiness
    Private _vehicleServ As VehicleService
    Private _visitorServ As VisitorService
    Private _emplServ As EmployeeService
    Private _plServ As ParkingLotService

    Public Sub New()
        _biz = New AdmissionBusiness()
        _vehicleServ = New VehicleService()
        _visitorServ = New VisitorService()
        _emplServ = New EmployeeService()
        _plServ = New ParkingLotService()
    End Sub

    Public Function Find(id As Integer) As Admission
        Return _biz.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Admission)
        Return _biz.FindAll()
    End Function

    Public Function FindAllActive() As ICollection(Of Admission)
        Return _biz.FindAllActive()
    End Function

    Public Function FindByDateRangeCedLicensePlate(car As String, ced As String, lower As String, upper As String) As IEnumerable(Of Admission)
        Return _biz.FindByDateRangeCedLicensePlate(car, ced, lower, upper)
    End Function

    'Solo rango de recha
    Public Function FindByDateRange(lowerBound As String, upperBound As String) As IEnumerable(Of Admission)
        Return _biz.FindByDateRange(lowerBound, upperBound)
    End Function

    'Rango de fecha y licencia
    Public Function FindByDateRangeLicensePlate(car As String, lowerBound As String, upperBound As String) As IEnumerable(Of Admission)
        Return _biz.FindByDateRangeLicensePlate(car, lowerBound, upperBound)

    End Function

    'Rango de fecha y cedula
    Public Function FindByDateRangeCed(ced As String, lowerBound As String, upperBound As String) As IEnumerable(Of Admission)
        Return _biz.FindByDateRangeCed(ced, lowerBound, upperBound)
    End Function

    Public Function GetByDate(myDate) As ICollection(Of Admission)
        Return _biz.FindByDate(myDate)
    End Function

    Public Function Search(key As String) As ICollection(Of Admission)
        Return _biz.Search(key)
    End Function

    ''' <summary>
    ''' Se está registrando la salida de un vehículo que se encontraba previamente
    ''' en el parqueo. Dicho vehículo está asociado a un ingreso activo que es el que se
    ''' va a modificar, cambiando su estado a Completado.
    ''' </summary>
    ''' <param name="parkingLotId">El id del espacio de parqueo donde se encuentra el vehículo</param>
    ''' <returns>El ingreso con su nuevo estado</returns>
    Public Function CheckOut(parkingLotId As Integer) As Admission
        Dim parkingLot As ParkingLot
        parkingLot = _plServ.Find(parkingLotId) ' tira NotFoundException si no existe

        Dim admission As Admission
        admission = parkingLot.GetActiveAdmission()

        If IsNothing(admission) And parkingLot.Status = StatusEnum.Ocupado Then
            parkingLot.Status = StatusEnum.Disponible
            _plServ.Save(parkingLot)
            Return admission
        End If

        Return _biz.CheckOut(admission)
    End Function

    Public Function Save(admission As Admission) As Admission
        ' A lot of stuff happens here

        Dim vehicle As Vehicle
        vehicle = _vehicleServ.FindOrCreate(admission.Vehicle)
        admission.Vehicle = vehicle
        admission.VehicleID = vehicle.Id

        admission.Visitors = FindAllVisitors(admission)

        If admission.Vehicle.Type = VehicleType.Visita Then
            Dim empl As Employee
            empl = _emplServ.Find(admission.Employee.Id)
            empl.Visits.Add(admission)
            admission.Employee = empl
        End If

        Dim pl As ParkingLot
        pl = _plServ.Find(admission.ParkingLotID) ' se usa mas adelante
        admission.ParkingLot = pl

        Return _biz.Save(admission)
    End Function

    ' this may take a while
    ' lets test it
    Private Function FindAllVisitors(admission As Admission) As ICollection(Of Visitor)
        Dim list As ICollection(Of Visitor) = New List(Of Visitor)()

        For Each v As Visitor In admission.Visitors
            Dim newVisitor As Visitor
            newVisitor = _visitorServ.FindOrCreate(v)
            newVisitor.Admissions.Add(admission)
            list.Add(newVisitor)
        Next

        Return list
    End Function

End Class
