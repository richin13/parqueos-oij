﻿Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

Public Class ParkingService
    Private _biz As ParkingBusiness
    Private _plServ As ParkingLotService
    Public Sub New()
        _biz = New ParkingBusiness()
        _plServ = New ParkingLotService()
    End Sub

    ''' <summary>
    ''' Busca un Parqueo dado su id
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    Public Function Find(id As Integer) As Parking
        Return _biz.Find(id)
    End Function

    ''' <summary>
    ''' Busca un parqueo dado su id y recupera la información completa
    ''' de sus espacios de parqueo (información de ingresos y reservaciones)
    ''' </summary>
    ''' <param name="id">Id del parqueo</param>
    ''' <returns>Parqueo</returns>
    Public Function FindWithDetails(id As Integer) As Parking
        Dim p As Parking
        p = _biz.Find(id)
        p.ParkingLots = _plServ.FindByParking(id)

        Return p
    End Function

    Public Function FindAll() As ICollection(Of Parking)
        Return _biz.FindAll()
    End Function

    Public Function Exists(id As Integer) As Boolean
        Return _biz.Exists(id)
    End Function

End Class
