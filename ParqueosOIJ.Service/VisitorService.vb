﻿Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

Public Class VisitorService

    Private _biz As VisitorBusiness

    Public Sub New()
        _biz = New VisitorBusiness()
    End Sub

    Public Function Find(id As Integer) As Visitor
        Return _biz.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Visitor)
        Return _biz.FindAll()
    End Function

    Public Function FindByDni(dni As String) As Visitor
        Return _biz.FindByDni(dni)
    End Function

    Public Function FindOrCreate(visitor As Visitor) As Visitor
        Dim vis As Visitor
        vis = FindOrCreateByDni(visitor.Dni)

        If IsNothing(vis) Then
            If _biz.Validate(visitor) Then
                Return visitor
            End If

            Throw New Exception("El visitante no es válido. Revise los datos e intente de nuevo")
        End If

        Return vis
    End Function

    ''' <summary>
    ''' Busca un visitante en la base de datos (usando él número de identificación)
    ''' si no existe ninguno, realiza la búsqueda en el servicio web y retorna los datos
    ''' encontrados. Por el momento retorna nulo en caso de que no esté en la bd porque
    ''' no existe ningún web service.
    ''' 
    ''' </summary>
    ''' <param name="dni">El número de identificación a buscar</param>
    ''' <returns></returns>
    Private Function FindOrCreateByDni(dni As String) As Visitor
        Dim visitor As Visitor
        visitor = FindByDni(dni)

        If IsNothing(visitor) Then
            Return Nothing
        End If

        Return visitor
    End Function

    Public Function Save(visitor As Visitor) As Visitor
        Return _biz.Save(visitor)
    End Function

    Public Function Update(visitor As Visitor) As Visitor
        Return _biz.Update(visitor)
    End Function

    Public Function Delete(dni As String) As Visitor
        Return _biz.Delete(dni)
    End Function

    Public Function Search(key As String) As ICollection(Of Visitor)
        Return _biz.Search(key)
    End Function

End Class
