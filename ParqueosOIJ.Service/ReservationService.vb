﻿Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

Public Class ReservationService

    Private _biz As ReservationBusiness
    Private _emplServ As EmployeeService
    Private _visitorServ As VisitorService
    Private _vehicleServ As VehicleService

    Public Sub New()
        _biz = New ReservationBusiness()
        _emplServ = New EmployeeService()
        _visitorServ = New VisitorService()
        _vehicleServ = New VehicleService()
    End Sub

    ''' <summary>
    ''' Busca la reservación cuyo ID (Llave primaria) corresponde al valor
    ''' de "id" dado como parámetro.
    ''' </summary>
    ''' <param name="id">El ID a buscar</param>
    ''' <returns>La reservación que corresponda al parámetro de búsqueda.</returns>
    Function Find(id As Integer)
        Return _biz.Find(id)
    End Function

    ''' <summary>
    ''' Guarda una nueva reservación en la base de datos.
    ''' </summary>
    ''' <param name="Reservation">La reservación que se va a guardar.</param>
    ''' <returns>La reservación guardada.</returns>
    Function Save(ByRef Reservation As Reservation)
        Dim empl As Person
        empl = _emplServ.Find(Reservation.Employee.Dni)
        Reservation.Employee = Nothing
        Reservation.EmployeeID = empl.Id

        Dim vis As Visitor
        vis = _visitorServ.FindOrCreate(Reservation.Visitor)
        Reservation.VisitorID = vis.Id

        Dim veh As Vehicle
        veh = _vehicleServ.FindOrCreate(Reservation.Vehicle)
        Reservation.Vehicle = Nothing
        Reservation.VehicleID = veh.Id

        _biz.Save(Reservation)

        ' Finally we can create a new notification or send an email
        ' That goes here
        ' TODO
        Return Reservation ' Check Reservation.Vehicle, Reservation.Visitor and Reservation.Employee value at this point
        ' I did, they're nothing at this point. Should I be worried? I'm sure LazyLoading is enabled.
    End Function

    ''' <summary>
    ''' Actualiza una reservación. Esto para cambiar su estado actual y/o
    ''' agregar algún comentario.
    ''' </summary>
    ''' <param name="reservation">La reservación que se va a actualizar.</param>
    Function Update(reservation As Reservation) As Reservation
        If reservation.Status = ReservationStatus.Rechazada Then
            ' Si el estado es rechazado se le quita el espacio que tenía asignado (si tenía)
            reservation.ParkingLotID = Nothing
        End If

        ' Send and email (or sms why not?) to the person that made the reservation.
        Return _biz.Update(reservation)
    End Function

    ''' <summary>
    ''' Busca todas las reservaciones de un visitante y su respectivo vehículo.
    ''' Las ordena en razón de Reservation.RDate
    ''' 
    ''' Podría arrojar un NotFoundException()
    ''' </summary>
    ''' <param name="vehicleLP">La placa del vehículo visitante.</param>
    ''' <param name="visitorDni">La cédula del visitante.</param>
    ''' <returns>La lista de todas reservaciones hechas para ese visitante y vehículo</returns>
    Function FindAllByLicensePlateAndDni(vehicleLP As String, visitorDni As String) As ICollection(Of Reservation)
        Return _biz.FindByLicensePlateAndDni(vehicleLP, visitorDni)
    End Function

    ''' <summary>
    ''' Busca una reservación dado el número de cédula del visitante y
    ''' la placa del vehículo con el cual registraron la reservación.
    ''' Como podrían existir varias reservaciones se selecciona la más reciente.
    ''' (Basándose en Reservation.RDate)
    ''' </summary>
    ''' <param name="vehicleLP">El número de placa del vehículo que visita</param>
    ''' <param name="visitorDni">La cédula de la persona que visita</param>
    ''' <returns>La reservación más reciente realizada para ese visitante y vehículo</returns>
    Function FindLastByLicensePlateAndDni(vehicleLP As String, visitorDni As String) As Reservation
        Return _biz.FindByLicensePlateAndDni(vehicleLP, visitorDni).First()
    End Function

    ''' <summary>
    ''' Encuentra todas las reservaciones que hayan en la base de datos.
    ''' </summary>
    ''' <returns></returns>
    Function FindAll() As ICollection(Of Reservation)
        Return _biz.FindAll()
    End Function

    'get reservation by status'
    Function FindByStatus(s As ReservationStatus) As ICollection(Of Reservation)
        Return _biz.FindByStatus(s)
    End Function
End Class
