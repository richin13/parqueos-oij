﻿Imports System.ComponentModel.DataAnnotations

Public Class AddAccountForm

    <Required>
    <Display(Name:="Nombre")>
    Public Property FirstName As String

    <Required>
    <Display(Name:="Apellidos")>
    Public Property LastName As String

    <Required>
    <EmailAddress>
    <Display(Name:="Email")>
    Public Property Email As String

    <Required>
    <StringLength(100, ErrorMessage:="La {0} debe contener al menos {2} caracteres de longitud", MinimumLength:=6)>
    <DataType(DataType.Password)>
    <Display(Name:="Contraseña")>
    Public Property Password As String

    <DataType(DataType.Password)>
    <Display(Name:="Confirme la contraseña")>
    <Compare("Password", ErrorMessage:="Las contraseñas no coinciden")>
    Public Property ConfirmPassword As String

    <Required>
    <Display(Name:="Rol")>
    Public Property Role As RolesEnum

    Public Enum RolesEnum
        Admin
        Officer
    End Enum
End Class

Public Class LoginForm
    <Required>
    <Display(Name:="Correo electrónico")>
    <EmailAddress>
    Public Property Email As String

    <Required>
    <DataType(DataType.Password)>
    <Display(Name:="Contraseña")>
    Public Property Password As String

    <Display(Name:="Recordar?")>
    Public Property RememberMe As Boolean
End Class