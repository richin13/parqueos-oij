﻿Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

Public Class ParkingLotService

    Private _biz As ParkingLotBusiness

    Public Sub New()
        _biz = New ParkingLotBusiness()
    End Sub

    Public Function Find(id As Integer) As ParkingLot
        Return _biz.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of ParkingLot)
        Return _biz.FindAll()
    End Function

    Public Function FindByParking(parkingId As Integer) As ICollection(Of ParkingLot)
        Return _biz.FindByParking(parkingId)
    End Function

    Public Function Save(pl As ParkingLot) As ParkingLot
        Return _biz.Save(pl)
    End Function

End Class
