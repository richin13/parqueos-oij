﻿Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

Public Class VehicleService

    Private _biz As VehicleBusiness

    Public Sub New()
        _biz = New VehicleBusiness()
    End Sub

    Public Function Find(id As Integer) As Vehicle
        Return _biz.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Vehicle)
        Return _biz.FindAll()
    End Function

    Public Function FindByLicensePlate(licensePlate As String) As Vehicle
        Return _biz.FindByLicensePlate(licensePlate)
    End Function

    ''' <summary>
    ''' Busca en la base de datos el vehículo con la placa dada.
    ''' Si este no existe previamente lo crea y lo guarda.
    ''' 
    ''' </summary>
    ''' <param name="licensePlate">La placa del vehículo a buscar.</param>
    ''' <returns>El vehículo encontrado o creado</returns>
    Private Function FindOrCreateByLicensePlate(licensePlate As String) As Vehicle

        Dim vehicle As Vehicle
        vehicle = FindByLicensePlate(licensePlate)

        If IsNothing(vehicle) Then
            ' Then, retrieve the vehicle's information from the Web Service
            ' Hard-coded here because we have not such power...
            vehicle = New Vehicle With {.LicensePlate = licensePlate, .Color = "Negro", .Brand = "Toyota"} ' what about type????? TODO

            Return _biz.Save(vehicle)
        End If

        Return vehicle
    End Function

    ''' <summary>
    ''' Busca o crea el vehículo sin utilizar el servicio web.
    ''' Usando en etapa de desarrollo para pruebas y demás
    ''' </summary>
    ''' <param name="vehicle">Vehículo a buscar o guardar</param>
    ''' <returns>El vehículo encontrado o recién guardado</returns>
    Public Function FindOrCreate(vehicle As Vehicle) As Vehicle
        ' Inicialmente sólo contamos con un número de placa y un tipo
        Dim newVehicle As Vehicle

        Try
            newVehicle = FindByLicensePlate(vehicle.LicensePlate) ' si tira excepción es porque los datos están mal.
            Return newVehicle ' si llega aquí el vehículo existía previamente
        Catch ex As LogicViolationException
            Throw ex
        Catch ex2 As NotFoundException
            ' entonces toca crearlo, usando el WebService o creando uno nuevo desde cero
            ' vehicle = FindInWebService(vehicle.licensePlate)
            ' Se crea uno desde 0 porque no hay acceso al WS.
            If _biz.Validate(vehicle) Then
                Return Save(vehicle)
            Else
                Throw New ValidationException("Faltan campos requeridos en los datos del vehículo")
            End If
        End Try
    End Function

    Private Function Save(vehicle As Vehicle) As Vehicle
        Return _biz.Save(vehicle)
    End Function

    ''' <summary>
    ''' Verifica si existe un vehículo con esa placa en la base de datos.
    ''' </summary>
    ''' <param name="licensePlate">La placa a buscar</param>
    ''' <returns>Si existe el vehículo o no</returns>
    Public Function Exist(licensePlate) As Boolean
        Return _biz.Exists(licensePlate)
    End Function

End Class
