﻿Imports System.Timers
Imports ParqueosOIJ.Model

Public Class ParkingLotUpdater
    Shared _timer As Timer
    Shared _serv As ParkingLotService
    Shared Interval As Double = 1800000

    Public Shared Sub Start()
        Debug.WriteLine("[JOB] Iniciando ParkingLotUpdater")
        _serv = New ParkingLotService()

        _timer = New Timer(Interval) ' 30 minutes
        AddHandler _timer.Elapsed, New ElapsedEventHandler(AddressOf Handler)
        _timer.Enabled = True
    End Sub

    Shared Sub Handler(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        Dim parkingLots As ICollection(Of ParkingLot)
        parkingLots = _serv.FindAll()

        For Each pl As ParkingLot In parkingLots
            If pl.HasReservation() And pl.Status <> StatusEnum.Reservado Then
                Debug.WriteLine("[JOB] Actualizar espacio de parqueo con ID=" + pl.Id.ToString())
                pl.Status = StatusEnum.Reservado
                _serv.Save(pl)
            End If

            If pl.HasUnredeemedReservation() And pl.Status <> StatusEnum.Disponible Then
                Debug.WriteLine("[JOB] Actualizar espacio de parqueo con ID=" + pl.Id.ToString())
                pl.Status = StatusEnum.Disponible
                _serv.Save(pl)
            End If
        Next

    End Sub
End Class
