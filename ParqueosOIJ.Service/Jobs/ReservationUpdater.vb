﻿Imports System.Timers
Imports ParqueosOIJ.Model

''' <summary>
''' Timer que actualiza el estado de una reservación en caso de que su fecha de
''' ingreso esté en el pasado
''' </summary>
Public Class ReservationUpdater
    Shared _timer As Timer
    Shared _serv As ReservationService
    Shared Interval As Double = 1800000

    Public Shared Sub Start()
        Debug.WriteLine("[JOB] Iniciando ReservationUpdater")
        _serv = New ReservationService()

        _timer = New Timer(Interval) ' 30 minutes
        AddHandler _timer.Elapsed, New ElapsedEventHandler(AddressOf Handler)
        _timer.Enabled = True
    End Sub

    Shared Sub Handler(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        Dim reservations As ICollection(Of Reservation)
        reservations = _serv.FindAll()

        For Each r As Reservation In reservations
            If r.HasExpired() Then
                ' Actualizamos el estado
                Debug.WriteLine("[JOB] Actualizar reservación con ID=" + r.Id.ToString())
                r.Status = ReservationStatus.Vencida
                _serv.Update(r)
            End If
        Next

    End Sub

End Class
