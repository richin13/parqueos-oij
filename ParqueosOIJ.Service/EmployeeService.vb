﻿Imports ParqueosOIJ.Business
Imports ParqueosOIJ.Model

Public Class EmployeeService

    Private _biz As EmployeeBusiness

    Public Sub New()
        _biz = New EmployeeBusiness()
    End Sub

    Public Function Find(id As Integer) As Employee
        Return _biz.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Employee)
        Return _biz.FindAll()
    End Function

    ''' <summary>
    ''' Retorna una persona de la base de datos con base en su
    ''' número de identificación.
    ''' 
    ''' </summary>
    ''' <param name="dni">El número de identificación de la persona a buscar.</param>
    ''' <returns>Una instancia de persona o una excepción del tipo NotFoundException</returns>
    Public Function FindByDni(dni As String) As Employee
        Return _biz.FindByDni(dni)
    End Function

    ''' <summary>
    ''' Busca un empleado en la base de datos dado su nombre (o parte de él)
    ''' como llave de búsqueda.
    ''' </summary>
    ''' <param name="key">El valor o clave para realizar la búsqueda</param>
    ''' <returns>Lista de empleados que cumplen el criterio de búsqueda en su nombre</returns>
    Function Search(key As String) As ICollection(Of Employee)
        Return _biz.Search(key)
    End Function

    Function Exists(guid As String) As Boolean
        Return _biz.Exists(guid)
    End Function

End Class
