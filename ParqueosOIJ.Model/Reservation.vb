Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Reservation

    Public Sub New()
        RDate = Date.Now
    End Sub

    ' The identification number of the reservation
    <Display(Name:="N�mero")>
    <Key>
    Public Property Id As Integer

    <Display(Name:="Fecha")>
    Public Property RDate As Date

    <Display(Name:="Estado")>
    Public Property Status As ReservationStatus

    <Required(ErrorMessage:="Debe especificar el motivo de la visita.")>
    <Display(Name:="Motivo de la visita")>
    <MaxLength(255, ErrorMessage:="El motivo debe tener menos de 255 caracteres")>
    Public Property Reason As String

    <Display(Name:="Comentario")>
    <MaxLength(255, ErrorMessage:="El comentario debe tener menos de 255 caracteres")>
    Public Property Comment As String

    <Display(Name:="Fecha de entrada")>
    Public Property EntryDate As Date

    <Display(Name:="Fecha de salida")>
    Public Property DepartureDate As Date

    <Required(ErrorMessage:="Debe especificar un n�mero de c�dula de visitante v�lido.")>
    Public Property VisitorID As Integer

    Public Property ParkingLotID As Integer?

    <ForeignKey("ParkingLotID")>
    Public Overridable Property ParkingLot As ParkingLot

    ' The visitor
    <ForeignKey("VisitorID")>
    Public Overridable Property Visitor As Visitor

    <Required(ErrorMessage:="Debe especificar un n�mero de placa de veh�culo v�lido.")>
    Public Property VehicleID As Integer

    ' The car
    <ForeignKey("VehicleID")>
    Public Overridable Property Vehicle As Vehicle

    <Required(ErrorMessage:="Debe especificar un n�mero de c�dula de empleado v�lido.")>
    Public Property EmployeeID As Integer

    <ForeignKey("EmployeeID")>
    Public Overridable Property Employee As Employee

    Public Property UserID As String

    <ForeignKey("UserID")>
    Public Overridable Property User As User

    ''' <summary>
    ''' Determina si la reservaci�n ha vencido. Una reservaci�n vence
    ''' </summary>
    ''' <returns></returns>
    Public Function HasExpired() As Boolean
        If EntryDate.CompareTo(Date.Now) < 0 And Status = ReservationStatus.Pendiente Then
            ' Si la fecha de entrada ya pas� y la reservaci�n a�n estaba pendiente.
            Return True
        End If

        If EntryDate.AddMinutes(20).CompareTo(Date.Now) < 0 And Status = ReservationStatus.Aprobada Then
            ' Si han pasado 20 minutos desde la fecha de entrada esperada y la reservaci�n
            ' fue aprobada
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Determina si la reservaci�n puede ser aprobada
    ''' </summary>
    ''' <returns></returns>
    Public Function CanApprove() As Boolean
        Return Date.Now.CompareTo(EntryDate) < 0 And
            (Status = ReservationStatus.Pendiente Or
            Status = ReservationStatus.Rechazada)
    End Function

    ''' <summary>
    ''' Determina si la reservaci�n puede ser rechazada.
    ''' </summary>
    ''' <returns></returns>
    Public Function CanReject() As Boolean
        Return Date.Now.CompareTo(EntryDate) < 0 And
            (Status = ReservationStatus.Pendiente Or
            Status = ReservationStatus.Aprobada)
    End Function

End Class

Public Enum ReservationStatus
    Pendiente
    Aprobada
    Rechazada
    Vencida
End Enum
