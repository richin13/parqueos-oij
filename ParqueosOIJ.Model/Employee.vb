Imports System.ComponentModel.DataAnnotations


Partial Public Class Employee
    Inherits Person

    Private _floor As Integer
    Private _reservations As ICollection(Of Reservation)
    Private _visits As ICollection(Of Admission)

    Public Sub New()
        _reservations = New HashSet(Of Reservation)
        _visits = New HashSet(Of Admission)
    End Sub

    ' The building floor where the employee has his office/workplace
    <Display(Name:="N�mero de piso")>
    Public Property Floor As Integer
        Get
            Return _floor
        End Get
        Set(value As Integer)
            _floor = value
        End Set
    End Property

    ' The list of reservations made by this employee
    Public Property Reservations As ICollection(Of Reservation)
        Get
            Return _reservations
        End Get
        Set(value As ICollection(Of Reservation))
            _reservations = value
        End Set
    End Property

    ' The list of registered admissions of people visiting this employee
    Public Property Visits As ICollection(Of Admission)
        Get
            Return _visits
        End Get
        Set(value As ICollection(Of Admission))
            _visits = value
        End Set
    End Property

End Class