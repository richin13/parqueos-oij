Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Parking

    Private _id As Integer
    Private _name As String
    Private _capacity As Integer
    Private _shapeSvg As String
    Private _parkingLots As ICollection(Of ParkingLot)

    Public Sub New()
        _parkingLots = New HashSet(Of ParkingLot)
    End Sub

    Public Property Id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    ' The parking's name
    <Display(Name:="Nombre")>
    <Required(ErrorMessage:="Debe especificar el nombre del parqueo")>
    <Index(IsUnique:=True)>
    <MaxLength(64, ErrorMessage:="El nombre del parqueo debe tener menos de 64 caracteres")>
    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    ' The parking's capacity
    <Required>
    <Display(Name:="Capacidad")>
    Public Property Capacity As Integer
        Get
            Return _capacity
        End Get
        Set(value As Integer)
            _capacity = value
        End Set
    End Property

    <Required>
    Public Property Shape As String
        Get
            Return _shapeSvg
        End Get
        Set(value As String)
            _shapeSvg = value
        End Set
    End Property

    ' The list of all the parking lots in this parking
    Public Overridable Property ParkingLots As ICollection(Of ParkingLot)
        Get
            Return _parkingLots
        End Get
        Set(value As ICollection(Of ParkingLot))
            _parkingLots = value
        End Set
    End Property

End Class