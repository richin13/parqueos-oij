Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework

Partial Public Class User
    Inherits IdentityUser
    Implements IUser(Of String)

    Public Sub New()
        JoinDate = Date.Now
    End Sub

    <Display(Name:="Nombre completo")>
    <Required(ErrorMessage:="Debe especificar el nombre")>
    Public Property FirstName As String

    <Display(Name:="Apellidos")>
    <Required(ErrorMessage:="Debe especificar los apellidos")>
    Public Property LastName As String

    <Display(Name:="Usuario desde")>
    Public Property JoinDate As Date

    Public Property ApprovedReservations As ICollection(Of Reservation)

    Public Property RegisteredAdmissions As ICollection(Of Admission)

    <NotMapped>
    Public Property Role As IdentityRole

End Class