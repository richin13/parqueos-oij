Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Newtonsoft.Json

Partial Public Class ParkingLot
    Public Sub New()
        Admissions = New List(Of Admission)()
        Reservations = New List(Of Reservation)()
    End Sub

    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property Id As Integer

    ' The parking lot's type. See ParkingLot.ParkingLotType for details
    <Required>
    <Display(Name:="Tipo")>
    Public Property Type As ParkingLotType

    ' The parking lot's status. See ParkingLot.StatusEnum for details
    <Required>
    <Display(Name:="Estado")>
    Public Property Status As StatusEnum

    <Required>
    Public Property Height As Double

    <Required>
    Public Property Width As Double

    <Required>
    Public Property X As Double

    <Required>
    Public Property Y As Double

    <JsonIgnore>
    Public Property Parking As Parking

    <Display(Name:="Ingresos")>
    <JsonIgnore>
    Public Property Admissions As ICollection(Of Admission)

    <Display(Name:="Reservaciones")>
    <JsonIgnore>
    Public Property Reservations As ICollection(Of Reservation)

    ''' <summary>
    ''' Devuelve el ingreso activo asignado al espacio de parqueo.
    ''' Si el espacio se encuentra disponible entonces retorna
    ''' Nothing
    ''' </summary>
    ''' <returns></returns>
    Public Function GetActiveAdmission() As Admission
        Return Admissions _
            .Select(Function(a) a) _
            .Where(Function(a) a.Status = AdmissionStatus.Incompleta) _
            .OrderBy(Function(a) a.EntryDate) _
            .FirstOrDefault()
    End Function

    ''' <summary>
    ''' Modifica el estado del espacio de parqueo de acuerdo al
    ''' estado que tendr� en la fecha indicada.
    ''' </summary>
    ''' <param name="eventualDate">La fecha a predecir/determinar</param>
    ''' <returns></returns>
    Public Function PredictStatus(eventualDate As Date) As ParkingLot
        If eventualDate.CompareTo(Date.Now) < 0 Then
            ' Si la fecha est� en el pasado usamos otro m�todo
            Return DeterminePastStatus(eventualDate)
        End If

        Dim status As StatusEnum = StatusEnum.Desconocido
        Dim activeAdm = GetActiveAdmission()

        If IsNothing(activeAdm) Then
            ' Si no hay ning�n ingreso activo en el espacio de parqueo.
            status = StatusEnum.Disponible
        Else
            ' Hay un ingreso actualmente en este espacio
            If Not IsNothing(activeAdm.DepartureDate) Then
                ' Tiene fecha de salida (probablemente tentativa)
                If activeAdm.DepartureDate.Value.CompareTo(eventualDate) <= 0 Then
                    ' La fecha de salida est� antes de la fecha dada, entonces
                    ' El espacio ya estar� disponible en dicha fecha
                    status = StatusEnum.Disponible
                Else
                    status = StatusEnum.Ocupado
                End If
            Else
                ' El Ingreso no tiene hora de salida por lo que es incierto
                ' el estado que tendr� en la fecha dada.
                status = StatusEnum.Desconocido
            End If
        End If

        If status = StatusEnum.Disponible And IsReservedForDate(eventualDate) Then
            ' Si despu�s de chequear el ingreso, el campo est� marcado
            ' como disponible, revisamos que no est� reservado para esa fecha,
            ' en caso de estar reservado el estado cambia a 'Reservado'
            status = StatusEnum.Reservado
        End If

        Me.Status = status
        Return Me
    End Function

    ''' <summary>
    ''' Determina si el espacio estar� reservado en una fecha dada
    ''' </summary>
    ''' <param name="eventualDate"></param>
    ''' <returns></returns>
    Private Function IsReservedForDate(eventualDate As Date) As Boolean
        For Each r As Reservation In Reservations
            If IsInBetween(r.EntryDate, eventualDate, r.DepartureDate) Then
                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' Determina el estado del espacio de parqueo en una fecha pasada
    ''' </summary>
    ''' <param name="pastDate">Fecha</param>
    ''' <returns>Instancia con el nuevo estado</returns>
    Private Function DeterminePastStatus(pastDate As Date) As ParkingLot
        Dim status As StatusEnum = StatusEnum.Disponible

        For Each admission As Admission In Admissions.OrderBy(Function(a) a.EntryDate)
            If admission.EntryDate.CompareTo(pastDate) < 0 Then
                If Not IsNothing(admission.DepartureDate) Then
                    If IsInBetween(admission.EntryDate, pastDate, admission.DepartureDate.Value) Then
                        ' Si existe un ingreso cuyas fechas comprenden la fecha dada
                        ' entonces el espacio estuvo ocupado
                        status = StatusEnum.Ocupado
                    End If
                Else
                    ' Al llegar a este punto la fecha de ingreso es anterior a la fecha dada
                    ' y como no hay fecha de salida definida entonces determinamos que el
                    ' estado es ocupado.
                    status = StatusEnum.Ocupado
                End If
            End If
        Next

        Me.Status = status
        Return Me
    End Function

    ''' <summary>
    ''' Determina si el espacio de Parqueo tiene alguna reservaci�n asignada
    ''' dentro de los pr�ximos 30 minutos
    ''' </summary>
    ''' <returns></returns>
    Public Function HasReservation() As Boolean
        For Each r As Reservation In Reservations
            If IsInBetween(r.EntryDate, Date.Now.AddMinutes(30), r.DepartureDate) Then
                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' Determina si el espacio estaba reservado pero la persona no lleg�
    ''' (da tiempo 20 minutos)
    ''' </summary>
    ''' <returns></returns>
    Public Function HasUnredeemedReservation() As Boolean
        For Each r As Reservation In Reservations
            ' Hay un periodo de gracia de 20 minutos
            ' Si la persona no ingresa al parqueo se le anular� la reservaci�n
            If IsInBetween(r.EntryDate.AddMinutes(20), Date.Now, r.DepartureDate) And
                    r.Status = ReservationStatus.Aprobada Then
                Return True
            End If
        Next

        Return False
    End Function


    ''' <summary>
    ''' Determina si una fecha dada est� dentro del rango comprendido por los pa-
    ''' r�metros 'lower' y 'upper'
    ''' </summary>
    ''' <param name="lower">El l�mite inferior del rango de fecha</param>
    ''' <param name="actualDate">La fecha a determinar si se encuentra en el rango</param>
    ''' <param name="upper">El l�mite superior del rango de fecha</param>
    ''' <returns>Verdadero si est� dentro del rango, falso en caso contrario.</returns>
    Private Function IsInBetween(lower As Date, actualDate As Date, upper As Date) As Boolean
        Return lower.CompareTo(actualDate) <= 0 And upper.CompareTo(actualDate) >= 0
    End Function

End Class

Public Enum ParkingLotType
    Visitas
    Oficial
    Jefatura
    Discapacitados
End Enum

Public Enum StatusEnum
    Disponible
    Deshabilitado
    Ocupado
    Reservado
    Desconocido
End Enum
