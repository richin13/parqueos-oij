Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Admission
    Public Sub New()
        Visitors = New HashSet(Of Visitor)
        Status = AdmissionStatus.Incompleta
    End Sub

    ' The internal identification number. For persistence purposes
    <Key>
    Public Property Id As Integer

    ' The reason of the visit.
    <Display(Name:="Raz�n de la visita")>
    Public Property Reason As String

    ' The name of the company or institution he/she/they represent(s)
    <Display(Name:="Compa��a/Instituci�n")>
    Public Property Company As String

    ' The date of the entry. Defaults to now
    <Display(Name:="Fecha de entrada")>
    <DataType(DataType.Date)>
    Public Property EntryDate As Date

    ' The expected departure hour.
    <Display(Name:="Fecha tentativa de salida")>
    <DataType(DataType.Date)>
    Public Property DepartureDate As Date?

    <Display(Name:="Estado")>
    <Required>
    Public Property Status As AdmissionStatus

    Public Property EmployeeID As Integer?

    ' The employee he/she/they visit(s)
    <Display(Name:="Empleado a quien visita")>
    <ForeignKey("EmployeeID")>
    Public Overridable Property Employee As Employee

    <Required>
    Public Property VehicleID As Integer

    ' The vehicle that is entering the parking
    <Display(Name:="Veh�culo")>
    <ForeignKey("VehicleID")>
    Public Overridable Property Vehicle As Vehicle

    <Required>
    Public Property ParkingLotID As Integer

    ' The assigned parking lot.
    <Display(Name:="Espacio asignado")>
    <ForeignKey("ParkingLotID")>
    Public Overridable Property ParkingLot As ParkingLot

    <Required>
    Public Property UserID As String

    ' The logged-in user that registered the admission.
    <Display(Name:="Registrada por")>
    <ForeignKey("UserID")>
    Public Overridable Property User As User

    ' The list of visitors entering the parking.
    <Display(Name:="Visitantes")>
    <Required>
    Public Overridable Property Visitors As ICollection(Of Visitor)
End Class

Public Enum AdmissionStatus
    Incompleta
    Completada
End Enum