﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Person

    Public Sub New()
        ' todo
    End Sub

    <Key>
    Public Property Id As Integer

    <Display(Name:="Cédula")>
    <Index(IsUnique:=True)>
    <MaxLength(20)>
    Public Property Dni As String

    <Display(Name:="Nombre")>
    <DataType(DataType.Text)>
    Public Property Name As String

    <Display(Name:="Correo electrónico")>
    <DataType(DataType.EmailAddress)>
    Public Property Email As String

End Class