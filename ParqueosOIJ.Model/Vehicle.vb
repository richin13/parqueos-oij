Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Vehicle

    Private _id As Integer
    Private _licensePlate As String
    Private _brand As String
    Private _color As String
    Private _type As VehicleType
    Private _reservations As ICollection(Of Reservation)
    Private _admissions As ICollection(Of Admission)


    Public Sub New()

    End Sub

    ' The internal ID of the vehicle.
    <Key>
    Public Property Id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    ' The license plate number or internal number.
    <Display(Name:="Placa/N�mero veh�culo")>
    <Required(ErrorMessage:="Debe especificar la placa del veh�culo")>
    <DataType(DataType.Text)>
    <Index(IsUnique:=True)>
    <MaxLength(20)>
    Public Property LicensePlate As String
        Get
            Return _licensePlate
        End Get
        Set(value As String)
            _licensePlate = value
        End Set
    End Property

    ' The brand and/or style of the vehicle.
    <Display(Name:="Modelo")>
    <DataType(DataType.Text)>
    <MaxLength(32, ErrorMessage:="La marca del veh�culo debe tener menos de 32 caracteres")>
    Public Property Brand As String
        Get
            Return _brand
        End Get
        Set(value As String)
            _brand = value
        End Set
    End Property

    ' The color of the vehicle
    <Display(Name:="Color")>
    <DataType(DataType.Text)>
    <MaxLength(32, ErrorMessage:="El color del veh�culo debe tener menos de 16 caracteres")>
    Public Property Color As String
        Get
            Return _color
        End Get
        Set(value As String)
            _color = value
        End Set
    End Property

    ' The type of vehicle
    <Display(Name:="Tipo")>
    Public Property Type As VehicleType
        Get
            Return _type
        End Get
        Set(value As VehicleType)
            _type = value
        End Set
    End Property

    <Display(Name:="Reservaciones")>
    Public Property Reservations As ICollection(Of Reservation)
        Get
            Return _reservations
        End Get
        Set(value As ICollection(Of Reservation))
            _reservations = value
        End Set
    End Property

    <Display(Name:="Ingresos")>
    Public Property Admissions As ICollection(Of Admission)
        Get
            Return _admissions
        End Get
        Set(value As ICollection(Of Admission))
            _admissions = value
        End Set
    End Property

End Class

Public Enum VehicleType
    Visita
    Oficial
    Jefatura
End Enum