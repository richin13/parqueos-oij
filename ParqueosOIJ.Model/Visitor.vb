Imports System.ComponentModel.DataAnnotations

Public Class Visitor
    Inherits Person

    Public Sub New()
        Admissions = New HashSet(Of Admission)
        Reservations = New HashSet(Of Reservation)
    End Sub

    <Display(Name:="Tel�fono")>
    <DataType(DataType.PhoneNumber)>
    Public Property PhoneNumber As String

    <Required>
    <Display(Name:="Nacionalidad")>
    Public Property Nationality As NationalityType

    <Display(Name:="Conductor?")>
    Public Property IsDriver As Boolean

    <Display(Name:="Ingresos")>
    Public Property Admissions As ICollection(Of Admission)

    <Display(Name:="Reservaciones")>
    Public Property Reservations As ICollection(Of Reservation)

End Class

Public Enum NationalityType
    Local = 0
    Foreigner = 1
End Enum