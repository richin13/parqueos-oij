﻿Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model

Public Class VisitorBusiness

    Private _db As VisitorData

    Public Sub New()
        _db = New VisitorData()
    End Sub

    Public Function Find(id As Integer) As Visitor
        Return _db.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Visitor)
        Return _db.FindAll()
    End Function

    Public Function FindByDni(dni As String) As Visitor
        Return _db.FindByDni(dni)
    End Function

    Public Function Save(visitor As Visitor) As Visitor
        ' Validations here
        Return _db.Save(visitor)
    End Function

    Public Function Delete(dni As String) As Visitor
        Return _db.Delete(dni)
    End Function

    Public Function Update(visitor As Visitor) As Visitor
        Return _db.Update(visitor)
    End Function

    Public Function Search(key As String) As ICollection(Of Visitor)
        Return _db.Search(key)
    End Function

    Public Function Validate(visitor As Visitor) As Boolean
        Return Not String.IsNullOrEmpty(visitor.Name) _
            And Not String.IsNullOrEmpty(visitor.Dni) _
            And Not IsNothing(visitor.Nationality)

    End Function

End Class
