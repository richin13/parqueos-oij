﻿Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model

Public Class ReservationBusiness

    Private _db As ReservationData
    Private _person_db As EmployeeData
    Private _vehicle_da As VehicleData
    Private _visitor_da As EmployeeData


    Public Sub New()
        _db = New ReservationData()
        _person_db = New EmployeeData()
        _visitor_da = New EmployeeData()
        _vehicle_da = New VehicleData()
    End Sub

    Function Find(id As Integer) As Reservation
        Return _db.Find(id)
    End Function

    Function Save(ByRef Reservation As Reservation) As Reservation
        If Reservation.EntryDate.CompareTo(Reservation.DepartureDate) >= 0 Then
            Throw New LogicViolationException("Fecha de entrada no puede ser mayor que fecha de salida")
        End If

        Return _db.Save(Reservation)
    End Function

    Function Update(reservation As Reservation)
        If reservation.Status = ReservationStatus.Aprobada Then
            If IsNothing(reservation.ParkingLotID) Or reservation.ParkingLotID = 0 Then
                Throw New ValidationException("Debe especificar el espacio a reservar")
            End If
        End If

        If reservation.HasExpired() Then
            reservation.Status = ReservationStatus.Vencida
            _db.Update(reservation)
            Throw New ValidationException("La reservación ya está vencida")
        End If

        Return _db.Update(reservation)
    End Function

    ''' <summary>
    ''' Retorna una lista con todas las reservaciones que hayan en la
    ''' base de datos.
    ''' </summary>
    ''' <returns>La lista con todas las reservaciones</returns>
    Function FindAll() As ICollection(Of Reservation)
        Return _db.FindAll()
    End Function

    ''' <summary>
    ''' Encuentra todas las reservaciones cuyo estado es igual al estado
    ''' dado por parámetro.
    ''' </summary>
    ''' <param name="status">El estado que servirá como filtro.</param>
    ''' <returns>La lista de todas las reservaciones que posean el estado dado.</returns>
    Function FindByStatus(status As ReservationStatus) As ICollection(Of Reservation)
        Return _db.FindByStatus(status)
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="vehicleLicensePlate"></param>
    ''' <param name="visitorDni"></param>
    ''' <returns></returns>
    Public Function FindByLicensePlateAndDni(vehicleLicensePlate As String, visitorDni As String) As ICollection(Of Reservation)
        Dim all As ICollection(Of Reservation)
        all = _db.FindByLicensePlateAndDni(vehicleLicensePlate, visitorDni)

        If all.Count = 0 Then
            Throw New NotFoundException("No existe ninguna reservación que concuerde con los datos dados.")
        End If

        Return all
    End Function
End Class
