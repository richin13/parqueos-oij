﻿Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model

Public Class VehicleBusiness

    Private _db As VehicleData

    Public Sub New()
        _db = New VehicleData()
    End Sub


    ''' <summary>
    ''' Busca un vehículo dada su llave o ID
    ''' </summary>
    ''' <param name="id">El id a buscar</param>
    ''' <returns>El vehículo encontrado</returns>
    Public Function Find(id As Integer) As Vehicle
        Dim vehicle As Vehicle
        vehicle = _db.Find(id)

        If IsNothing(vehicle) Then
            Throw New NotFoundException(String.Format("El vehículo #{0} no existe.", id))
        End If

        Return vehicle
    End Function

    ''' <summary>
    ''' Devuelve todos los vehículos guardados en la base de datos.
    ''' </summary>
    ''' <returns>Lista de todos lo vehículos</returns>
    Public Function FindAll() As ICollection(Of Vehicle)
        Return _db.FindAll()
    End Function

    ''' <summary>
    ''' Busca un vehículo en la base de datos dado su número de placa
    ''' </summary>
    ''' <param name="licensePlate">Número de placa del vehículo a buscar</param>
    ''' <returns>El vehículo con la placa dada, nulo si no existe</returns>
    Public Function FindByLicensePlate(licensePlate As String) As Vehicle
        If String.IsNullOrEmpty(licensePlate) Then
            Throw New LogicViolationException("Debe especificar el número de placa")
        End If

        Dim vehicle As Vehicle
        vehicle = _db.FindByLicensePlate(licensePlate)

        If IsNothing(vehicle) Then
            Throw New NotFoundException(String.Format("El vehículo con placa {0} no existe.", licensePlate))
        End If

        Return vehicle
    End Function

    ''' <summary>
    ''' Busca un vehículo en la base de datos dado su número de placa,
    ''' si no existe previamente lo guarda.
    ''' </summary>
    ''' <param name="vehicle">El vehículo a buscar (o guardar)</param>
    ''' <returns>El vehículo encontrado o recién guardado, según corresponda</returns>
    Public Function FindOrCreateByLicensePlate(vehicle As Vehicle) As Vehicle
        Dim _vehicle As Vehicle
        _vehicle = FindByLicensePlate(vehicle.LicensePlate)

        If IsNothing(_vehicle) Then
            Return Save(vehicle)
        End If

        Return _vehicle
    End Function

    ''' <summary>
    ''' Guarda un nuevo vehículo en la base de datos
    ''' </summary>
    ''' <param name="vehicle"></param>
    ''' <returns>El vehículo recién guardado</returns>
    Public Function Save(vehicle As Vehicle) As Vehicle
        ' Validations here
        Return _db.Save(vehicle)
    End Function

    ''' <summary>
    ''' Verifica si existe un vehículo en la base de datos.
    ''' 
    ''' </summary>
    ''' <param name="licensePlate">El número de placa del vehículo</param>
    ''' <returns>True si el vehículo existe, false en caso contrario</returns>
    Function Exists(licensePlate As String) As Boolean
        Return _db.Exists(licensePlate)
    End Function

    Function Validate(vehicle As Vehicle) As Boolean
        Dim isValid As Boolean = True

        isValid = isValid And Not IsNothing(vehicle.LicensePlate) _
            And Not IsNothing(vehicle.Type)

        If isValid And vehicle.Type = VehicleType.Visita Then
            If String.IsNullOrEmpty(vehicle.Brand) _
                And String.IsNullOrEmpty(vehicle.Color) Then
                Throw New ValidationException("Para vehículo visitante debe especificar todas las caracterísitcas.")
            End If
        End If

        Return isValid
    End Function

End Class
