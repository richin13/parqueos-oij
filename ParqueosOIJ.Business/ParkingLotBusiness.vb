﻿Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model

Public Class ParkingLotBusiness

    Private _db As ParkingLotData

    Public Sub New()
        _db = New ParkingLotData()
    End Sub

    Public Function Find(number As Integer) As ParkingLot
        Dim parkingLot As ParkingLot
        parkingLot = _db.Find(number)

        If IsNothing(parkingLot) Then
            Throw New NotFoundException(String.Format("El espacio de parqueo #{0} no existe", number))
        End If

        Return parkingLot
    End Function

    Public Function FindAll() As ICollection(Of ParkingLot)
        Return _db.FindAll()
    End Function

    Public Function FindByParking(parkingId As Integer) As ICollection(Of ParkingLot)
        Return _db.FindByParking(parkingId)
    End Function

    Public Function Save(parkingLot As ParkingLot) As ParkingLot
        Return _db.Save(parkingLot)
    End Function

    Public Function Update(parkingLot As ParkingLot) As ParkingLot
        Return _db.Save(parkingLot)
    End Function

    Public Function Delete(id As Integer) As ParkingLot
        Dim pl As ParkingLot
        pl = Find(id)

        If pl.Status = StatusEnum.Disponible Or pl.Status = StatusEnum.Deshabilitado Then
            Return _db.Delete(id)
        End If

        Throw New LogicViolationException("No puede eliminar un espacio de parqueo que no esté disponible o deshabilitado")
    End Function

    Public Function Exist(id As Integer) As Boolean
        Return _db.Exist(id)
    End Function

End Class
