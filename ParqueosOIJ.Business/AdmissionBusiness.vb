﻿Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model

Public Class AdmissionBusiness

    Private _db As AdmissionData

    Private _plBusiness As ParkingLotBusiness
    Private _employeeBusiness As EmployeeBusiness

    Public Sub New()
        _db = New AdmissionData()
        _plBusiness = New ParkingLotBusiness()
        _employeeBusiness = New EmployeeBusiness()
    End Sub

    Public Function Find(id As Integer) As Admission
        Dim admission As Admission
        admission = _db.Find(id)

        If IsNothing(admission) Then
            Throw New NotFoundException(String.Format("No existe el ingreso con el ID #{0}", id))
        End If

        Return admission
    End Function

    Public Function FindAll() As ICollection(Of Admission)
        Return _db.FindAll()
    End Function

    Public Function FindAllActive() As ICollection(Of Admission)
        Return _db.FindAllByStatus(AdmissionStatus.Incompleta)
    End Function

    Public Function FindByDateRangeCedLicensePlate(car As String, ced As String, lowerS As String, upperS As String) As IEnumerable(Of Admission)
        'Se validan campos vacios
        If car Is Nothing Or ced Is Nothing Or lowerS = "" Or upperS = "" Then
            Throw New ValidationException("Debe ingresar un rango de fecha.")
        End If

        'Si en efecto vienen fechas entonces se les hace un casting de String a tipo Date
        Dim lowerD = CDate(lowerS)
        Dim upperD = CDate(upperS)
        upperD = upperD.AddDays(1)

        'Revision de orden cronologico correcto en las fechas
        If lowerD.Date > upperD.Date Then
            Throw New ValidationException("El orden cronológico en el rango de fechas es incorrecto. La fecha inicial debe ser inferior a la fecha final.")
        End If

        'Se envia la solicitud a la capa Data
        Dim reservations As ICollection(Of Admission)
        reservations = _db.FindByDateRangeCedLicensePlate(car, ced, lowerD, upperD)

        'Se valida que se reciban datos
        If reservations.Count = 0 Then
            Throw New NotFoundException("No existe ningun ingreso que concuerde con los datos ingresados.")
        End If
        Return reservations
    End Function

    'Solo rango de recha
    Public Function FindByDateRange(lowerBound As String, upperBound As String) As IEnumerable(Of Admission)
        'Se validan campos vacios
        If lowerBound = "" Or upperBound = "" Then
            Throw New ValidationException("Debe ingresar un rango de fecha.")
        End If

        'Si en efecto vienen fechas entonces se les hace un casting de String a tipo Date
        Dim lowerD = CDate(lowerBound)
        Dim upperD = CDate(upperBound)
        upperD = upperD.AddDays(1)

        'Revision de orden cronologico correcto en las fechas
        If lowerD.Date > upperD.Date Then
            Throw New ValidationException("El orden cronológico en el rango de fechas es incorrecto. La fecha inicial debe ser inferior a la fecha final.")
        End If

        'Se envia la solicitud a la capa Data
        Dim reservations As ICollection(Of Admission)
        reservations = _db.FindByDateRange(lowerD, upperD)

        'Se valida que se reciban datos
        If reservations.Count = 0 Then
            Throw New NotFoundException("No existe ningun ingreso que concuerde con los datos ingresados.")
        End If
        Return reservations


        'upperBound = upperBound.AddDays(1)
        'Return _db.FindByDateRange(lowerBound, upperBound)
    End Function

    'Rango de fecha y licencia
    Public Function FindByDateRangeLicensePlate(car As String, lowerBound As String, upperBound As String) As IEnumerable(Of Admission)
        'Se validan campos vacios
        If lowerBound = "" Or upperBound = "" Then
            Throw New ValidationException("Debe ingresar un rango de fecha.")
        End If

        'Si en efecto vienen fechas entonces se les hace un casting de String a tipo Date
        Dim lowerD = CDate(lowerBound)
        Dim upperD = CDate(upperBound)
        upperD = upperD.AddDays(1)

        'Revision de orden cronologico correcto en las fechas
        If lowerD.Date > upperD.Date Then
            Throw New ValidationException("El orden cronológico en el rango de fechas es incorrecto. La fecha inicial debe ser inferior a la fecha final.")
        End If

        'Se envia la solicitud a la capa Data
        Dim reservations As ICollection(Of Admission)
        reservations = _db.FindByDateRangeLicensePlate(car, lowerD, upperD)

        'Se valida que se reciban datos
        If reservations.Count = 0 Then
            Throw New NotFoundException("No existe ningun ingreso que concuerde con los datos ingresados.")
        End If
        Return reservations


        'upperBound = upperBound.AddDays(1)
        'Return _db.FindByDateRangeLicensePlate(car, lowerBound, upperBound)
    End Function

    'Rango de fecha y cedula
    Public Function FindByDateRangeCed(ced As String, lowerBound As String, upperBound As String) As IEnumerable(Of Admission)
        'Se validan campos vacios
        If lowerBound = "" Or upperBound = "" Then
            Throw New ValidationException("Debe ingresar un rango de fecha.")
        End If

        'Si en efecto vienen fechas entonces se les hace un casting de String a tipo Date
        Dim lowerD = CDate(lowerBound)
        Dim upperD = CDate(upperBound)
        upperD = upperD.AddDays(1)

        'Revision de orden cronologico correcto en las fechas
        If lowerD.Date > upperD.Date Then
            Throw New ValidationException("El orden cronológico en el rango de fechas es incorrecto. La fecha inicial debe ser inferior a la fecha final.")
        End If

        'Se envia la solicitud a la capa Data
        Dim reservations As ICollection(Of Admission)
        reservations = _db.FindByDateRangeCed(ced, lowerD, upperD)

        'Se valida que se reciban datos
        If reservations.Count = 0 Then
            Throw New NotFoundException("No existe ningun ingreso que concuerde con los datos ingresados.")
        End If
        Return reservations


        'upperBound = upperBound.AddDays(1)
        'Return _db.FindByDateRangeCed(ced, lowerBound, upperBound)
    End Function

    Public Function FindByDate(myDate As Date) As ICollection(Of Admission)
        'Se envia la solicitud a la capa Data
        Dim reservations As ICollection(Of Admission)
        reservations = _db.FindByDate(myDate)

        'Se valida que se reciban datos
        If reservations.Count = 0 Then
            Throw New NotFoundException("No se ha registrado ninguana visita para el presente día.")
        End If

        Return reservations
    End Function

    Public Function Search(key As String) As ICollection(Of Admission)
        Return _db.Search(key)
    End Function


    ''' <summary>
    ''' Registra la salida de un vehículo del parqueo. Para ello modifica
    ''' la propiedad Status el cual es una enumeración con los posibles valores: 
    ''' Completed e Incomplete. En esta función se verifica que el ingreso 
    ''' posea un ingreso incompleto así como que el valor de Status sea Incomplete.
    ''' </summary>
    ''' <param name="admission">El ingreso que se va a marcar como completado</param>
    ''' <returns>El ingreso actualizado con su nuevo estado</returns>
    Public Function CheckOut(admission As Admission) As Admission
        If IsNothing(admission) Then
            Throw New ValidationException("El espacio de parqueo no posee ningún ingreso activo.")
        ElseIf admission.Status = AdmissionStatus.Completada Then
            Throw New ValidationException("El ingreso ya se encuentra completado.")
        End If

        admission.DepartureDate = Date.Now
        admission.Status = AdmissionStatus.Completada
        admission.ParkingLot.Status = StatusEnum.Disponible
        Return _db.Update(admission)
    End Function

    Public Function Save(admission As Admission) As Admission
        ' Validations here

        ' verificar que existe el empleado, sino lanzar un notfound
        If admission.Vehicle.Type = VehicleType.Visita Then
            If Not _employeeBusiness.Exists(admission.Employee.Id) Then
                Throw New ValidationException("No existe el empleado a visitar. Contacte al administrador")
            End If
            admission.EmployeeID = admission.Employee.Id
        End If

        If Not ValidAdmissionType(admission.ParkingLot.Type, admission.Vehicle.Type) Then
            Throw New ValidationException("El tipo de vehículo no se puede insertar en un espacio de tipo " + admission.ParkingLot.Type.ToString())
        End If

        If admission.ParkingLot.Status <> StatusEnum.Disponible Then
            Throw New ValidationException("El espacio debe estar disponible para poder registrar un nuevo ingreso.")
        Else
            admission.ParkingLot.Status = StatusEnum.Ocupado
        End If

        Return _db.Save(admission)
    End Function

    ''' <summary>
    ''' Valida que un vehículo se esté insertando en un espacio de parqueo adecuado.
    ''' 
    ''' Un vehículo de jefatura sólamente puede ser ingresado en un espacio de parqueo
    ''' tipo Jefatura.
    ''' Un vehículo oficial sólamente puede ser ingresado en un espacio tipo oficial.
    ''' Un vehículo visitante puede ser ingresado tanto en un espacio visitante o de
    ''' discapacitados..
    ''' </summary>
    ''' <param name="plType">El tipo de espacio de parqueo donde se está realizando el ingreso.</param>
    ''' <param name="vehicleType">El tipo de vehículo que se está ingresando</param>
    ''' <returns></returns>
    Private Function ValidAdmissionType(plType As ParkingLotType, vehicleType As VehicleType)
        Return (plType = ParkingLotType.Jefatura And vehicleType = VehicleType.Jefatura) _
            Or (plType = ParkingLotType.Oficial And vehicleType = VehicleType.Oficial) _
            Or ((plType = ParkingLotType.Visitas Or plType = ParkingLotType.Discapacitados) And vehicleType = VehicleType.Visita)
    End Function

End Class
