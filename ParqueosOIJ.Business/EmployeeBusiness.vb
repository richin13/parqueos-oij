﻿Imports ParqueosOIJ.DataAccess
Imports System.Linq
Imports ParqueosOIJ.Model

Public Class EmployeeBusiness

    Private _db As EmployeeData

    Public Sub New()
        _db = New EmployeeData()
    End Sub

    Public Function Find(id As Integer) As Person
        Dim person As Person
        person = _db.Find(id)

        If person Is Nothing Then
            Throw New NotFoundException(String.Format("El funcionario #{0} no existe. Contacte al admnistrador", id))
        End If

        Return person
    End Function

    Public Function FindAll() As ICollection(Of Visitor)
        Return _db.FindAll()
    End Function

    Function FindByDni(dni As String) As Employee
        Return _db.FindByDni(dni)
    End Function

    Public Function Save(employee As Employee) As Employee
        Return _db.Save(employee)
    End Function

    Public Function Update(employee As Employee) As Employee
        Return _db.Update(employee)
    End Function

    Public Function Delete(dni As String) As Employee
        Return _db.Delete(dni)
    End Function

    Function Search(key As String) As ICollection(Of Employee)
        Return _db.Search(key)
    End Function

    Function Exists(guid As String) As Boolean
        Return _db.Exists(guid)
    End Function


End Class
