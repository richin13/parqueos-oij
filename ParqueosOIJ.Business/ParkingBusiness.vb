﻿Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model

Public Class ParkingBusiness
    Private _db As ParkingData

    Public Sub New()
        _db = New ParkingData()
    End Sub

    Public Function Find(id As Integer) As Parking
        Return _db.Find(id)
    End Function

    Public Function FindAll() As ICollection(Of Parking)
        Return _db.FindAll()
    End Function

    Public Function Exists(id As Integer) As Boolean
        Return _db.Exists(id)
    End Function

End Class
