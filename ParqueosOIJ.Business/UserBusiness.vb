﻿Imports ParqueosOIJ.DataAccess
Imports ParqueosOIJ.Model

Public Class UserBusiness

    Private _db As UserData

    Public Sub New()
        _db = New UserData()
    End Sub

    ''' <summary>
    ''' Busca un usuario en la base de datos dado su
    ''' unique id. El string unico generado por AspIdentity
    ''' </summary>
    ''' <param name="userId">string unico generado por AspIdentity</param>
    ''' <returns>El usuario encontrado</returns>
    Function Find(userId As String) As User
        Dim u As User
        u = _db.Find(userId)

        If IsNothing(u) Then
            Throw New NotFoundException("No existe el usuario")
        End If

        Return u
    End Function

    Function FindAll() As ICollection(Of User)
        Return _db.FindAll()
    End Function

    ''' <summary>
    ''' Guarda o actualiza un usuario en la base de datos
    ''' </summary>
    ''' <param name="user">El usuario a modificar o guardar.</param>
    ''' <returns>El usuario nuevo o actualizado</returns>
    Function Save(user As User) As User
        Return _db.Save(user)
    End Function

    Function Exists(id As String) As Boolean
        Return _db.Exists(id)
    End Function


End Class
