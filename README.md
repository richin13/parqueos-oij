# parqueos-oij

## Instalación

- Descargue [Git](https://git-scm.com/download/win) en su computadora y ejecutelo

- Clone el repositorio

```
git clone https://github.com/richin13/parqueos-oij.git
```

- Abra el proyecto con VisualStudio
- Verifique el proyecto ParqueosOIJ.Web esté como el **Startup Project**
- Inicie la **Nuget Package Manager console**
- Seleccione como default project **ParqueosOIJ.DataAccess**
- Ejecute el comando `Update-Database -Verbose`

En caso de que falle algo vaya al siguiente link -> [Link](https://www.google.com)
